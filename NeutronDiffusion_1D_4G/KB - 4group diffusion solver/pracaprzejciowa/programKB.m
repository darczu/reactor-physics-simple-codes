F=dlmread('dane_fraction.csv');
I=dlmread('dane_izotopy.csv');
R=dlmread('dane_rozne.csv');
Z=dlmread('dane_zawartosc.csv');
G=dlmread('dane_gestosc.csv');
chi=dlmread('chi.csv');

# Opis zmiennych
# 1. Na
# 2. 235 U
# 3. 238 U
# 4. 239 Pu
# 5. 240 Pu
# 6. 241 Pu
# 7. 242 Pu
# 8. oxygen
# 9. carbon
# 10. chromium
# 11. nickel
# 12. iron
# 13. molybdenum 

# Opis dla R
# 1. szerokosc inner core
# 2. szeokosc blanket
# 3. number of section
# 4. group number
# 5. dencity of steal
# 6. avogadro number
# 7. density MOX
# 8. B_z
# 9. number of zones
# 10. number of elements

# plik izotopy 1. Na  :1 grupa, 2 grupa ... kolejnosc jak wyzej 

# Obliczanie atom density

# Initialization of the parameters 
for j=1 : R(9)
  for i=1 : length(F)
    N(i,j)=(F(i,j)*Z(i,j)*G(i,1)*R(6))/G(i,2);
  endfor 
endfor

for j=1 : R(9)
  for i=1 : R(4)
    sigma_transport(i,j)=0;
    sigma_removal(i,j)=0;
    sigma_fission(i,j)=0;
    sigma_fission_nonu(i,j)=0;
    
  endfor
endfor

for j=1 : R(4)
  for i=1 : R(4)
    for k=1 : R(9)
      sigma_scattering (j,i,k)=0;
    endfor
  endfor 
endfor

for k=1 : R(9)
  for j=1 : R(4)
    for i=1 : R(10)
      sigma_transport(j,k)=sigma_transport(j,k)+N(i,k)*I(4*(i-1)+j,6);
      sigma_removal(j,k)=sigma_removal(j,k)+N(i,k)*I(4*(i-1)+j,3)+N(i,k)*I(4*(i-1)+j,4)+N(i,k)*I(4*(i-1)+1,5);
      sigma_fission(j,k)=sigma_fission(j,k)+N(i,k)*I(4*(i-1)+j,4)*I(4*(i-1)+j,7);
      sigma_fission_nonu(j,k)=sigma_fission_nonu(j,k)+N(i,k)*I(4*(i-1)+j,4);
    endfor
  endfor
endfor 

for i=1 : R(9) 
  j=0;
  g=1;
  while (g<length(I))
    j=j+1;
    sigma_scattering(1,2,i)=sigma_scattering(1,2)+N(j,i)*I(g,8);
    sigma_scattering(1,3,i)=sigma_scattering(1,3)+N(j,i)*I(g,9);
    sigma_scattering(2,3,i)=sigma_scattering(2,3)+N(j,i)*I(g,10);
    sigma_scattering(3,4,i)=sigma_scattering(3,4)+N(j,i)*I(g,11);
    g=g+4;
  endwhile
endfor

keff=1;
B_z=R(8);
F=1;
delta_r=R(2)/R(3);
width_of_reactor(2)=R(2);
width_of_reactor(1)=R(1);
number_of_section=R(3);
number_of_energy_group=R(4);
number_of_section=R(3);

#checking at which position the parameters are changing 
r=0;
j=1;
#calculation of coefficients
while (r<width_of_reactor(1))
  r=r+delta_r;
  p=j;
  j=j+1;
endwhile

for i=1 : number_of_energy_group
  for j=1 : number_of_section
    if (j<p)
      a(j,i)=(1/(3*sigma_transport(i,1)))*(2*delta_r*(j-1)/(delta_r));
    else
      a(j,i)=(1/(3*sigma_transport(i,2)))*(2*delta_r*(j-1)/(delta_r));
    endif
  endfor
  a(1+number_of_section,i)=0;
endfor

k=1;

for i = 1 : number_of_energy_group  
for j = 1 : number_of_section
A(k)=a(j,i);
k=k+1;
endfor
endfor
k=1;
while (k<number_of_energy_group*number_of_section)
A_1(k)=A(k+1);
k=k+1;
endwhile 

for i=1 : number_of_energy_group
for j=1 : number_of_section
if (j<p)
b(j,i)=a(j,i)+a(j+1,i)+(1/(3*sigma_transport(i,1)))*B_z*B_z*((delta_r*(j))^2-(delta_r*(j-1))^2)+sigma_removal(i,1)*((delta_r*(j))^2-(delta_r*(j-1))^2);
else
b(j,i)=a(j,i)+a(j+1,i)+(1/(3*sigma_transport(i,2)))*B_z*B_z*((delta_r*(j))^2-(delta_r*(j-1))^2)+sigma_removal(i,2)*((delta_r*(j))^2-(delta_r*(j-1))^2);
endif
endfor
endfor

k=1;
for i = 1 : number_of_energy_group  
for j = 1 : number_of_section
B_1(k)=b(j,i);
k=k+1;
endfor
endfor
#initial values of flux 

for i = 1 : number_of_energy_group 
for j = 1 : number_of_section 
flux(j,i)=1;
endfor 
endfor

# colculation of F1
F=0;
for j= 1 : number_of_section
fission=0;
for g = 1 : number_of_energy_group 
if (j<p)
fission=fission+sigma_fission(g,1)*flux(j,g)*((delta_r*(j))^2-(delta_r*(j-1))^2);
else
fission=fission+sigma_fission(g,2)*flux(j,g)*((delta_r*(j))^2-(delta_r*(j-1))^2);
endif
endfor 
F=F+fission;
endfor
F_prev=F;
F=10*F_prev; #made just to ensure that the loop will be initialized
h=1;

while and(abs(1-(F/F_prev))>0.0001, h<1000)
#colculation of sources

for i = 1 : number_of_energy_group 
for j = 1 : number_of_section
fission=0;
scattering=0;
for g = 1 : number_of_energy_group 
if (j<p)
fission=fission+sigma_fission(g,1)*flux(j,g);
else
fission=fission+sigma_fission(g,2)*flux(j,g);
endif
endfor 
for k = 1 : i-1 
if (j<p)
scattering=scattering+sigma_scattering(k,i,1)*flux(j,k);
else 
scattering=scattering+sigma_scattering(k,i,2)*flux(j,k);
endif
endfor
sources(j,i)=((1/keff)*chi(i)*fission+scattering)*((delta_r*j)^2-(delta_r*(j-1))^2);
endfor 
endfor

#creation of A*X=C elements

#creation of the vector C
k=1;
for i = 1 : number_of_energy_group  
for j = 1 : number_of_section
C(k)=sources(j,i);
k=k+1;
endfor
endfor

#creation of matrix A

B=diag(B_1,0)-diag(A_1,1)-diag(A_1,-1); 

X=B\C';

#changing vector of fluxes into matrix
k=1;
for i = 1 : number_of_energy_group  
for j = 1 : number_of_section
flux(j,i)=X(k);
k=k+1;
endfor
endfor

F_prev=F;
F=0;
for j= 1 : number_of_section
fission=0;
for g = 1 : number_of_energy_group 
if (j<p)
fission=fission+sigma_fission(g,1)*flux(j,g)*((delta_r*(j))^2-(delta_r*(j-1))^2);
else
fission=fission+sigma_fission(g,2)*flux(j,g)*((delta_r*(j))^2-(delta_r*(j-1))^2);
endif
endfor 
F=F+fission;
endfor

#changing of the keff

keff=keff*(F/F_prev);

h=h+1;
end
