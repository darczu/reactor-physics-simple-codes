%Plik zawiera przekroje czynne dla wielogrupowego przybli�enia dyfuzyjnego
%konkretnie 4 grupy neutron�w.
%�r�d�o: A.E. Waltar Fast Breeder Reactors
%Appendix C - 4 group Corss-Sections created at HEDL with 42-group code and
%ENDF/B (1978) Typical neutron spectrum for oxide-fueled LMFBR. 
%jednostka g��wna [barn]

function [a]=Sigma(i,j,k);
%{  
    Argmuenty funkcji:
    i - numer pierwiastka  1...18
        1 - Boron
        2 - Boron-10
        3 - Carbon 
        4 - Oxygen
        5 - Sodium
        6 - Chormium
        7 - Iron
        8 - Nickel
        9 - Molybdenum
        10 - Thorium 232
        11 - Uranium 233 
        12 - Uranium 235
        13 - Uranium 238 
        14 - Plutonium 239
        15 - Plutonium 240
        16 - Plutonium 241
        17 - Plutonium 242 
        18 - Fisson Products (pairs) 
    j - numer grupy energetycznej neutron�w
         1 - E >820 keV
         2 - 110 keV < E < 820 keV
         3 - 15 keV < E < 110 keV
         4 - 0 keV < E < 15 keV
    k - typ reakcji   i  ilosc neutron�w powstajacych na jedno rozsczepienie
         1 - Sigma_tr           microscopic transport cross section 
         2 - Sigma_c            capture/wychwyt
         3 - Sigma_f            fission/rozszczepienie
         4 - Sigma_er+ir        Elestic + inelastic / przekroje na rozproszenie el i nie el.
         5 - Ni                 Srednia liczba neutronow powstajacych w jednym akcie rozszczepienia
%}
Sigma=[];   %Generacja pustej macierzy
Sigma=zeros(18,4,5);

if (i>18)
    error('Nie ma takiego materialu')
elseif(j > 4)
    error('Nie ma takiej grupy')
elseif(k > 5)
    error('Nie ma takiej wartosci')    
end

%Boron (natural)
Sigma(1,1,:)= [1.6 0.06 0.0 0.45 0.0];  
Sigma(1,2,:)= [3.3 0.2  0.0 0.33 0.0];
Sigma(1,3,:)= [2.8 0.6  0.0 0.15 0.0];
Sigma(1,4,:)= [3.9 2.0  0.0 0.0 0.0 ];

%Boron-10
Sigma(2,1,:) = [1.8 0.3 0.0 0.45 0.0];
Sigma(2,2,:) = [4.2 1.1 0.0 0.33 0.0] ;
Sigma(2,3,:) = [5.2 3.0 0.0 0.15 0.0];
Sigma(2,4,:) = [12.2 10.3 0.0 0.0 0.0];

%Carbon
Sigma(3,1,:) = [1.8 0.001 0.0 0.39 0.0];
Sigma(3,2,:) = [3.4 0.0 0.0 0.38 0.0];
Sigma(3,3,:) = [4.3 0.0 0.0 0.27 0.0];
Sigma(3,4,:) = [4.4 0.0 0.0 0.0 0.0];

%Oxygen
Sigma(4,1,:) = [2.2 0.007 0.0 0.40 0.0];
Sigma(4,2,:) = [3.8 0.0 0.0 0.26 0.0];
Sigma(4,3,:) = [3.6 0.0 0.0 0.16 0.0];
Sigma(4,4,:) = [3.6 0.0 0.0  0.0 0.0];

%Sodium
Sigma(5,1,:) = [2.0 0.002 0.0 0.51 0.0];
Sigma(5,2,:) = [3.6 0.001 0.0 0.17 0.0];
Sigma(5,3,:) = [4.0 0.001 0.0 0.13 0.0];
Sigma(5,4,:) = [7.0 0.009 0.0 0.0 0.0];

%Chromium
Sigma(6,1,:) = [2.4 0.006 0.0 0.34 0.0];
Sigma(6,2,:) = [3.6 0.005 0.0 0.13 0.0];
Sigma(6,3,:) = [4.5 0.02 0.0 0.05 0.0];
Sigma(6,4,:) = [11.1 0.07 0.0 0.0 0.0];

%Iron
Sigma(7,1,:) = [2.2 0.007 0.0 0.4 0.0];
Sigma(7,2,:) = [2.8 0.005 0.0 0.08 0.0 ];
Sigma(7,3,:) = [5.1 0.010 0.0 0.03 0.0];
Sigma(7,4,:) = [7.2 0.03 0.0 0.0 0.0];

%Nickel
Sigma(8,1,:) = [2.3 0.073 0.0 0.31 0.0];
Sigma(8,2,:) = [4.4 0.01 0.0 0.11 0.0];
Sigma(8,3,:) = [12.7 0.03 0.0 0.06 0.0];
Sigma(8,4,:) = [21.1 0.05 0.0 0.0 0.0];

%Molybdenum
Sigma(9,1,:) = [3.6 0.02 0.0 0.75 0.0];
Sigma(9,2,:) = [7.0 0.06 0.0 0.11 0.0];
Sigma(9,3,:) = [8.2 0.13 0.0 0.06 0.0];
Sigma(9,4,:) = [9.5 0.7 0.0 0.0 0.0];

%Thorium 232
Sigma(10,1,:) = [4.5  0.08 0.07 1.2 2.34];
Sigma(10,2,:) = [7.4 0.19 0.0 0.21 0.0];
Sigma(10,3,:) = [11.8 0.41 0.0 0.05 0.0];
Sigma(10,4,:) = [15.3 1.48 0.0 0.0 0.0];

%Uranium 233
Sigma(11,1,:) = [4.4 0.03 1.81 0.93 2.69];
Sigma(11,2,:) = [6.4 0.17 2.05 0.12 2.52];
Sigma(11,3,:) = [11.5 0.30 2.74 0.04 2.5];
Sigma(11,4,:) = [17.9 0.74 6.49 0.0 2.5];

%Uranium 235
Sigma(12,1,:) = [4.6 0.06 1.2 0.79 2.69];
Sigma(12,2,:) = [7.4 0.3 1.3 0.2 2.46];
Sigma(12,3,:) = [12.5 0.6 1.9 0.04 2.43];
Sigma(12,4,:) = [18.7 2.0 5.0 0.0 2.42];

%Uranium 238
Sigma(13,1,:) = [4.6 0.06 0.32 1.39 2.77];
Sigma(13,2,:) = [7.8 0.13 0.0 0.22 0.0];
Sigma(13,3,:) = [12.1 0.35 0.0 0.05 0.0];
Sigma(13,4,:) = [12.9 0.9 0.0 0.0 0.0];

%Plutonium 239
Sigma(14,1,:) = [4.9 0.02 1.83 0.83 3.17];
Sigma(14,2,:) = [7.5 0.16 1.55 0.13 2.92];
Sigma(14,3,:) = [12.0 0.45 1.63 0.07 2.88];
Sigma(14,4,:) = [17.4 2.4 3.25 0.0 2.87];

%Plutonium 240
Sigma(15,1,:) = [4.8 0.07 1.59 0.74 3.18];
Sigma(15,2,:) = [7.4 0.18 0.27 0.22 2.95];
Sigma(15,3,:) = [12.0 0.5 0.07 0.05 2.88];
Sigma(15,4,:) = [17.2 2.1 0.13 0.0 2.87];

%Plutonium 241
Sigma(16,1,:) = [4.8 0.08 1.65 0.82 3.23];
Sigma(16,2,:) = [8.0 0.2 1.72 0.31 2.98];
Sigma(16,3,:) = [12.6 0.48 2.48 0.05 2.94];
Sigma(16,4,:) = [19.8 1.74 6.32 0.0 2.93];

%Plutonium 242
Sigma(17,1,:) = [4.5 0.04 1.46 0.65 3.12];
Sigma(17,2,:) = [7.1 0.12 0.17 0.18 2.89];
Sigma(17,3,:) = [12.6 0.33 0.04 0.05 2.81];
Sigma(17,4,:) = [22.0 1.54 0.02 0.0 2.81];

%Fission Products
Sigma(18,1,:) = [7.8 0.05 0.0 1.83 0.0];
Sigma(18,2,:) = [11.4 0.17 0.0 0.2 0.0];
Sigma(18,3,:) = [14.7 0.50 0.0 0.09 0.0];
Sigma(18,4,:) = [19.1 1.88 0.0 0.0 0.0];


a=Sigma(i,j,k);



        
        
