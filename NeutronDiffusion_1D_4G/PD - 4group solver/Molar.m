%Funkcja zwraca mase molowa/atomowa zadanego materia�u 1..18
%Jednostka [g/mol]
function [M]=Molar(i);
%�r�d�o danych: http://atom.kaeri.re.kr/ton/      Nuclide Table
%Masa molowa Boronu - mieszaniny z udzia��w molowych
%argument i to numer pierwiastka taki jak w funkcji Sigma
%{  
    Argmuenty funkcji:
    i - numer pierwiastka  1...18
        1 - Boron 
        2 - Boron-10
        3 - Carbon 
        4 - Oxygen
        5 - Sodium
        6 - Chormium
        7 - Iron
        8 - Nickel
        9 - Molybdenum
        10 - Thorium 232
        11 - Uranium 233 
        12 - Uranium 235
        13 - Uranium 238 
        14 - Plutonium 239
        15 - Plutonium 240
        16 - Plutonium 241
        17 - Plutonium 242 
        18 - Fisson Products (pairs) 
%}

if (i>18)
    error('Nie ma takiego materialu')
end
mBoron = 10.0129370*0.199+0.801*11.0093055;
Molar(1:18)=0.0;

Molar(1)=mBoron;
Molar(2)=10.0129370;
Molar(3)=12.0000000;
Molar(4)=15.9949146;
Molar(5)=22.9897697;
Molar(6)=52.9406538;
Molar(7)=55.934942;
Molar(8)=57.9353479;
Molar(9)=97.9054078 ;
Molar(10)=232.0380504;
Molar(11)=233.0396282;
Molar(12)=235.0439231;
Molar(13)=238.0507826 ;
Molar(14)=239.0521565;
Molar(15)= 240.0538075;
Molar(16)=241.0568453;
Molar(17)=242.0587368;
Molar(18)=(90+100+130+140)*0.5;  %�rednia z najcz�ciej wyst�puj�cych mass atomowych produkt�w rozszczepienia

M=Molar(i);



