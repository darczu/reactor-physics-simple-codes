%{
%Na dzie� dzisiejszy w kodzie najpewniej jest b��d wsp. mno�enia jest zbyt ma�y.
%Aktualne poprawki 10.02.2012 rok. Kolejne 14.02.2012.
Start - pocz�tek 2012 modyfikacje 25, 31.01.2012

Praca przejsciowa 2011-2012 Semestr Zimowy 
Piotr Darnowski Studia Magisterskie Energetyka Jadrowa Wydzial Mechaniczny Energetyki i Lotnictwa
Program/Skrypt w Matlabie do rozwiazywania wielogrupowego (4 grupy) przyblizenia dyfuzyjnego w jednym wymiarze
dla reaktora pr�dkiego ch�odzonego ciek�ym sodem (LMFBR) z paliwem tlenkowym MOX.
G��wna gemetria: walec stanowiacy przyblizony typowy ksztalt reaktora 
Dodatkowo zastosowane jest przyblizenie aproksymujace osiowa ucieczke neutron�w tzn. Transverse Leakage Approximation.
Ilo�� grup neutron�w: 4
 
Algorytm i g��wne �r�d�o danych: A.E. Waltar, Fast Breeder Reactors, Pergamon Press 1981
Metody analogiczne do zastosowanych w profesjonalnych duzo bardziej skomplikowanych kodach 1DX i MINX opisanych w ksi��ce Waltara. 
Reaktor podzielony/zdyskretyzowany jest na N przedzia��w. Punkty obliczeniowe znajduj� si� w �rodku ka�dego podzia�u. Dodatkowo wprowadzono 3 strefy rdzen centralny, obrzeza rdzenia oraz p�aszcz promieniowy.
Zastosowane jest przybli�enie osiowej ucieczki neutron�w przez fundamentalne rozwi�zanie -B_z^2 * phi tak jak w �r�dle.
Do g��wnego skryptu do��czone s� dodatkowe programy.
Molar   - Zawiera masy molowe/~liczby atomowe 18 pierwiastk�w  
Sigma   - Zawiera przekroje czynne mikroskopowe na r�ne reakcje dla 18 pierwiastk�w
Sigma_s - Zawiera przekroje czynne na przej�cia pomi�dzy grupami w reakcjach rozpraszania
Rho     - Zawiera gesto�ci (w r�nych warunkach! trzeba tu zachowa� ostro�no��)

g - numer grupy neutronu
         1 - E >820 keV
         2 - 110 keV < E < 820 keV
         3 - 15 keV < E < 110 keV
         4 - 0 keV < E < 15 keV
k - element podzia�u (N podzia��w)

Dostepne pierwiastki/materia�y dla danych materia�owych + Dane paliw MOX, stali SS 316  oraz w�gliku boru B4C. Dane pierwiastk�w zawarte w dodatkowych funkcjach matlabowskich dane materia��w -miszanin  w kodzie.
        1 - Boron
        2 - Boron-10      
        3 - Carbon 
        4 - Oxygen
        5 - Sodium
        6 - Chormium
        7 - Iron
        8 - Nickel
        9 - Molybdenum
        10 - Thorium 232
        11 - Uranium 233 
        12 - Uranium 235
        13 - Uranium 238 
        14 - Plutonium 239
        15 - Plutonium 240
        16 - Plutonium 241
        17 - Plutonium 242 
        18 - Fisson Products (pairs) 
Jednostki typowo stosowane w obliczeniach neutronowych w uk�adzie CGS:
Wymiar: [cm]
Objeto��: [cm3]
Pole: [cm2]
Przekr�j czynny: [barn]  1 barn = 10^-24 cm2
Strumie� neutronow:  [n/cm2s]
Gestosc: [g/cm3]
Dodatkowo stosowany jest tzn. barn-cm poprzez zastapienie liczby Avogadra jej warto�ci� podzielon�przez 10^24
%}

format long e                           %Format zapisu danych
clear                                   %Czyszczenie ekranu
clc
tic                                     %Start timera
%Wst�pne Dane 
G = 4;                                 %Ilo�� grup energetycznyc
R_stref=[117.0 42.0 40.0];             %[cm] grubosci ka�dej z stref rdzenia 
R_0 = sum(R_stref);                    %[cm] Promie� rdzenia - suma grubo�ci

H = 100.0;                             %[cm] Wysokosc rdzenia                  
delta_r = 20.0;                        %[cm] Promieniowy wymiar ekstrapolowany rdzeni - za�o�ony i identyczny dla ka�dej grupy neutron�w
delta_z = 20.0;                        %[cm] Osiowy wymiar ekstrapolowany rdzenia - za�o�ony przyj�ty jednakowy dla ka�dej grupy
B_r = 2.405/(R_0+delta_r);             %[1/cm] Promieniowy parametr geometryczny reaktora walcowego - Geometrical Buckling 
B_z = pi/(H+2*delta_z);                %[1/cm] Osiowy parametr geometryczny reaktora walcowego
%N_A = 6.023e23;                       %[atom/g mol]  Sta�a Avogadra 
N_A = 0.6023;                          %Sta�a Avogadra/10^24
V_tot=  H*pi*R_0^2;                    %[cm3] ca�kowita obj�to�� rdzenia
h =[1 2 3];                            %Strefy rdzenia  %size(h,2)ilo�� stref - 3
R_s=R_stref;                         

V_stref=[0 0 0];
V_stref = pi.*H.*[R_s(1).^2  R_s(2).^2-R_s(1).^2  R_s(3).^2-R_s(2).^2];   %[cm3] Obj�to�ci stref rdzenia
if(V_stref(2)<=0.0)                                                       %Zerowanie na wypadek braku strefy (odejmowanie!)
 V_stref(2)=0.0;    
elseif(V_stref(3)<=0.0)
 V_stref(3)=0.0;
end 
 
chi = [0.76 0.22 0.02 0];           %Chi - udzia� danej grupy energetycznej w widmie rozszczepieniowym [1 2 3 4] �r�d�o: FBR Waltara
chi=chi';                           %Transpozycja wektora wierszowego do kolumnowego - wektory kolumnowe bardziej intuicyjne

%Sk�ad Reaktora
%Trzeba podzieli� rdze� na szereg cz�ci - p�aszcz rdze�/ obszary o r�nym
%wzbogaceniu w pierwszej wersji programu wybieramy trzy strefy promieniowe:1 - �rodkowa,
%2 - Zewn�trzna oraz 3- P�aszcz.
%Stefy charakteryzuja sie roznym wzbogaceniem i generalnie rozna
%zawartoscia pierwiastk�w wszelakiego typu
%Tworzymy macierze zawierajace dane dotyczace sk�adu tych stref.
%F_m - udzia� obj�to�ciowy danego materia�u (1..18) 

%Obliczenia niektorych wielkosci tutaj odbywaja sie niejako poza
%dodatkowymi funkcjami z tablicami materialowymi (Molar etc...) dla roznych pierwiastkow - niestety jest to konieczne 
%Typowy sk�ad obj�to�ciowy rdzenia Paliwo 30-45% S�d 35-45% Stal 15-20% B4C 1-2%
%Paliwo MOX typowo sk�ada si� z 20-25% PuO2 reszta to zubozony tlenek U238. 
%Na obecnym etapie paliwo jest �wie�e - tylko Pu239 i tylko U238 bez innych
%izotop�w i produkt�w rozszczepienia.

%DYGRESJA - niekotre wazniejsze rownania zastosowane �r�d�o: KTH Reactor Physics Lectures
%1/M = suma(w_i/M_i)
%M=suma(f_i*M_i)
%rho_i=w_i*rho
%rho_MOX=w_puo2*rho_UO2+w_uo2*rho_PuO2
%f_UO2=0.2 w tym wypadku i f_PuO2=0.8
%Przelicza sie w_i=f_i*M_i / M
%Ogolnie gdy np mamy Fe3O2 to N_Fe=3*N_Fe3O2  i  N_O=2*N_Fe3O2  a udzial
%maswoy  w_Fe = 3*M_fe /(3*M_fe + 2*M_O) !!!
%Example N235=rho_235*N_A/M_235 
%Ponizsze obliczenia opieraja sie na rozwazaniach z wyk�ad�w KTH, niestet
%FBR Waltara niewiele m�wi o tych zagadnianiach - cho� co jasne to s�
%podstawy podstaw fizyki reaktor�w, aczkolwiek jak pokaza�a praktyka
%sprawiaja� nawi�cej trudno�ci i to w samym Waltarze s� ma�e rozbie�no�ci/niejasno�ci.
%Bardzo u�yteczny opis znajduje si� w dokumencie Criticality Calculations with MCNP5:A Primer
%////////////////////////////////////////////////////////////////////////
f_UO2  = 0.75;  f_PuO2=0.25;                %MOX sk�ad obj�to�ciowy paliwa
%///////////////////////////////////////////////////////////////////////

M_UO2  = Molar(13)+2*Molar(4);              %masa molowa tylko UO2 
M_PuO2 = Molar(14)+2*Molar(4);              %masa molowa tylko PuO2
M_MOX  = f_UO2*M_UO2 + f_PuO2*M_PuO2;       %Masa molowa/atomowa paliwa jadrowego ok 270

w_PuO2 = f_PuO2*M_PuO2/M_MOX;                %przeliczenie udzialow objetosciowych na udzialy masowe
w_UO2  = f_UO2*M_UO2/M_MOX;                  %udzial masowy tlenku uranu w Moxie

%KTH Reactor PHysics Lectures podaje wz�r: w_i= n*M_i/(n*M_i+m*M_j) gdzie n i m to ilo�ci atom�w w cz�steczce, M to masy molowe odpowiednie

w_U238  = Molar(13)/(M_UO2);                %udzial masowy uranu U238 w UO2
w_Pu239 = Molar(14)/(M_PuO2);               %udzial masowy plutonu jezeli caly pluton to Pu239 w PuO2      

w_U238_MOX = w_UO2*w_U238;                  %Udzial masowy uranu238 w Moxie
w_Pu239_MOX = w_PuO2*w_Pu239;               %Udzial masowy plutonu 239 w Moxie

w_O_UO2  = 2*Molar(4)/(M_UO2);              %udzia� tlenu w tlenku uranu
w_O_PuO2 = 2*Molar(4)/(M_PuO2);             %udzial tlenu w tlenku plutonu
w_O_MOX =  2*Molar(4)/(M_MOX);              %udzia� masowy tlenu w MOXie

Rho_TD_UO2  =10.97;                         %[g/cm3] gesto�� teoretyczna tlenku uranu
Rho_TD_PuO2 =11.46;                         %[g/cm3] g�sto�� teoretyczna tlenku plutonu
Rho_TD_MOX = Rho_TD_UO2*w_UO2 + Rho_TD_PuO2*w_PuO2;               %[g/cm3] g�sto�� teoretyczna MOXu. Przyk�ad: Rho_TD_MOX  = 11.08 g/cm3;   %80%U i 20%Pu
TD = 0.95;                                                        %Procent teoretycznej gestosci paliwa dla paliwa zastosowanego     
SD = 0.90;                                                         %Smear Density - g�sto�� "wype�niania paliwa" - w obliczeniach neutronowych przyjmuje sie ze paliwo wypelnia cala koszulke 
Rho_MOX = Rho_TD_MOX*SD;                                          %[g/cm3] gestosc paliwa jadrowego tlenkowego UO2 lub PuO2 do obliczen j�drowych

M_B4C = 4*Molar(2)+Molar(3);                                      %Obliczenia masy molowej W�gliku Boru B4C poch�anioacza w reaktorach predkich tylko BOR 10
Rho_B4C=2.52;                                                     %[g/cm3] g�sto�� B4C
w_B = 4*Molar(2)/(4*Molar(4)+Molar(3));                           %udzial masowy boru 10 w B4c
w_C = 1*Molar(3)/(4*Molar(4)+Molar(3));                           %udzial masowy w�gla w B4C

w_Na = 1.0;                                                        %Przyjmujemy, �e w sodzie jest tylko s�d Na 23 czyli jego udzial masowy = 1.0
M_Na=Molar(5);                                                     %Masa molowa Sodu
Rho_Na=Rho(5);                                                     %G�sto�� sodu

%Stal SS316 stosowana w reaktorach pr�dkich: Fe, <0.03% C, 16-18.5% Cr, 10-14% Ni, 2-3% Mo, <2% Mn, <1% Si, <0.045% P,
%<0.03% S dla stali te wsp�czynniki s� masowo; pomini�to pomniejsze pierwiastki pod uwage brana jest: Fe, Cr, C, Mo, Ni �r�d�o:http://www.azom.com/article.aspx?ArticleID=863

w_Cr=0.17; w_Ni=0.12; w_C_steel=0.0003; w_Mo=0.02;                %Udzia�y masowe chromu, niklu, stali i molibdenu
w_Fe=1 - w_Cr - w_Ni - w_C - w_Mo;                                %reszta sk�adu to �elazo
M_steel = ( w_Fe/Molar(7)+w_Cr/Molar(6)+w_Ni/Molar(8)+w_C_steel/Molar(3)+w_Mo/Molar(9) )^(-1);         %Masa molowa stali
Rho_steel=8.00;                                                   %[g/cm3] g�sto�� dla stali SS316                        

%Masa molowa, gesto�c i udzia� masowy materia�u rodnego 100% U238 w postaci UO2.
M_fertile = M_UO2;
Rho_fertile = Rho_TD_UO2*SD;
w_fertile = 1.0;

%Materia�y w rdzeniu cd..

Rho_m(3,5)=0.0;                                                   %Wyzerowanie tablicy g�sto�ci - deklaracja
%Rho_mm=[Rho_MOX  Rho(5) Rho_steel Rho_B4C Rho_TD_UO2];            %Wektor g�sto�ci dla reaktora ()  Rho(5) g�sto�� Sodu2
for(i=1:3)
 Rho_m(i,:)=[Rho_MOX  Rho(5) Rho_steel Rho_B4C Rho_TD_UO2];       %Gestosci 5 glownych skladnikow rdzenia - tablica o 3 takich samych wierszach (wiersz to wektor Rho_m) pomocna w dalszych obliczeniach
end

%Udzialy objetosciowe  F_m(i,j)  i-strefa rdzenia, j-materia�: Materia�y 1..18 
F_i(size(h,2),18)=0.0;   %Pocz�tkowo zerujemy wszystko - macierz udzia��w obj�to�ciowych - ta macierz jest dla pierwiastk�w F_m(STREFA 1...3, PIERWIASTEK 1...18)
%Dla tych materia��w mamy baze przekroj�w mikroskopowych

%Tworzymy dodatkowa macierz tylko dla paliwa, pochlaniacza, materialu konstrukyjnego i chlodziwa i %materialu rodnego
%Te materia�y to mieszaniny pierwiastk�w z tablicy F_i.
F(size(h,2),5)=0.0;    %F(Strefa 1..3, MATERIA� 1..5) Gdzie pierwszy argument materia�u to: 1- Paliwo  2-S�d/chlodziwo  3-Stal/Material konstrukcyjny   4-B4C/pochlaniacz   5 - Materia� rodny U238
%1 - Paliwo
%2 - S�d
%3 - Stal
%4 - Pochlaniacz
%5 - Materia� paliworodny
%Tworzymy macierze w i N gdzie w to udzia�y masowe oraz N ilo�� j�der inne
%dla kazdej strefy. w(i,j) N(i,j) i-strefa 1..3, k-materia� 1..18 
%Inicjalizacje macierzy
w_i(3,18)=0.0;          %Macierz udzia��w masowych dla wszystkich pierwiastk�w
N_i(3,18)=0.0;          %Macierz ilo�ci atom�w konkretnych pierwiastk�w
V_m(3,5)=0.0;           %Macierz obj�to�ci stref reaktora - 3 strefy po 5 g��wnych materia��w na stref�.
masa_m(3,5)=0.0;        %Macierz mas materia�u w danej strefie (paliwo, poch�aniacz etc)
Rho_i(3,18)=0.0;        %Macierz g�sto�ci danych materia��w w danej strefie rdzenia
%F


%#STREFA 1 �RODEK RDZENIA 
%udzialy objetosciowe rdzenia F
F(1,1)=0.40;     %paliwo
F(1,2)=0.35;     %chlodziwo
F(1,3)=0.15;     %stal   
F(1,4)=0.01;     %pochlaniacz
F(1,5)=0.09;     %material rodny
%F(1,5)=1 - F(1,1)-F(1,2)-F(1,3)-F(1,4);  %materia� rodny
if(sum(F(1,:)) ~= 1)
    disp('udzialy nie sumuja sie do jednosci!!')
end
V_m(1,:)=F(1,:).*V_stref(1); %obj�to�ci danego materia�u w stefie w strefie = udzia� obj�to�ciowy danego materia�u * obj�to�� strefy

%#STERFA 2 ZEWNETRZNA CZESC RDZENIA o wyzszym wzbogaceniu
F(2,1) = 0.45; %paliwo
F(2,2) = 0.35; %chlodziwo
F(2,5) = 0.10; %material rodny
F(2,4) = 0.03; %pochlaniacz
F(2,3) = 0.07; %stal
%F(2,3)=1 - F(2,1)-F(2,2)-F(2,5)-F(2,4); %stal to reszta
if(sum(F(2,:)) ~= 1)
    disp('udzialy nie sumuja sie do jednosci!!')
end
V_m(2,:)=F(2,:).*V_stref(2);


%#STREFA 3 PLASZCZ
F(3,5)=0.5;                    %ilosc materialu paliworodnego
F(3,1)=0.0;                    %paliwo   
F(3,2)=0.2;                   %chlodziwo 
F(3,4)=0.001;                   %pochlaniacz
F(3,3)=1 - F(3,4) - F(3,5) - F(3,2) - F(3,1);  %stal
if(sum(F(3,:)) ~= 1)
    disp('udzialy nie sumuja sie do jednosci!!')
end
V_m(3,:)=F(3,:).*V_stref(3);

%Trzeba wyznaczyc liczby j�der w 1cm3 
%N_s = N_A * gestosc/masa atomowa
%Znane s� g�sto�ci ka�dego materia�u w rdzeniu - za�o�one
%ge�to�� ka�dego pierwiastka to gesto�� materia�u * udzia� masowy danego
%pierwiastka czyli:   rho_m = w_m*rho_s
%Obliczamy ilo�ci c�asteczek ka�dego materia�u w ka�dej z trzech stref

% w_i(i,:) = [0.0 w_B w_C+w_C_steel w_O_PuO2 + w_O_UO2 w_Na w_Cr w_Fe w_Ni w_Mo 0.0 0.0 0.0 w_U w_Pu 0.0 0.0 0.0 0.0]
%Niektore tablice mog�y wcze�niej by� zadeklarowane ale w trakcie tworzenia kodu zosta�y nieumy�lnie powt�rzone
N_m(3,5)=0.0;                                              %inicjalizacja
M_m = [M_MOX M_Na M_steel M_B4C M_fertile];                %Masy molowe materia��w
Rho_m =[Rho_MOX Rho_Na Rho_steel Rho_B4C Rho_fertile];     %G�sto�ci materia��w
N_m = Rho_m.*N_A./M_m;                                     %ilo�� cz�steczek danego materia�u !!! nie j�der!!!

w_ii(3,5,18)= 0.0;  %Macierz pierwiastk�w 3 strefy, 5 materia��w, 18 pierwistk�w %bardzo og�lne podej�cie/ mozna prosciej mniej ogolnie

%w_ii wiekszosc to zera - de facto wykonujemy mase mnozen zero co moze
%zakrawac o bezsens ale mamy relatywnie og�lna metod�.
%W kazdej strefie udzial masowy przyjmujemy taki sam, 
%w_ii(1:3,1,:)= [0.0 0.0 0.0 w_O_MOX 0.0 0.0 0.0  0.0 0.0 0.0 0.0 w_U238_MOX w_Pu239_MOX 0.0 0.0 0.0 0.0];   %udzialy masowe w paliwie
%1. PALIWO UDZIALY MASOWE
w_ii(1:3,1,4) = w_O_MOX;            %Zawartosc tlenu w paliwie
w_ii(1:3,1,13)= w_U238_MOX;         %Zawartosc uranu 238 w paliwie
w_ii(1:3,1,14)= w_Pu239_MOX;

%2. CHLODZIWO UDZIALY MASOWE
w_ii(1:3,2,5) = w_Na;

%3. MATERIA�Y KONSTRUKCYJNE UDZIALY MASOWE
w_ii(1:3,3,3) = w_C_steel;
w_ii(1:3,3,6) = w_Cr;
w_ii(1:3,3,7) = w_Fe;
w_ii(1:3,3,8) = w_Ni;
w_ii(1:3,3,9) = w_Mo;

%4. POCH�ANIACZ UDZIA�Y MASOWE
w_ii(1:3,4,2) = w_B;  %Bor 10
w_ii(1:3,4,3) = w_C; %Wegiel

%5. MATERIA� PALIWORDONY UDZIALY MASOWE
w_ii(1:3,5,13) = w_fertile;

% W tym podejsciu trzeba utworzyc dodatkowo dwie macierze dla M_mm i Rho_ii o  takim samym rozmiarze jak w_ii
Rho_mm=[];
Rho_mm(3,5,18) = 0.0;
M_ii(3,5,18)   = 0.0;
N_ii(3,5,18)   = 0.0;

for(k=1:3)
 for(j=1:18)
  for(i=1:5)
   Rho_mm(k,i,j)=Rho_m(i);  %Mozna to prosciej wypelnic
  end
 end
end

for(k=1:3)
 for(j=1:18)
  for(i=1:5)
    M_mm(k,i,j)=M_m(i);  %Mozna to prosciej wypelnic
  end
 end
end

for(i=1:18)
 M_ii(1:3,1:5,i) = Molar(i);
end

%Dodatkowe przeliczenia gestosci efektywnej  - mozna to podwazyc nie jestem w 100% tego pewny
Rho_m3(3,5)=0.0;
for(i=1:5)
 Rho_m3(1:3,i)=Rho_m(i);  %Macierz 3 wierszowa w kazdym wierszu Rho_m
end

%gestosc efektywna = gestoc materialu * udzial objetosciowy materialu w
%ca�o�ci obj�to�ci strefy pozniej wykorzystujemy iloczyn gestosci
%efektywnejw danej strefie i udzialu masowego do obliczenia ilosci czastek.
%Sam materia� ma jak�s ge�to�� np MOX 11 g/cm3 ale jak zalozymy ze to
%paliwo jest w ca�ej obj�to�ci rdzenia (homogenizacja) to gestosc nam
%zmaleje o stosunek objetosci rzeczywistej MOX do objetosci calego rdzenia
%czyli de facto F.

Rho_eff = Rho_m3.*F;
Rho_eff_i(3,5,18)=0.0;

for(i=1:18)
 Rho_eff_i(1:3,1:5,i) = Rho_eff;   %18 tablic 3x5 zawierajacych gestosci materialow np tlen lub uran moga byc w dwoch roznych metrialach dlatego tak to dziala
end

N_ii = N_A.*w_ii.*Rho_eff_i./M_ii;  %Macierz ilo�ci j�der w zalezno�ci od strefy, materia�u i pierwiastka
%Teraz sumujemy po materia�ach i dostajemy ilo�� j�der danego pierwiastka w strefie
%wiersze to strefy, kolumny to materia�y i 18 takich tablic daje nam ca�o��
%N_ii. Dla naszych obliczen sumujemy wpierw po kolumnach
%Ilo�ci cz�stek: N_i=Rho_i *N_A / M_i ,  gdzie: Rho_i = Rho_m * w_i 
N_i=[];
GG=[];
GG=sum(N_ii,2);      %dostajemy osiemnascie macierzy (wektorow 3x1 kolumnowych)  tak jakby to byla macierz (3,1,18) z sumami kolumn i trzeba sklecic te macierze w jedna macierz 3 na 18

%Macierz 3x18 z ilosciami j�der (1..18) danego pierwiastka w zaleznosci od strefy (1...3)
for(i=1:18)
 N_i = [N_i GG(1:3,1:1,i)];  %Macierze 3x18 3 steefy po 18 pierwiastkow - ilo�� cz�stek
end
N_i=N_i';   %18x3
%Znajmosc ilosci jader pozwala na wyliczenie makroskopowych przekrojow
%czynnych i w konsekwencji wsp�czynnik�w dyfuzjii neutron�w dla przybli�enia czterogrupowego.  
N_i_tr(18,4,3)=0.0;

for(i=1:4)
 N_i_tr(:,i,:)=N_i(:,:);
end
N_i_f=N_i_tr; %Ta sa te same macierze zarowno dla tranposrtu jak i rozszczepien

%Ilosci neutronow powstajacych na jedno rozszczepienie w zaleznosci od pierwiastka i od grupy
ni(18,4) = 0.0;
ni = Sigma(1:18,1:4,5);   


%for(i=1:18)
%    for(j=1:4)
%        Sigma_sc(i,j)=Sigma_s(i,j);  %Macierz rozproszen
  %  end
%en%d


%Przekroje transportowe
Sigma_tr_micro(18,4) = 0.0;
Sigma_f_micro(18,4)  = 0.0;
Sigma_sc_micro(18,4) = 0.0;
Sigma_a_micro(18,4) =0.0;

Sigma_tr_micro = Sigma(1:18,1:4,1);  %Mikroskopowy przekroj czynny transportowy dla 18 pierwiastkow i 4 grup
Sigma_f_micro  = Sigma(1:18,1:4,3);
Sigma_sc_micro = Sigma_s(1:18,1:4);
Sigma_a_micro = Sigma(1:18,1:4,2) + Sigma(1:18,1:4,3) + Sigma(1:18,1:4,4);

Sigma_f_micro2 = Sigma_f_micro;       %zachowanie tej tablicy
Sigma_f_micro  = Sigma_f_micro.*ni;    %Iloczyn makroskopowego przekroju czynnego i ilosci neutronow rozszczepieniowych
Sigma_f_micro3 = Sigma_f_micro;       %Zachowanie tej tablicy w takiej postaci


for(i=1:3)
 Sigma_tr_micro(1:18,1:4,i) = Sigma_tr_micro(1:18,1:4);   %3 razy to samo!!! (18x4x3)
 Sigma_f_micro(1:18,1:4,i)  = Sigma_f_micro(1:18,1:4);
 Sigma_sc_micro(1:18,1:4,i) = Sigma_sc_micro(1:18,1:4);
 Sigma_a_micro(1:18,1:4,i)  = Sigma_a_micro(1:18,1:4);
end

%Simga_tr - makroskopowy przekr�j czynny
Sigma_tr(18,4,3) = 0.0;        %1 barn = 10^-24 cm2
Sigma_f(18,4,3)  = 0.0;
Sigma_sc(18,4,3) = 0.0;
Sigma_a(18,4,3)  = 0.0;

%Sigma_tr = Sigma_tr_micro.*N_i_tr.*10e-24;   %Macierz tr�jwymiarowa 18x4x3 z wartosciami makroskopowych przekrojow czynnych z podzialem na strefy i na grupy neutronow oraz pieriwastki
Sigma_tr = Sigma_tr_micro.*N_i_tr;
%Sigma_f  = Sigma_f_micro.*N_i_f.*10e-24;   %Makroskopowe przekroje czynne na rozszczepienie z uwzglednieniem ze 1barn=10^-24 cm2
Sigma_f  = Sigma_f_micro.*N_i_f;
%Sigma_sc = Sigma_sc_micro.*N_i_tr.*10e-24; %Makroskopowe przekroje czynne na rozpraszanie
Sigma_sc = Sigma_sc_micro.*N_i_tr;
%Sigma_a  = Sigma_a_micro.*N_i_tr.*10e-24;
Sigma_a  = Sigma_a_micro.*N_i_tr;


%Teraz sumujemy po pierwiastkach
Sigma_tr1 = sum(Sigma_tr,1);   %3 wektory  4x1 trzeba zamienic na jedna tabilce 3x4
Sigma_f1  = sum(Sigma_f,1);
Sigma_sc1 = sum(Sigma_sc,1);
Sigma_a1  = sum(Sigma_a,1);
Sigma_tr_macro=[];
Sigma_f_macro =[];
Sigma_sc_macro=[];
Sigma_a_macro =[];

for(i=1:3)
   Sigma_tr_macro = [Sigma_tr_macro; Sigma_tr1(:,:,i)];     %MAcierz 3x4 z makroskopowymi przekrojami transportowymi dla 3 stref i 4 grup neutron�w
   Sigma_f_macro  = [Sigma_f_macro;  Sigma_f1(:,:,i) ];
   Sigma_sc_macro = [Sigma_sc_macro; Sigma_sc1(:,:,i)];
   Sigma_a_macro  = [Sigma_a_macro;  Sigma_a1(:,:,i)];
end

D_g(3,4)=0.0;
D_g=1./(3.*Sigma_tr_macro);  %Wsp�czynniki dyfuzji neutron�w [cm]

%Macierz srednich dr�g swobodnych transportu
lambda_tr = [];
lambda_tr = 3.*D_g;
lambda_tr_avg=[mean(lambda_tr(:,1)) ,  mean(lambda_tr(:,2)), mean(lambda_tr(:,3)), mean(lambda_tr(:,4))];






%Walec/ Dane wynikajace z elementarnych rozwaza� teorii dyfuzjii
%jednogrupowej. Przybli�enie: Transverse Leakage Aproximation -
%Prostopad�em przybli�enie ucieczki neutron�w - przej�cie z problemu
%dwuwymiarowego do problemu jednowymiarowego
% | 1 | 2 | 3 |...| k-1 | k | k+1 |... | N-1 | N |
%r=0                    |   |                   r=R
%---------------------->r(k-1/2)
%-------------------------->r(k+1/2)
%---------------------->|   |<-------------- dr(k)
%---->| r(2-1/2) 
%-------->| r(2+1/2)
%----------------------------------------------->|r(N+1/2)

%W kodzie
% | 1 | 2 | 3 |...| k-1 | k | k+1 |... | N-1 | N |
%r=0              |     |   |                   r=R
%r(1)=0         r(k-1) r(k)                    r(N+1)=R_0

%!!!!!!!!! W kodzie r(k) == r(k-1/2)  w �r�dle !!!!!!!!

%podzial=1...k-1,k,k+1....N
%Ilo�� | - lini podzia�u N+1 przypisane sa im pola i objetosci - teoria w
%raporcie 

%Podzia� obszaru obliczeniowego na N przedzia��w o jednakowych d�ugo�ciach
%Kod og�lnie napisany dla podzia��w o dowolonej d�ugo�ci
N = 100;                               %Ilo�� podzia��w domeny obliczeniowej
k = linspace(1,N,N)';                  %Indeks k numerujacy przedzia�y
g = [1 2 3 4]';                        %Wektor numeracji grup energii
r = linspace(0,R_0,N+1)';                 %[cm] ten wektor odpowiada kolejnym po�o�eniom przedzia��w siatki
rr = r(1:N);                               %Zmodyfikowany wektor po�o�en od 5-495 cm 99 element�w 
rr_col =  [rr; rr; rr; rr];
rr = [rr, rr, rr, rr];
dr = R_0/(N);                             %[cm] d�ugo�� elementarnego podzia�u dla siatki r�wnomiernej - w naszym modelu constans

%Warto�ci P�l oraz obj�to�ci
%ilo�� warto�ci jest N+1 
A(1:N+1)=0.0;                        %pole odpowiadajace po�o�eniom
A = 2.*pi.*r.*H;                     %[cm2] wektor p�l A w kodzie to A(k-1,k) w �r�dle
A=A';
V(1:N)=0.0;                          %Objeto�ci odpowiadajace danym przedzia�om
V(k) = pi.*H.*( - r(k).^2 + r(k+1).^2);  %[cm3] obj�to�ci
V=V';
v2(k) = ( -r(k).^2 + r(k+1).^2);        %[cm2] Roznice kwadratow promieni dla danego przedzia�u - stosowane w obliczeniach
v2=v2';
v2_col=[v2; v2; v2; v2];   %4 razy po 100 v2
v22 =[v2 v2 v2 v2];     
FF = 0.0;

%Wsp�czynnik Dyfuzji - obliczana na bazie mikroskopowych przekroj�w czynnych oraz ilo�ci atom�w danego materia�u
%Mamy trzy strefy pojawia si� problem strefy przej�ciowej pomiedyz strefami
%W kazdej strefie wsp�czynniki dyfuzji sie nie zmieniaja

%Wsp�czynniki w w�z�ach- w�z��w jest  N (test N=100)
%R_stref=[150.0 200.0 150.0]; %D_g(3,4);
%Mikroskopowe tez sa potrzebne
Sigma_tr_gk =[];
Sigma_f_gk  =[];
Sigma_sc_gk  =[];
Sigma_a_gk = [];
D_gk=[];
%Sigma pomiedzy pktami k i k-1
for(j=1:N)
   if(r(j)<=R_s(1))
     Sigma_tr_gk  = [Sigma_tr_gk; Sigma_tr_macro(1,:)];
     Sigma_f_gk   = [Sigma_f_gk;  Sigma_f_macro(1,:)];
     Sigma_sc_gk  = [Sigma_sc_gk; Sigma_sc_macro(1,:)];
     Sigma_a_gk   = [Sigma_a_gk; Sigma_a_macro(1,:)];
     D_gk=[D_gk; D_g(1,:)];
     
   elseif( (r(j)<=(R_s(1)+R_s(2)))& (r(j)>R_s(1)))
      Sigma_tr_gk  = [Sigma_tr_gk; Sigma_tr_macro(2,:)];
      Sigma_f_gk   = [Sigma_f_gk;  Sigma_f_macro(2,:)];
      Sigma_sc_gk  = [Sigma_sc_gk; Sigma_sc_macro(2,:)];
      Sigma_a_gk   = [Sigma_a_gk; Sigma_a_macro(2,:)];
      D_gk=[D_gk; D_g(2,:)];
      
   elseif( (r(j)>(R_s(2))+R_s(1)) & (r(j)<=R_0))
      Sigma_tr_gk  = [Sigma_tr_gk; Sigma_tr_macro(3,:)];  %Macierze 100x4 
      Sigma_f_gk   = [Sigma_f_gk;  Sigma_f_macro(3,:)];
      Sigma_sc_gk  = [Sigma_sc_gk; Sigma_sc_macro(3,:)];
      Sigma_a_gk   = [Sigma_a_gk; Sigma_a_macro(3,:)];
      D_gk=[D_gk; D_g(3,:)];
      
   else
   end    
end    
%D_gk w sumie nie potrzebne bo sie inaczej przelicza!!
%Powstaja macierze Nx4 (tutaj np 100x4) zawierajaca czterogupowe przekroje
%dla kazdego wezla i czterogurpowe wspolczyynniki dyfuzji
%Przyst�pujemy do oblicze� Wsp�czynnik�w dyfuzji i przekrojow pomniedzy wezlami


D_gkk1(N-1,G)=0.0;   %efektywnye wspolczynniki dyfuzji pomiedzy wezlami siatki podzialowej jest ich N-1 (tutaj 99) G=4
%resszta jest wyznaczana najpewniej pozniej

%efektywne wspolczynniki dyfuzji pomiedzy w�z�ami obliczeniowymi
%for(=1:N-1)
for(k=2:N) %moze k-1
   D_gkk1(k,:) = (D_gk(k-1,:).*D_gk(k,:).*(dr+dr))./(D_gk(k-1,:).*dr + D_gk(k,:).*dr);   %czyli element k odnosi sie do przestreznieni pomiedzy k a k+1 np k=1 to przestrzen k a k+1
end    
%TEN wyraz D_gkk1 jest wielkosci 1xN-1 !!!! czyli jest dla wezlow w srodku rdzenia


%Do dalszych obliczen konieczne jest wyznaczenie cz�onu �rodla neutron�w
%rozszczepieniowych S_gk. Obliczamy go tak jak w ksiazce korzystajac z
%przekrojow mikroskopowych na rozszczepienie oraz wspolczynnikow ni ilosci
%neutronow powstajacych na jeden akt rozszczepienia, a wszystko to dla 4
%grup neutronow. Ponadto konieczne jest wyznaczenie makroskopowych
%przekrjow czynnych na przejscia z grupy g' -> g.

%k_eff=1.0;   %Wsp�czynnik mno�enia neutron�w

chi=chi';
for(i=1:N)
   chi_gk(i,:) = chi(:);
end

%Deklaracja Strumienia neutron�w Macierz N (zmienna k) wierszy - n warto�ci strumienia
%G (4) - zmienna g kolumny - odpowiadaja grupa energetycznym   phi(k,g) 
phi = zeros(N*G,1); 
C   = zeros(N*G,1);
A   = zeros(N*G);

%Macierze po 400 element�w plus 4 strumienie w srodku rdzenia i 4
%strumienie na kra�cach.
%Warunki brzegowe:  d phi/dr=0 dla r=0      oraz phi=0 gdy dla d�ugo�ci ekstrapolacji r=R+0.71lambda_tr
%wprowadzane sa punkty k=0 oraz k=N+1 w �rodku na na ko�cu reaktora.

%Pierwszy guess strumienia neutron�w - zwykle rozk�ad rozszczepieniowy /
%wartosci wzgledne; k_eff tez pierwszy guess
phi1_0(1:N,1) = chi(1)*10^15;
phi2_0(1:N,1) = chi(2)*10^15;
phi3_0(1:N,1) = chi(3)*10^15;
phi4_0(1:N,1) = chi(4)*10^15;
phi_0 = [phi1_0; phi2_0; phi3_0; phi4_0];
phi_gk_0 = [];
phi_gk_0 = [phi1_0, phi2_0,phi3_0,phi4_0]; 

chi_gk_0 = [chi_gk(:,1); chi_gk(:,2); chi_gk(:,3); chi_gk(:,4)]; %Grupa za grupa 400x1
k_eff=1.0;    %First Guess k_eff
%Pierwszy Guess cz�onu �r�d�owego S
S_gk_0 = [];



%Inicjalizujemy - tzn liczymy pierwszy raz wyraz rozszczepieniowy F.
wyraz =  phi_gk_0.*v22.*Sigma_f_gk;
FF = sum(wyraz,2);
FF = sum(FF);
F_old = FF;


eps = 1e-6;               %Poziom zbie�no�ci
lambda = 2;             %Pierwszy guess lambdy


%///////////////////////////////////////////////////////
iteracja = 0;
disp('iteracja: ')

while( abs(1.0-lambda) > eps ) 
disp(iteracja)

Suma_0=[];
Suma_0 = Sigma_f_gk.*phi_gk_0;    %Iloczyn strumieni i czlonow ni*Sigma macierz 100x4  N na G
Suma_gk_0 = [Suma_0(:,1); Suma_0(:,2); Suma_0(:,3); Suma_0(:,4)]; %Grupa za grupa 400x1

S_gk_01 =[];        %Suma taka ze macierz 100x4 -> 100x1 czyli suma wyrazow w wierszu - suma po grupach !!!!
S_gk_01 = (1./k_eff).*chi_gk_0.*Suma_gk_0; 
%Wyznaczenie sumy S_gk_02 Sigma_sc_gk to macierz 100x4 100 element�w podzialowych i 4 mozliwe reakcje przejscia g'->g z grupy do grupy dla zjawiska rozpraszania

S_gk_02 =[];
sumka=0.0;

%Pewnie da sie prosciej ale ja nie widze sposobu
for(g=1:G)
    for(k=1:N)         
         if(g==1)    
           S_gk_02 =[S_gk_02; 0];      %Brak wyrazu rozpraszania
         elseif(g==2) 
                 sumka =  phi_gk_0(k,1)*Sigma_sc_gk(k,1);    %wyraz rozpraszania 1->2 phi1*Sig(1->2) etc  !!!nie ma rozpraszania 1->4 tylko 1->2 i 1->3
                 S_gk_02 =[S_gk_02; sumka];        
         elseif(g==3)   
                 sumka = phi_gk_0(k,1)*Sigma_sc_gk(k,2) + phi_gk_0(k,2)*Sigma_sc_gk(k,3);
                 S_gk_02 =[S_gk_02; sumka];
         elseif(g==4)
                 sumka = phi_gk_0(k,3)*(Sigma_sc_gk(k,4));  
                 S_gk_02 =[S_gk_02; sumka];
         end
    end
end    

%Ostatecznie cz�on �rod�owy dla w�z��w obliczeniowych
S_gk=[];
S_gk = S_gk_02 + S_gk_01;
%Wyraz C - prawa strona uk�adu r�wnan

C = S_gk.*v2_col;
%Wspolczynniki do rownania: alfa i beta
alfa_gk=[];
beta_gk=[];


%Z warunkow brzegowych
alfa_1g = 0.0; %Tak jest caly czas juz w macierzy alga_gk
alfa_gNplus1 = D_gk(N,:).*(2.*R_0)./(0.5.*dr + 0.71.*lambda_tr_avg);    %wspolczynniki N+1 dla 4 grup droga transportu usredniona
%Strumienie z warunkow brzegowych
phiNg=0.0; %Strumien na dlugosci ekstrapolacji


alfa_gk = D_gkk1.*(2.*rr)./dr;
%Buckling w kierunku z B_z jest przyjety za identyczny dla kazdej grupy neutron�w
alfa_gk = [alfa_gk; [alfa_gNplus1(1) alfa_gNplus1(2) alfa_gNplus1(3) alfa_gNplus1(4) ] ];   %Dodatnie 101 wiersza z wartosciami alfa N+1

%Wyznaczenie wspolczynnikow beta
for(k=1:N)
   beta_gk(k,1) = alfa_gk(k,1) + alfa_gk(k+1,1) - D_gk(k,1)*B_z*B_z*v2(k) + Sigma_a_gk(k,1)*v2(k);        %Pierwsza grupa neutronow 
   beta_gk(k,2) = alfa_gk(k,2) + alfa_gk(k+1,2) - D_gk(k,2)*B_z*B_z*v2(k) + Sigma_a_gk(k,2)*v2(k); 
   beta_gk(k,3) = alfa_gk(k,3) + alfa_gk(k+1,3) - D_gk(k,3)*B_z*B_z*v2(k) + Sigma_a_gk(k,3)*v2(k); 
   beta_gk(k,4) = alfa_gk(k,4) + alfa_gk(k+1,4) - D_gk(k,1)*B_z*B_z*v2(k) + Sigma_a_gk(k,4)*v2(k);     
end    
%Koonstruujemy teraz macierz A do rozwiazania ukladu rownan
%Macierz A sklada sie z 4 podmacierzy kazda na 1 grupe neutronow;

A1 = diag(beta_gk(:,1));
A1 = A1 + diag( -alfa_gk(2:N,1),1);
A1 = A1 + diag( -alfa_gk(2:N,1),-1);


A2 = diag(beta_gk(:,2));
A2 = A2 + diag( -alfa_gk(2:N,2),1);
A2 = A2 + diag( -alfa_gk(2:N,2),-1);

A3 = diag(beta_gk(:,3));
A3 = A3 + diag( -alfa_gk(2:N,3),1);
A3 = A3 + diag( -alfa_gk(2:N,3),-1);

A4 = diag(beta_gk(:,4));
A4 = A4 + diag( -alfa_gk(2:N,4),1);
A4 = A4 + diag( -alfa_gk(2:N,4),-1);

A0=zeros(N);

A=[A1 A0 A0 A0;A0 A2 A0 A0; A0 A0 A3 A0; A0 A0 A0 A4]; 

%Orzwoazujemy uk�ad r�wna�:

phi = A\C;  %Strumie neutronow

%Stosujemy schemat iteracyjny dlatego tez bedzie trzeba policzyc dodatkowy czlon

phiX=[phi(1:N) phi(N+1:2*N) phi(2*N+1:3*N) phi(3*N+1:4*N)];



wyraz =  phiX.*v22.*Sigma_f_gk;
FF = sum(wyraz,2);
FF = sum(FF);
lambda = FF/F_old;
%F = F_old + 1.4*(F-F_old);   %SOR


phi_gk_0 = phiX; %Zmiana wartosci strumieni do dalszych obliczen
F_old = FF;
lambda_old = lambda;


k_eff_old = k_eff;    %tu jest cos nie tak jak trzeba
k_eff  =  k_eff_old*lambda; 

iteracja=iteracja+1;
%abs(1-lambda)
end   %KONIEC ITERACJI!!!! 


k_eff
%Ca�kowity strumien
strumien =  sum(phiX,2);   %strumien ca�kowity w danym miejscu
figure
plot(rr(:,1),strumien);



figure
plot(rr(:,1),phiX)
legend('1','2','3','4')



toc



