%{
Praca przejsciowa 2011-2012 Semestr Zimowy 
Piotr Darnowski Studia Magisterskie Energetyka Jadrowa Wydzial Mechaniczny Energetyki i Lotnictwa
Program/Skrypt Matlab do rozwiazywania wielogrupowego (4 grupy) przyblizenia dyfuzyjnego w jednym wymiarze
dla reaktora pr�dkiego c�odzonego ciek�y msodem LMFBR z paliwem tlenkowym MOX.
G��wna gemetria: walec stanowiacy przyblizenie typowy ksztalt reaktora 
Dodatkowo zastosowane jest przyblizenie aproksymujace osiowa ucieczke neutron�w tzn. Transverse Leakage Approximation.
Ilo�� grup neutron�w: 4
 
Algorytm i g��wne �r�d�o danych: A.E. Waltar, Fast Breeder Reactors, Pergamon Press 1981
Metody analogiczne do zastosowanych w kodach 1DX i MINX opisanych w ksi��ce Waltara 
Reaktor podzielony/zdyskretyzowany jest na N przedzia��w. Punkty obliczeniowe znajduj� si� w �rodku ka�dego podzia�u
Zastosowane jest przybli�enie osiowej ucieczki neutron�w przez fundamentalne rozwi�zanie -B_z^2 * phi.
g - numer grupy neutronu
         1 - E >820 keV
         2 - 110 keV < E < 820 keV
         3 - 15 keV < E < 110 keV
         4 - 0 keV < E < 15 keV
k - element podzia�u (N podzia��w)
    Dostepne pierwiastki dla danych materia�owych + Dane paliw MOX, stali SS 316  oraz w�gliku boru B4C. Dane pierwiastk�w zawarte w dodatkowych funkcjach dane materia��w z�o�onych w kodzie.
        1 - Boron
        2 - Boron-10
        3 - Carbon 
        4 - Oxygen
        5 - Sodium
        6 - Chormium
        7 - Iron
        8 - Nickel
        9 - Molybdenum
        10 - Thorium 232
        11 - Uranium 233 
        12 - Uranium 235
        13 - Uranium 238 
        14 - Plutonium 239
        15 - Plutonium 240
        16 - Plutonium 241
        17 - Plutonium 242 
        18 - Fisson Products (pairs) 
Jednostki typowo stosowane w obliczeniach neutronowych w uk�adzie CGS:
Wymiar: [cm]
Objeto��: [cm3]
Pole: [cm2]
Przekr�j czynny: [barn]  1 barn = 10^-24 cm2
Strumie� neutronow:  [n/cm2s]
Gestosc: [g/cm3]
%}


%Wst�pne Dane 
G = 4;                           %Ilo�� grup energetycznyc
R_0 = 500.0;                     %[cm] Promie� rdzenia
H = 100.0;                       %[cm] Wysokosc rdzenia                  
delta_r = 20.0;                        %[cm] Promieniowy wymiar ekstrapolowany rdzeni
delta_z = 20.0;                        %[cm] Osiowy wymiar ekstrapolowany rdzenia
B_r = 2.405/(R_0+delta_r);             %[1/cm] Promieniowy parametr geometryczny reaktora walcowego- Geometrical Buckling 
B_z = pi/(H+2*delta_z);                %[1/cm] Osiowy parametr geometryczny reaktora walcowego
N_A = 6.023e23;                        %[atom/g mol]  Sta�a Avogadra 
V_tot= H*pi*R_0^2;                     %[cm3] ca�kowita obj�to�� rdzenia
h =[1 2 3];    %Strefy rdzenia  %size(h,2)ilo�� stref - 3
%Promienie stref rdzenia 
R_stref=[150.0 200.0 150.0];  %musi sie sumowac do 500
if(sum(R_stref) ~= R_0)
    error('Strefy nie sumuja sie do promienia rdzenia')
end
V_stref = H.*pi.*R_stref.^2;

%Chi - udzia� danej grupy energetycznej w widmie rozszczepieniowym [1 2 3 4]
chi(g) = [0.76 0.22 0.02 0];
chi=chi';

%Sk�ad Reaktora

%Trzeba podzieli� rdze� na szereg cz�ci - p�aszcz rdze�/ obszary o r�nym
%wzbogaceniu w pierwszeh wersji programu wybieramy trzy strefy promieniowe:1 - �rodkowa,
%2 - Zewn�trzna oraz 3- p�aszcz.
%Stefy charakteryzuja sie roznym wzbogaceniem i generalnie rozna zawartoscia pierwiastkow
%Tworzymy macierze zawierajace dane dotyczace sk�adu tych stref.
%F_m - udzia� obj�to�ciowy danego materia�u 

%Obj�to�� stref:


%PRZEMYSLEC TO I SPRAWDZIC!!!
%/////////////MATERIA�Y STREF RDZENIA - udzialy f/////////////
%Obliczenia niektorych wielkosci tutaj odbywaja sie niejako poza
%dodatkowymi funkcjami z tablicami materialowymi dla roznych pierwiastkow
%Typowy sk�ad rdzenia Paliwo 30-45% S�d 35=45% Stal 15-20% B4C 1-2%
%Paliwo MOX typowo sk�ada si� z 20-25% PuO2 reszta to zubozony tlenek uranu
%238. Dodatkowo za�o�ymy pewne dane, kt�re de facto mo�na by wyliczy� i przy r�nych obliczeniach w kodzie by�oby to mo�liwe bez za�o�e�.:
%Przyjmujemy paliwo PuO2 - 25% oraz UO2 - z 238 - 75%
%DYGRESJA �r�d�o: KTH Reactor Physics Lectures
%1/M = suma(w_i/M_i)
%M=suma(f_i*M_i)
%rho_i=w_i*rho
%rho_MOX=w_puo2*rho_UO2+w_uo2*rho_PuO2
%f_UO2=0.2 w tym wypadku i f_PuO2=0.8
%Przelicza sie w_i=f_i*M_i / M
%Ogolnie gdy np mamy Fe3O2 to N_Fe=3*N_Fe3O2  i  N_O=2*N_Fe3O2  a udzial
%maswoy  w_Fe = 3*M_fe /(3*M_fe + 2*M_O) !!!

f_UO2  = 0.75;  f_PuO2=0.25;              %MOX sk�ad
M_UO2  = Molar(13)+2*Molar(4);          %tylko U238
M_PuO2 = Molar(14)+2*Molar(4);          %tylko Pu239
M_MOX  = f_UO2*M_UO2 + f_PuO2*M_PuO2;    %Masa molowa/atomowa paliwa jadrowego ok 270

w_PuO2 = f_PuO2*M_PuO2/M_MOX   %przeliczenie udzialow objetosciowych na udzialy masowe
w_UO2  = f_UO2*M_UO2/M_MOX     %udzialmasowy tlenkow w Moxie
w_U  = Molar(13)/(M_UO2);      %udzial masowy uranu jezeli calosc UO2 to U238 - udzial uranu w UO2
w_U=w_U*w_UO2;
w_Pu = Molar(14)/(M_PuO2);       %udzial masowy plutonu jezeli caly pluton to Pu239      
w_Pu=w_Pu*w_PuO2;                % udzial plutonu to udzial plutonu w tlenku plutonu tak samo dla uranu i tlenu

w_O_UO2  = 2*Molar(4)/(M_UO2);
w_O_UO2 = w_O_UO2*w_UO2;
w_O_PuO2 = 2*Molar(4)/(M_PuO2);
w_O_PuO2 = w_O_PuO2*w_PuO2;

Rho_TD_UO2  =10.97; %[g/cm3]
Rho_TD_PuO2 =11.46; %[g/cm3]

Rho_TD_MOX = Rho_TD_UO2*w_UO2 + Rho_TD_PuO2*w_PuO2;            %Przyk�ad Rho_TD_MOX  =11.08;   %80%U i 20%Pu
TD = 0.95;                                                        %Procent teoretycznej gestosci paliwa     
SD = 0.9;                                                         %Smear Density       - g�sto�� "wype�niania"
Rho_MOX = Rho_TD_MOX*SD;                                                         %[g/cm3] gestosc paliwa jadrowego tlenkowego UO2 lub PuO2 do obliczen j�drowych

M_B4C = 4*Molar(2)+Molar(3);                                                %Obliczenia W�gliku Boru B4C poch�anioacza w reaktorach predkich tylko BOR 10
Rho_B4C=2.52;
w_B = 4*Molar(2)/(4*Molar(4)+Molar(3));
w_C = 1*Molar(3)/(4*Molar(4)+Molar(3));

w_Na = 1.0  %W sodzie jest tylko s�d Na 23 czyli jego udzial masowy = 1.0

%Stal SS316 stosowana w reaktorach pr�dkich: Fe, <0.03% C, 16-18.5% Cr, 10-14% Ni, 2-3% Mo, <2% Mn, <1% Si, <0.045% P,
%<0.03% S dla stali te wsp�czynniki s� masowo; pomini�to pomniejsze pierwiastki pod uwage brana jest: Fe, Cr, C, Mo, Ni �r�d�o:http://www.azom.com/article.aspx?ArticleID=863
w_Cr=0.17; w_Ni=0.12; w_C=0.0003; w_Mo=0.02; w_Fe=1 - w_Cr - w_Ni - w_C - w_Mo;
M_steel = ( w_Fe/Molar(7)+w_Cr/Molar(6)+w_Ni/Molar(8)+w_C/Molar(3)+w_Mo/Molar(9) )^(-1);
Rho_steel=8.00;   %[g/cm3] dla stali SS316

Rho_s(3,5)=0.0;  %deklaracja
for(i=1:3)
 Rho_s(i,:)=[Rho_MOX  Rho(5) Rho_steel Rho_B4C Rho_T_UO2];  %Gestosci 5 glownych skladnikow rdzenia
end

%Udzialy objetosciowe  F_m(i,j)  i-strefa, j-materia� obj�to�ciowy Materia�y 1-18 jak w funkcjach na przekroje czynne
F_m(size(h,2),18)=0.0;   %Pocz�tkowo zerujemy wszystko - macierz udzia��w obj�to�ciowych - ta macierz jest dla pierwiastk�w F_m(STREFA, PIERWIASTEK)

%Tworzymy dodatkowa macierz tylko dla paliwa, wegilku boru, stali i sodu i materialu rodnego
F(size(h,2),5)=0.0;    %Gdzie pierwszy argument to: 1- Paliwo  2-S�d/chlodziwo  3-Stal/Material konstrukcyjny   4-B4C/pochlaniacz   5 - Materia� rodny U238
%1 - Paliwo
%2 - S�d
%3 - Stal
%4 - Pochlaniacz
%5 - Absorber

%Tworzymy macierze w i N
%w(i,j) N(i,j) i-strefa 1..3, k-materia� 1..18 
w_m(3,18)=0.0;
N_m(3,18)=0.0;
V_s(3,5)=0.0;
masa_s(3,5)=0.0;
%#STREFA 1 �RODEK RDZENIA  o nizszym wzbogaceniu
%udzialy objetosciowe rdzenia F
F(1,1)=0.30;
F(1,2)=0.45;
F(1,3)=0.15;
F(1,4)=0.02;
F(1,5)=1 - F(1,1)-F(1,2)-F(1,3)-F(1,4);
if(sum(F(1,:)) ~= 1)
    disp('udzialy nie sumuja sie do jednosci!!')
end

V_s(1,:)=F(1,:).*V_stref(1); %obj�to�ci danych materia��w w strefie = udzia�
%obj�to�ciowy danego materia�u * obj�to�� strefy

%#STERFA 2 ZEWNETRZNA CZESC RDZENIA o wyzszym wzbogaceniu
F(2,1)=0.35;
F(2,2)=0.35;
F(2,5)=0.0;
F(2,4)=0.02;
F(2,3)=1 - F(2,1)-F(2,2)-F(2,3)-F(2,4); %stal to reszta
if(sum(F(2,:)) ~= 1)
    disp('udzialy nie sumuja sie do jednosci!!')
end

V_s(2,:)=F(2,:).*V_stref(2);

%#STREFA 3 PLASZCZ
F(3,5)=0.40;                    %ilosc materialu paliworodnego
F(3,1)=0.0;
F(3,2)=0.35;
F(3,4)=0.02;
F(3,3)=1 - F(3,4) - F(3,5) - F(3,2) - F(3,1);
if(sum(F(3,:)) ~= 1)
    disp('udzialy nie sumuja sie do jednosci!!')
end

V_s(3,:)=F(3,:).*V_stref(3);

%V_s=F.*V_stref;
%Trzeba wyznaczyc liczby j�der w 1cm3 dla ka�dej strefy rdzenia do tego zajmiemy sie macierzami F_m
%///////////////////////////////////////////////// 
%Schemat dzialania V_strefy znana -> F*V_strefy = V paliwa, absorbera itp
%-> g�sto�� materia�u typu paliwo, absorber znana -> liczba atom�w danego
%pierwiaastka N_m = w_m*N_A*rho_i/M_m  

%w_m = [0.0 w_B w_C w_O  ]

masa_s = Rho_s.*V_s;    %Masy poszczegolnych stref i materia��w czyli np masa Mox w strefie 1,2,3 masa absorbera w 1,2,3 etc.
 



%Macierz sk�adu rdzenia ze strefami N_m(sterfa; materia�)
%N_m = F_m.*rho_m.*N_A./M
%Dodac czy f sumuje sie do 1 i znalezc wsp�czynniki dyfuzji i makroskopowe przekroje
%czynne


%Walec/ Dane wynikajace z elementarnych rozwaza� teorii dyfuzjii
%jednogrupowej. Przybli�enie: Transverse Leakage Aproximation -
%Prostopad�em przybli�enie ucieczki neutron�w - przej�cie z problemu
%dwuwymiarowego do problemu jednowymiarowego
% | 1 | 2 | 3 |...| k-1 | k | k+1 |... | N-1 | N |
%r=0                    |   |                   r=R
%---------------------->r(k-1/2)
%-------------------------->r(k+1/2)
%---------------------->|   |<-------------- dr(k)
%---->| r(2-1/2) 
%-------->| r(2+1/2)
%----------------------------------------------->|r(N+1/2)

%W kodzie
% | 1 | 2 | 3 |...| k-1 | k | k+1 |... | N-1 | N |
%r=0              |     |   |                   r=R
%r(1)=0         r(k-1) r(k)                    r(N+1)=R_0

%!!!!!!!!! W kodzie r(k) == r(k-1/2)  w �r�dle !!!!!!!!

%podzial=1...k-1,k,k+1....N
%Ilo�� | - lini podzia�u N+1 przypisane sa im pola i objetosci - teoria w
%raporcie 

%Podzia� obszaru obliczeniowego na N przedzia��w o jednakowych d�ugo�ciach
%Kod og�lnie napisany dla podzia��w o dowolonej d�ugo�ci
N = 100;                               %Ilo�� podzia��w domeny obliczeniowej
k = linspace(1,N,N)';                  %Indeks k numerujacy przedzia�y
g = [1 2 3 4]';                        %Wektor numeracji grup energii
r = linspace(0,R_0,N+1)';                 %[cm] ten wektor odpowiada kolejnym po�o�eniom przedzia��w siatki
dr = R_0/(N);                             %[cm] d�ugo�� elementarnego podzia�u dla siatki r�wnomiernej

%Warto�ci P�l oraz obj�to�ci
%ilo�� warto�ci jest N+1 
A(1:N+1)=0.0;                        %pole odpowiadajace po�o�eniom
A=A';
A = 2.*pi.*r.*H;                     %[cm2] wektor p�l A w kodzie to A(k-1,k) w �r�dle
V(1:N)=0.0;                          %Objeto�ci odpowiadajace danym przedzia�om
V=V';
V(k) = pi.*H.*( r(k).^2 - r(k+1).^2);  %[cm3] obj�to�ci
v2(k) = ( r(k).^2 - r(k+1).^2);        %[cm2] Roznice kwadratow promieni dla danego przedzia�u - stosowane w obliczeniach




%Deklaracja Strumienia neutron�w Macierz N (zmienna k) wierszy - n warto�ci strumienia
%G (4) - zmienna g kolumny - odpowiadaja grupa energetycznym   phi(k,g) 
phi = zeros(N,G); 




%Wsp�czynnik Dyfuzji - obliczana na bazie mikroskopowych przekroj�w czynnych oraz ilo�ci atom�w danego materia�u





