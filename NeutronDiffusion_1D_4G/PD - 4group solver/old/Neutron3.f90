!Praca przejsciowa 2011-2012 Semestr Zimowy
!Piotr Darnowski Studia Magisterskie Energetyka Jadrowa Wydzial Mechaniczny Energetyki i Lotnictwa
!Program do rozwiazywania elemntarnych przypadkow dyfuzjii neutronow  
!Wszystkie jednostki SI
!Wstepna wersja z roznymi danymi dla moderatorow
!Zrodlo glowne: Nuclear Reactor Analysis - 

!Jednowymiarowe rownanie dyfuzji neutronow
!Ze stalym wspolczynnikiem dyfuzji i stalym makroskopowym przekrojem czynnym na absorbcje
!Przyblizenie dyfuzyjne dla jednej predkosci neutronow w osrodku slabo pochalniajacym ze zrodlem neutronow S.
!Rownanie stacjonarne- niezalezne od czasu. Dla przypadku plyty jednowymiarowej - czyli nieskonczonego odsorka w postaci plyty. Tymczasowo nie jest to reaktor jadrowy z uwagi
!na to ze czlon zrodlowy jest ogolony i nie opisuje mnozenia neutronow w wyniku rozszczepien. W ogolonosci jest to rownanie rozniczkowe niejednorodne o stalych wspolczynnikach.
!-D phi(x)'' + sigma*phi(x)=S(x)   uklad kartezjanski
!-D(phi(r)'' +1/r phi(r)') + Sigma*phi(r) = S(r)    uklad cylindryczny 
!-D(phi(r)'' +2/r phi(r)') + Sigma*phi(r) = S(r)    uklad sferyczny 

!ogolniejsza postac rownanie dla zmiennych D i Sigma 
!-(D(x)phi(x)')' + sigma(x)phi(x)=S(x)          w ukladzie kartezjanskim
!lun bardziej ogolnie dla kazdego ukladu odniesienia:
!-div D(r)grad phi(r) + Sigma(r)*phi(r) =S(r)   gdzie r jest wektorem polozenia

!*************************************************************************************************
program Neutron
use DFPORT
implicit none
real(8) dv, N_real   
integer i,j
real(8) Sigma_a(5)
real(8) Sigma_tr(5)
real(8) Si_f_ni(2)
real(8) D(5)                      !wspolczynnik dyfuzji
real(8) delta                     !delta - elementany podzial
real(8) grubosc                   !grubosc plyty
real(8) Sigma                     !Wybrany makroskopowy przekroj czynny
real(8) DD                         !Wybrany wspolczynnik dyfuzji
real(8) c                         !Wybrany wspolczynnik dla schematu roznicowego ktory zmienia uklad odniesienia
real(8), allocatable ::  phi(:)   !wektor strumieni neutronow
real(8), allocatable ::  A(:,:)   !macierz ukladu rownan (wspolczynnikow)
real(8), allocatable ::  S(:)     !wektor zrodla
real(8) phi_0, phi_N , S_0, S_N


integer N                         !ilosc podzialow
character*30 wersja
integer przypadek                     !uklad wsplorzednych

!Makroskopowe przekroje czynne dla niektorych materialow [1/cm]
Sigma_a(1)=9.24D-2      !U235
Sigma_a(2)=1.39D-2      !U238    
Sigma_a(3)=8.08D-3      !H
Sigma_a(4)=4.90D-6      !O
Sigma_a(5)=7.01D-4      !Zr
!Makroskopowe przekroje czynne - transportowe [1/cm]
Sigma_tr(1)=3.08D-4     !U235
Sigma_tr(2)=6.95D-3     !U238
Sigma_tr(3)=1.79D-2     !H
Sigma_tr(4)=7.16D-3     !O
Sigma_tr(5)=2.91D-3     !Zr
!Iloczyn ni - ilosci neutronow na rozszczepienie oraz makroskppowego przekroju czynnego na rozszczepienie [1/cm]
Si_f_ni(1)=0.145
Si_f_ni(2)=1.2D-2
!Wspolczynniki dyfuzji roznych materialow [cm]  D=1/(3*Sigma_tr) [cm]
D(1)=1/(3*Sigma_tr(1))
D(2)=1/(3*Sigma_tr(2))
D(3)=1/(3*Sigma_tr(3))
D(4)=1/(3*Sigma_tr(4))
D(5)=1/(3*Sigma_tr(5))

!Ustawienie aktywnych parametrow
Sigma=Sigma_a(1)
Sigma_tr=Sigma_tr(1)
DD=D(1)
Si_f_ni=Si_f_ni(1)

open(1, file='Macierz.txt')      !Pliki do zapisu wynikow, macierzy dla ukladu wspolrzednych oraz wynikow stromieni neutronow
open(2, file='Strumienie.txt')

wersja='0.0.0.1'
write(*,'("Neutron, Wersja ", A,/)') wersja

write(*,'("Podaj ilosc podzialow dla problemu jednowymiarowego:")')
read(*,*) N
N_real=REAL(N)/REAL(1)
delta=grubosc/N_real

!Dynamiczna alokacja tablicy strumieni neutronow bez strumieni brzegowych z strumieniami to byloby N+1 bo tyle jest wezlow dla N podzialow
allocate (phi(N-1))     
allocate (A(N-1,N-1))
allocate (S(N-1))

write(*,'("Podaj przypadek do obliczen")')
write(*,'("Zagadnienia Jednowymiarowe")')
write(*,'("1 - Nieskonczona plyta")')
write(*,'("2 - Nieskonczony reaktor plytowy")')
write(*,'("3 - Nieskonczony walec")')
write(*,'("4 - Nieskonczony reaktor walcowy")')
write(*,'("5 - Sfera")')
write(*,'("6 - Reaktor Sferyczny")')

read(*,*) przypadek


select case(przypadek)
  case(1)
      write(*,'("Podaj szerokosc plyty:")')
      read(*,*) grubosc
      c=0
  case(2)
      write(*,'("Podaj promien walca:")')
      read(*,*) grubosc
      c=1
  case(3)
       write(*,'("Podaj promien kuli:")')
       read(*,*) grubosc
       c=2
  case(4)       
  case(5)
  case(6)
         
  case default
       write(*,'("Nie ma takiego ukladu")')
       pause
       stop
  end select

write(*,'("Dlugosc elemntarnej komorki: ", F10.4)') delta


!Wypelnienie tablicy a(:,:) zerami i wypelnienie wektorow zerami
do i=1,N-1,1
  do j=1,N-1,1
   A(i,j)=0.0 
  end do
end do  
!Wypelnienie zrodla zerami
do i=1,N-1,1
 S(i)=0.0
end do
!Wypelnienie zrodla
do i=1,N-1,1
 S(i)=1D+3
end do
!Przypisanie wektorowi rozwiazan jedynek
do i=1,N-1,1
 phi(i)=0.0
end do

!Warunki brzegowe - tymczasowo bez poprawki ekstrapolacji z teorii transportu
!Z teorii transportu dlugosc ekstrapolacji z0=0.7104*lambda_tr     D=lambda_tr/3    lambda_tr = 1/Sigma_tr
!Dla Plyty
phi_0=0.0
phi_N=0.0
S_0=phi_0
S_N=phi_N
!Dla Walca i Sfery


!Rownanie dyfuzji rozwiazujemy dla i=1,...,N-1
!wezlow jest N+1, a przedzialow jest N.   0,1,...,N-1,N  wezlow jest N+1
!a(i,i-1)phi(i-1) + a(i,i)phi(i) + a(i,i+1)phi(i+1) = S(i)
!a(i,i-1)= -D/delta**2
!a(i,i))= 2D/delta**2 + Sigma_a
!a(i,i+1)=-D/delta**2
!Wypelniamy tablice A z pominieciem pierwszego i ostatniego wiersza
do i=1,N-1,1
 if((i.NE.1).AND.(i.NE.N-1)) then
   j=i 
   A(i,j-1)= -(DD/delta**2)*(1 - c/(2*i-1))
   A(i,j)= 2*DD/delta**2 + Sigma
   A(i,j+1)= -(DD/delta**2)*(1 + c/(2*i-1)) 
  end if  
end do
!Wypelnienie pierwszego wiersza i ostatniego wiersza
A(1,1)=2*DD/delta**2 + Sigma
A(1,2)=-(DD/delta**2)*(1+c/(2*1-1)) 
A(N-1,N-1)=2*DD/delta**2 + Sigma
A(N-1,N-2)=-(DD/delta**2)*(1-c/(2*(N-1)-1))
!Reszta elemt�w macierzy to zera


pause

do i=1,N-1,1
write(1,'("Kolumna: ", I)') i
 do j=1,N-1,1
   write(1,*)  A(i,j)
 end do  
end do
dv=TIMEF()	

write(2,'("Zrodlo:")')
do i=1,N-1,1
 write(2,*)  S(i)
end do

!Teraz aplikujemy metod� elimnacji gaussa

call gauss_1(A,S,phi,N-1)  

write(2,'("Strumienie:")')
do i=1,N-1,1
 write(2,*)  phi(i)
end do






write(2,'("czas:")')
write(2,*) TIMEF()
pause 'Neutron - koniec programu'
close(1, status='keep')
close(2, status='keep')
end program Neutron
!**************************************KONIEC PROGRAMU************************************



!GAUSS ELIMINATION METHOD 
subroutine gauss_1(a,b,x,n)
!============================================================
! Solutions to a system of linear equations A*x=b
! Method: the basic elimination (simple Gauss elimination)
! Alex G. November 2009
!-----------------------------------------------------------
! input ...
! a(n,n) - array of coefficients for matrix A
! b(n)   - vector of the right hand coefficients b
! n      - number of equations
! output ...
! x(n)   - solutions
! comments ...
! the original arrays a(n,n) and b(n) will be destroyed 
! during the calculation
!===========================================================
implicit none 
integer n
double precision a(n,n), b(n), x(n)
double precision c
integer i, j, k

!step 1: forward elimination
do k=1, n-1
   do i=k+1,n
      c=a(i,k)/a(k,k)
      a(i,k) = 0.0
      b(i)=b(i)- c*b(k)
      do j=k+1,n
         a(i,j) = a(i,j)-c*a(k,j)
      end do
   end do
end do

!step 2: back substitution
x(n) = b(n)/a(n,n)
do i=n-1,1,-1
   c=0.0
   do j=i+1,n
     c= c + a(i,j)*x(j)
   end do 
   x(i) = (b(i)- c)/a(i,i)
end do
end subroutine gauss_1


!SOR ITERATIVE METHOD

subroutine gs_sor(a,b,x,omega,eps,n,iter)
!==========================================================
! Solutions to a system of linear equations A*x=b
! Method: The successive-over-relaxation (SOR)
! Alex G. (November 2009)
!----------------------------------------------------------
! input ...
! a(n,n) - array of coefficients for matrix A
! b(n)   - array of the right hand coefficients b
! x(n)   - solutions (initial guess)
! n      - number of equations (size of matrix A)
! omega  - the over-ralaxation factor
! eps    - convergence tolerance 
! output ...
! x(n)   - solutions
! iter   - number of iterations to achieve the tolerance
! coments ...
! kmax   - max number of allowed iterations
!==========================================================
implicit none 
integer, parameter::kmax=100
integer n
double precision a(n,n), b(n), x(n)
double precision c, omega, eps, delta, conv, sum
integer i, j, k, iter, flag

! check if the system is diagonally dominant
flag = 0
do i=1,n
  sum = 0.0
  do j=1,n
    if(i == j) cycle
    sum = sum+abs(a(i,j))
  end do
  if(abs(a(i,i)) < sum) flag = flag+1
end do
if(flag >0) write(*,*) 'The system is NOT diagonally dominant'    

do k=1,kmax
  conv = 0.0
  do i=1,n
    delta = b(i)
    do j=1,n
      delta = delta - a(i,j)*x(j)
    end do
    x(i) = x(i)+omega*delta/a(i,i)
    if(abs(delta) > conv) conv=abs(delta)
  end do
  if(conv < eps) exit
end do
iter = k
if(k == kmax) write (*,*)'The system failed to converge'

end subroutine gs_sor


! ---------------------- MODULE fseidel.f90 ---------------------------
!Gauss Seidel Method with relaxation 
!source: http://jean-pierre.moreau.pagesperso-orange.fr/f_matrices.html

subroutine seidel(crit,n,mat,b,omega,x,residu,iter,rc)
parameter(ITERMAX=500)            ! Maximal number of iterations
parameter(ONE=1.d0,TWO=2.d0,ZERO=0.d0)
  integer crit, n, iter, rc
  REAL*8 mat(n,n),b(n),omega
  REAL*8 x(n),residu(n)
!*====================================================================*
!*                                                                    *
!*  seidel solves the linear system  mat * x = b  iteratively.        *
!*  Here  mat  is a nonsingular  n x n  matrix, b is the right hand   *
!*  side for the linear system and x is the solution.                 *
!*                                                                    *
!*  seidel uses the Gauss Seidel Method with relaxation for a given   *
!*  relaxation coefficient 0 < omega < 2.                             *
!*  If  omega = 1, the standard Gauss Seidel method (without          *
!*  relaxation) is performed.                                         *
!*                                                                    *
!*====================================================================*
!*                                                                    *
!*   Applications:                                                    *
!*   =============                                                    *
!*      Solve linear systems with nonsingular system matrices that    *
!*      satisfy one of the following criteria: row sum criterion,     *
!*      column sum criterion or the criterion of Schmidt and v. Mises.*
!*      Only if at least one of these criteria is satisfied for mat,  *
!*      convergence of the scheme is guaranteed [See BIBLI 11].       *
!*                                                                    *
!*====================================================================*
!*                                                                    *
!*   Input parameters:                                                *
!*   ================                                                 *
!*      crit     integer crit                                         *
!*               select criterion                                     *
!*               =1 : row sum criterion                               *
!*               =2 : column sum criterion                            *
!*               =3 : criterion of Schmidt-v.Mises                    *
!*               other : no check                                     *
!*      n        integer n ( n > 0 )                                  *
!*               size of mat, b and x                                 *
!*      mat      REAL*8   mat(n,n)                                    *
!*               Matrix of the liear system                           *
!*      b        REAL*8 b(n)                                          *
!*               Right hand side                                      *
!*      omega    REAL*8 omega; (0 < omega < 2)                        *
!*               Relaxation coefficient.                              *
!*      x        REAL*8  x(n)                                         *
!*               Starting vector for iteration                        *
!*                                                                    *
!*   Output parameters:                                               *
!*   ==================                                               *
!*      x        REAL*8  x(n)                                         *
!*               solution vector                                      *
!*      residu   REAL*8   residu(n)                                   *
!*               residual vector  b - mat * x; close to zero vector   *
!*      iter     integer iter                                         *
!*               Number of iterations performed                       *
!*      rc       integer return code                                  *
!*               =  0     solution has been found                     *
!*               =  1     n < 1  or omega <= 0 or omega >= 2          *
!*               =  2     improper mat or b or x (not used here)      *
!*               =  3     one diagonal element of mat vanishes        *
!*               =  4     Iteration number exceeded                   *
!*               = 11     column sum criterion violated               *
!*               = 12     row sum criterion violated                  *
!*               = 13     Schmidt-v.Mises criterion violated          *
!*                                                                    *
!*====================================================================*
  REAL*8 tmp, eps;

  iter = 0                        !Initialize iteration counter
  rc = 0

  if (n<1.or.omega<=ZERO.or.omega>=TWO) then
    rc=1
    return
  end if

  eps = 1.d-10

  do i=1, n                       !transform mat so that all
                                          !diagonals equal 1
    if (mat(i,i) == ZERO) then
      rc=3
      return
    end if
    tmp = ONE / mat(i,i)
    do j=1, n
      mat(i,j)= mat(i,j)*tmp
    end do
    b(i) = b(i)*tmp               !adjust right hand side b
  
  end do


  !check convergence criteria
  if (crit==1) then
     do i = 1, n                  !row sum criterion
       tmp=ZERO
       do j=1,n
         tmp = tmp + dabs(mat(i,j))
       end do
       if (tmp >= TWO) then
         rc=11
         return
       end if 
     end do
  else if (crit==2) then  
     do j=1, n                    !column sum criterion
	   tmp=ZERO
       do i=1,n
         tmp = tmp + dabs(mat(i,j))
       end do
       if (tmp >= TWO) then
         rc=12
	 return
       end if
     end do
  else if (crit==3) then
     tmp=ZERO
     do i=1, n
       do j=1, n                  !criterion of Schmidt
         tmp = tmp + mat(i,j)**2  !von Mises
       end do
     end do
     tmp = DSQRT(tmp - ONE)
     if (tmp >= ONE) then
       rc=13
       return
     end if
  end if

  do i=1, n 
    residu(i) = x(i)              !store x in residu
  end do

  do while (iter <= ITERMAX)      !Begin iteration
  
    iter=iter+1

    do i=1, n
      tmp=b(i)
      do j=1, n
        tmp =  tmp - mat(i,j) * residu(j)
      end do 
      residu(i) = residu(i) + omega * tmp
    end do

    do i=1, n                     !check break-off criterion
      tmp = x(i) - residu(i)
      if (DABS (tmp) <= eps) then
        x(i) = residu(i)          !If rc = 0 at end of loop
        rc = 0                    !  -> stop iteration
      else
        do j=1, n 
          x(j) = residu(j)
        end do
        rc = 4
        goto 10
      end if
    end do
    if (rc == 0) goto 20          !solution found
10 end do                         !End iteration

20 do i=1, n                      !find residual vector
     tmp=b(i)
     do j=1, n
       tmp = tmp - mat(i,j) * x(j)
     end do
     residu(i) = tmp
   end do

  return

end

! ----------------------- END fseidel.f90 ----------------------------