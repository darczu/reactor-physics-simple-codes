%{
Praca przejsciowa 2011-2012 Semestr Zimowy 
Piotr Darnowski Studia Magisterskie Energetyka Jadrowa Wydzial Mechaniczny Energetyki i Lotnictwa
Program/Skrypt Matlab do rozwiazywania wielogrupowego (4 grupy) przyblizenia dyfuzyjnego w jednym wymiarze
dla reaktora pr�dkiego c�odzonego ciek�y msodem LMFBR z paliwem tlenkowym.
G��wna gemetria: walec stanowiacy przyblizenia reaktora
Dodatkowo zastosowane jest przyblizenie aproksymujace osiowa ucieczke neutron�w.
Ilo�� grup neutron�w: 4
 
Algorytm i g��wne �r�d�o danych: A.E. Waltar, Fast Breeder Reactors, Pergamon Press 1981
Algorytm analogiczny zastosowany w kodach 1DX i MINX 
Reaktor podzielony/zdyskretyzowany jest na N przedzia��. Punkty obliczeniowe znajduj� si� w �rodku ka�dego podzia�u
Zastosowane jest przybli�enie osiowej ucieczki neutron�w przez fundamentalne rozwi�zanie -B_z^2 * phi.
g - numer grupy neutronu
         1 - E >820 keV
         2 - 110 keV < E < 820 keV
         3 - 15 keV < E < 110 keV
         4 - 0 keV < E < 15 keV
k - element podzia�u (N podzia��w)

    Dostepne pierwiastki dla danych materia�owych
        1 - Boron
        2 - Boron-10
        3 - Carbon 
        4 - Oxygen
        5 - Sodium
        6 - Chormium
        7 - Iron
        8 - Nickel
        9 - Molybdenum
        10 - Thorium 232
        11 - Uranium 233 
        12 - Uranium 235
        13 - Uranium 238 
        14 - Plutonium 239
        15 - Plutonium 240
        16 - Plutonium 241
        17 - Plutonium 242 
        18 - Fisson Products (pairs) 

Jednostki:
Wymiar: [cm]
Przekr�j czynny: [barn]  1 barn = 10^-24 cm2
Strumie�:  [n/cm2s]
%}


%Wst�pne Dane 
G = 4;                           %Ilo�� grup energetycznyc
R_0 = 500.0;                     %[cm] Promie� rdzenia
H = 100.0;                       %[cm] Wysokosc rdzenia                  
delta_r = 20.0;                        %[cm] Promieniowy wymiar ekstrapolowany rdzeni
delta_z = 20.0;                        %[cm] Osiowy wymiar ekstrapolowany rdzenia
B_r = 2.405/(R_0+delta_r);             %[1/cm] Promieniowy parametr geometryczny reaktora walcowego- Geometrical Buckling 
B_z = pi/(H+2*delta_z);                %[1/cm] Osiowy parametr geometryczny reaktora walcowego
N_A = 6.023e23;                        %[atom/g mol]  Sta�a Avogadra     

%Chi - udzia� danej grupy energetycznej w widmie rozszczepieniowym [1 2 3 4]
chi(g) = [0.76 0.22 0.02 0];
chi=chi';

%Sk�ad Reaktora

%Trzeba podzieli� rdze� na szereg cz�ci - p�aszcz rdze�/ obszary o r�nym
%wzbogaceniu w pierwszeh wersji programu wybieramy trzy strefy promieniowe:1 - �rodkowa,
%2 - Zewn�trzna oraz 3- p�aszcz.
%Stefy charakteryzuja sie roznym wzbogaceniem i generalnie rozna zawartoscia pierwiastkow
%Tworzymy macierze zawierajace dane dotyczace sk�adu tych stref.
%F_m - udzia� obj�to�ciowy danego materia�u 
h =[1 2 3];    %Strefy rdzenia  %size(h,2)ilo�� stref - 3
%Udzialy objetosciowe  F_m(i,j)  i-strefa, j-materia�
%obj�to�ciowy
%Materia�y 1-18 jak w funkcjach na przekroje czynne
F_m(size(h,2),18)=0.0;   %Pocz�tkowo zerujemy wszystko - macierz udzia��w obj�to�ciowych - ta macierz jest dla pierwiastk�w

%/////////////MATERIA�Y STREF RDZENIA - udzialy f/////////////
%Obliczenia niektorych wielkosci tutaj odbywaja sie niejako poza
%dodatkowymi funkcjami z tablicami materialowymi dla roznych pierwiastkow
%Typowy sk�ad rdzenia Paliwo 30-45% S�d 35=45% Stal 15-20% B4C 1-2%
%Paliwo MOX typowo sk�ada si� z 20-25% PuO2 reszta to zubozony tlenek uranu
%238. Dodatkowo za�o�ymy pewne dane, kt�re de facto mo�na by wyliczy� i przy r�nych obliczeniach w kodzie by�oby to mo�liwe bez za�o�e�.:
%Przyjmujemy paliwo PuO2 - 25% oraz UO2 - z 238 - 75%
v_UO2=0.75;  v_PuO2=0.25;  %Mov sk�ad
M_fuel=(Molar(13)+2*Molar(4))*v_UO2+v_PuO2*(Molar(14)+2*Molar(4));   %Masa molowa/atomowa paliwa jadrowego
Rho_fuel=9.5;                                                         %[g/cm3] gestosc paliwa jadrowego tlenkowego 
M_abs = 4*Molar(2)+Molar(3);                                         %Obliczenia W�gliku Boru B4C poch�anioacza w reaktorach predkich
Rho_abs=2.52;                                                         %[g/cm3] g�sto�� w�gliku boru mozna by tez przyblizyc masami molowymi

%Stal SS316 stosowana w reaktorach pr�dkich: Fe, <0.03% C, 16-18.5% Cr, 10-14% Ni, 2-3% Mo, <2% Mn, <1% Si, <0.045% P,
%<0.03% S; pomini�to pomniejsze pierwiastki pod uwage brana jest: Fe, Cr, C, Mo, Ni
a_Cr=0.17; a_Ni=0.12; a_C=0.0003; a_Mo=0.02; a_Fe=1 - a_Cr - a_Ni - a_C - a_Mo; 
M_steel = a_Fe*Molar(7)+a_Cr*Molar(6)+a_Ni*Molar(8)+a_C*Molar(3)+a_Mo*Molar(9);
Rho_steel=Rho(7);   %Przyjmujemy gestosc typowej stali ktora jest zblizona do gestosci zelaza Fe


%Pojawia si� teraz konieczno�� ustalenia sk�adu stref rdzenia - Program
%daje mozliwosc wykorzystania calych tablic my jednak pos�u�ymy si�
%wato�ciami sta�ymi z oblicze�
%Tworzymy dodatkowa macierz tylko dla paliwa, wegilku boru, stali i sodu
F(size(h,2),5)=0.0;    %Gdzie pierwszy argument to: 1- Paliwo  2-S�d  3-Stal   4-B4C   5 - Materia� powielajacy U238
%#STREFA 1 �RODEK RDZENIA
F(1,1)=0.30;
F(1,2)=0.45;
F(1,3)=0.15;
F(1,4)=0.02;
F(1,5)=1 - F(1,1)-F(1,2)-F(1,3)-F(1,4); %stal to reszta -normuje sie do jednosci

%#STERFA 2 ZEWNETRZNA CZESC RDZENIA
F(2,1)=0.35;
F(2,2)=0.35;
F(2,5)=0.13;
F(2,4)=0.02;
F(2,3)=1 - F(2,1)-F(2,2)-F(2,3)-F(2,4); %stal to reszta

%#STREFA 3 PLASZCZ
F(3,5)=0.40; %stal to reszta
F(3,1)=0.0;
F(3,2)=0.35;
F(3,4)=0.02;
F(3,3)=1 - F(3,4) - F(3,5) - F(3,2) - F(3,1);

%Inne na 0
%Trzeba wyznaczyc liczby j�der w 1cm3 dla ka�dej strefy rdzenia
%/////////////////////////////////////////////////



rho_m(1:18)               % funkcja na g�sto��
M_m(1:18)                 % funkcja na masy molowe

%Macierz sk�adu rdzenia ze strefami N_m(sterfa; materia�)
N_m=F_m*.rho_m.*N_A./M_m
%Dodac czy sumuje sie do 1


%Walec/ Dane wynikajace z elementarnych rozwaza� teorii dyfuzjii
%jednogrupowej. Przybli�enie: Transverse Leakage Aproximation -
%Prostopad�em przybli�enie ucieczki neutron�w - przej�cie z problemu
%dwuwymiarowego do problemu jednowymiarowego
% | 1 | 2 | 3 |...| k-1 | k | k+1 |... | N-1 | N |
%r=0                    |   |                   r=R
%---------------------->r(k-1/2)
%-------------------------->r(k+1/2)
%---------------------->|   |<-------------- dr(k)
%---->| r(2-1/2) 
%-------->| r(2+1/2)
%----------------------------------------------->|r(N+1/2)

%W kodzie
% | 1 | 2 | 3 |...| k-1 | k | k+1 |... | N-1 | N |
%r=0              |     |   |                   r=R
%r(1)=0         r(k-1) r(k)                    r(N+1)=R_0

%!!!!!!!!! W kodzie r(k) == r(k-1/2)  w �r�dle !!!!!!!!

%podzial=1...k-1,k,k+1....N
%Ilo�� | - lini podzia�u N+1 przypisane sa im pola i objetosci - teoria w
%raporcie 

%Podzia� obszaru obliczeniowego na N przedzia��w o jednakowych d�ugo�ciach
%Kod og�lnie napisany dla podzia��w o dowolonej d�ugo�ci
N = 100;                               %Ilo�� podzia��w domeny obliczeniowej
k = linspace(1,N,N)';                  %Indeks k numerujacy przedzia�y
g = [1 2 3 4]';                        %Wektor numeracji grup energii
r = linspace(0,R_0,N+1)';                 %[cm] ten wektor odpowiada kolejnym po�o�eniom przedzia��w siatki
dr = R_0/(N);                             %[cm] d�ugo�� elementarnego podzia�u dla siatki r�wnomiernej

%Warto�ci P�l oraz obj�to�ci
%ilo�� warto�ci jest N+1 
A(1:N+1)=0.0;                        %pole odpowiadajace po�o�eniom
A=A';
A = 2.*pi.*r.*H;                     %[cm2] wektor p�l A w kodzie to A(k-1,k) w �r�dle
V(1:N)=0.0;                          %Objeto�ci odpowiadajace danym przedzia�om
V=V';
V(k) = pi.*H.*( r(k).^2 - r(k+1).^2);  %[cm3] obj�to�ci
v2(k) = ( r(k).^2 - r(k+1).^2);        %[cm2] Roznice kwadratow promieni dla danego przedzia�u - stosowane w obliczeniach




%Deklaracja Strumienia neutron�w Macierz N (zmienna k) wierszy - n warto�ci strumienia
%G (4) - zmienna g kolumny - odpowiadaja grupa energetycznym   phi(k,g) 
phi = zeros(N,G); 




%Wsp�czynnik Dyfuzji - obliczana na bazie mikroskopowych przekroj�w czynnych oraz ilo�ci atom�w danego materia�u





