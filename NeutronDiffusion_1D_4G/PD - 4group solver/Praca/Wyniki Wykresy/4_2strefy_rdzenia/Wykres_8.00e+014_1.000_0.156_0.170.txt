Fractional Volume 
           |Inner Core|Outer Core|Radial Blanket|
Plutonium : 0.046654  0.050844    0.000000
Uranium   : 0.253346  0.249156    0.600000
Sodium    : 0.400000  0.400000    0.200000
Steel     : 0.292000  0.296000    0.200000
Absorber  : 0.008000  0.004000    0.000000

Total power       : 1245.20 MWt
Max total flux    : 3.35e+015 n/cm2s
Avg total flux    : 2.30e+015 n/cm2s
k eff             : 1.027683
Initial k eff     : 1.000000
Initial flux      : 8.00e+014 n/cm2s

Wzbogacenie w izotopy plutonu i uranu paliwa: 
Strefa 1 
Pu239  :0.156065
Pu240  :0.000000
Pu241  :0.000000
Pu242  :0.000000
Th232  :0.000000
U233   :0.000000
U235   :0.000000
U238   :0.843935
Strefa 2 
Pu239  :0.170070
Pu240  :0.000000
Pu241  :0.000000
Pu242  :0.000000
Th232  :0.000000
U233   :0.000000
U235   :0.000000
U238   :0.829930
Strefa 3 
Pu239  :0.000000
Pu240  :0.000000
Pu241  :0.000000
Pu242  :0.000000
Th232  :0.000000
U233   :0.000000
U235   :0.000000
U238   :1.000000
Udzial masowy tlenkow plutonu i uranu w miszance paliwowej: 
  0.156000  0.170000  0.000000
  0.844000  0.830000  1.000000