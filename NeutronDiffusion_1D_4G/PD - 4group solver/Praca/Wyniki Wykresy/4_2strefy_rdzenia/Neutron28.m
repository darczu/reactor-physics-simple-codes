%{
16.02.2012 - problemy
Wynik zalezy od przyjetych strumieni neutronow inicjalizacji
oraz od przyjetego k_eff na wejsciu
!!!

%Modyfikacja sekcja Fuel - materia��w staje si� plutonem dla paliwa
%Sekcja Fertile staje si� uranem!!!!! 20.02.2012
%W takiej sytuacji sekcja fuel to izotopy plutonu
%Sekcja fertile to izotopy plutonu pozwala to stosowac dwa rozne paliwa o
%dwoch roznych wzbogaceniach !!!

Dzia�aj�ca wersja kodu: krtytyczo�� i kszta�t strumienie neutron�w
%Aktualne poprawki 10.02.2012 rok. Kolejne 14.02.2012. 16.02.2012
%znaleziony potezny blad kod dziala teraz super!
Start - pocz�tek 2012 modyfikacje 25, 31.01.2012

Praca przejsciowa 2011-2012 Semestr Zimowy 
Piotr Darnowski Studia Magisterskie Energetyka Jadrowa Wydzial Mechaniczny Energetyki i Lotnictwa
Program/Skrypt w Matlabie do rozwiazywania wielogrupowego (4 grupy) przyblizenia dyfuzyjnego w jednym wymiarze
dla reaktora pr�dkiego ch�odzonego ciek�ym sodem (LMFBR) z paliwem tlenkowym MOX.
G��wna gemetria: walec stanowiacy przyblizony typowy ksztalt reaktora 
Dodatkowo zastosowane jest przyblizenie aproksymujace osiowa ucieczke neutron�w tzn. Transverse Leakage Approximation.
Ilo�� grup neutron�w: 4
 
Algorytm i g��wne �r�d�o danych: A.E. Waltar, Fast Breeder Reactors, Pergamon Press 1981
Metody analogiczne do zastosowanych w profesjonalnych duzo bardziej skomplikowanych kodach 1DX i MINX opisanych w ksi��ce Waltara. 
Reaktor podzielony/zdyskretyzowany jest na N przedzia��w. Punkty obliczeniowe znajduj� si� w �rodku ka�dego podzia�u. Dodatkowo wprowadzono 3 strefy rdzen centralny, obrzeza rdzenia oraz p�aszcz promieniowy.
Zastosowane jest przybli�enie osiowej ucieczki neutron�w przez fundamentalne rozwi�zanie -B_z^2 * phi tak jak w �r�dle.
Do g��wnego skryptu do��czone s� dodatkowe programy.
Molar   - Zawiera masy molowe/~liczby atomowe 18 pierwiastk�w  
Sigma   - Zawiera przekroje czynne mikroskopowe na r�ne reakcje dla 18 pierwiastk�w
Sigma_s - Zawiera przekroje czynne na przej�cia pomi�dzy grupami w reakcjach rozpraszania
Rho     - Zawiera gesto�ci (w r�nych warunkach! trzeba tu zachowa� ostro�no��)

g - numer grupy neutronu
         1 - E >820 keV
         2 - 110 keV < E < 820 keV
         3 - 15 keV < E < 110 keV
         4 - 0 keV < E < 15 keV
k - element podzia�u (N podzia��w)

Dostepne pierwiastki/materia�y dla danych materia�owych + Dane paliw MOX, stali SS 316  oraz w�gliku boru B4C. Dane pierwiastk�w zawarte w dodatkowych funkcjach matlabowskich dane materia��w -miszanin  w kodzie.
        1 - Boron
        2 - Boron-10      
        3 - Carbon 
        4 - Oxygen
        5 - Sodium
        6 - Chormium
        7 - Iron
        8 - Nickel
        9 - Molybdenum
        10 - Thorium 232
        11 - Uranium 233 
        12 - Uranium 235
        13 - Uranium 238 
        14 - Plutonium 239
        15 - Plutonium 240
        16 - Plutonium 241
        17 - Plutonium 242 
        18 - Fisson Products (pairs) 
Jednostki typowo stosowane w obliczeniach neutronowych w uk�adzie CGS:
Wymiar: [cm]
Objeto��: [cm3]
Pole: [cm2]
Przekr�j czynny: [barn]  1 barn = 10^-24 cm2
Strumie� neutronow:  [n/cm2s]
Gestosc: [g/cm3]
Dodatkowo stosowany jest tzn. barn-cm poprzez zastapienie liczby Avogadra jej warto�ci� podzielon�przez 10^24
%}

format long e                           %Format zapisu danych
clear                                   %Czyszczenie ekranu
clc
tic                                     %Start timera
%Wst�pne Dane 
G = 4;                                 %Ilo�� grup energetycznyc
R_stref=[60.0 40.0 40.0];             %[cm] grubosci ka�dej z stref rdzenia 
R_0 = sum(R_stref);                    %[cm] Promie� rdzenia - suma grubo�ci


%Moc = 1200;                             %[MW] moc termiczna reaktora
H = 100.0;                             %[cm] Wysokosc rdzenia                  
delta_r = 20.0;                        %[cm] Promieniowy wymiar ekstrapolowany rdzeni - za�o�ony i identyczny dla ka�dej grupy neutron�w
delta_z = 20.0;                        %[cm] Osiowy wymiar ekstrapolowany rdzenia - za�o�ony przyj�ty jednakowy dla ka�dej grupy
B_r = 2.405/(R_0+delta_r);             %[1/cm] Promieniowy parametr geometryczny reaktora walcowego - Geometrical Buckling 
B_z = pi/(H+2*delta_z);                %[1/cm] Osiowy parametr geometryczny reaktora walcowego
%N_A = 6.023e23;                       %[atom/g mol]  Sta�a Avogadra 
N_A = 0.6023;                          %Sta�a Avogadra/10^24
V_tot=  H*pi*R_0^2;                    %[cm3] ca�kowita obj�to�� rdzenia
h =[1 2 3];                            %Strefy rdzenia  %size(h,2)ilo�� stref - 3
R_s=[0 0 0];     

R_s(1)=R_stref(1);
R_s(2)=R_stref(1) + R_stref(2);
R_s(3)=R_stref(3) + R_stref(1) + R_stref(2);


V_stref = pi.*H.*[(R_s(1).^2)  (R_s(2).^2-R_s(1).^2)  (R_s(3).^2-R_s(2).^2)];   %[cm3] Obj�to�ci stref rdzenia
if(V_stref(2)<=0.0)                                                       %Zerowanie na wypadek braku strefy (odejmowanie!)
 V_stref(2)=0.0;    
elseif(V_stref(3)<=0.0)
 V_stref(3)=0.0;
end 
 
chi = [0.76 0.22 0.02 0];           %Chi - udzia� danej grupy energetycznej w widmie rozszczepieniowym [1 2 3 4] �r�d�o: FBR Waltara
%chi=chi';                           %Transpozycja wektora wierszowego do kolumnowego - wektory kolumnowe bardziej intuicyjne

%Sk�ad Reaktora
%Trzeba podzieli� rdze� na szereg cz�ci - p�aszcz rdze�/ obszary o r�nym
%wzbogaceniu w pierwszej wersji programu wybieramy trzy strefy promieniowe:1 - �rodkowa,
%2 - Zewn�trzna oraz 3- P�aszcz.
%Stefy charakteryzuja sie roznym wzbogaceniem i generalnie rozna
%zawartoscia pierwiastk�w wszelakiego typu
%Tworzymy macierze zawierajace dane dotyczace sk�adu tych stref.
%F_m - udzia� obj�to�ciowy danego materia�u (1..18) 

%Obliczenia niektorych wielkosci tutaj odbywaja sie niejako poza
%dodatkowymi funkcjami z tablicami materialowymi (Molar etc...) dla roznych pierwiastkow - niestety jest to konieczne 
%Typowy sk�ad obj�to�ciowy rdzenia Paliwo 30-45% S�d 35-45% Stal 15-20% B4C 1-2%
%Paliwo MOX typowo sk�ada si� z 20-25% PuO2 reszta to zubozony tlenek U238. 
%Na obecnym etapie paliwo jest �wie�e - tylko Pu239 i tylko U238 bez innych
%izotop�w i produkt�w rozszczepienia.

%DYGRESJA - niekotre wazniejsze rownania zastosowane �r�d�o: KTH Reactor Physics Lectures
%1/M = suma(w_i/M_i)
%M=suma(f_i*M_i)
%rho_i=w_i*rho
%rho_MOX=w_puo2*rho_UO2+w_uo2*rho_PuO2
%f_UO2=0.2 w tym wypadku i f_PuO2=0.8
%Przelicza sie w_i=f_i*M_i / M
%Ogolnie gdy np mamy Fe3O2 to N_Fe=3*N_Fe3O2  i  N_O=2*N_Fe3O2  a udzial
%maswoy  w_Fe = 3*M_fe /(3*M_fe + 2*M_O) !!!
%Example N235=rho_235*N_A/M_235 
%Ponizsze obliczenia opieraja sie na rozwazaniach z wyk�ad�w KTH, niestet
%FBR Waltara niewiele m�wi o tych zagadnianiach - cho� co jasne to s�
%podstawy podstaw fizyki reaktor�w, aczkolwiek jak pokaza�a praktyka
%sprawiaja� nawi�cej trudno�ci i to w samym Waltarze s� ma�e rozbie�no�ci/niejasno�ci.
%Bardzo u�yteczny opis znajduje si� w dokumencie Criticality Calculations with MCNP5:A Primer
%////////////////////////////////////////////////////////////////////////
%Modyfikacja sekcja Fuel - materia��w staje si� plutonem dla paliwa
%Sekcja Fertile staje si� uranem
%MOX FUEL
%/////////////////////////////////////////////////////////////
%Udzialy masowe tlenkow plutonu w paliwie MOX w przyblizeniu wzbogacenie
w_pluton = [0.156 0.17 0.0000];  %Musza byc rozne od zera!!!


w_uran = 1 - w_pluton;
%.///////////////////////////////////////////////////////


%w_pluton_1 = 0.15;     %wzbogacenie paliwa w pluton - fuel - strefa 1 inner core
%w_pluton_2 = 0.17;    %wzbogacenie w pluton w strefie 2
%w_pluton_3 = 0.0;      %wzbogacenie w pluton p�aszcza - strefa 3
%w_uran_1 = 1 - w_pluton_1; %wzbogacenie paliwa w uran - fertile - strefa 1 
%w_uran_2 = 1 - w_pluton_2;  %Wzbogacenie w uran strefa 2
%w_uran_3 = 1 - w_pluton_3;  %wzbogacenie w uran strefa 3 - p�aszcz

%PLUTONIUM    - sk�ad paliwa
%1 - Pu239  2 - Pu 240 3 - Pu242  4 - Pu242
  f_PuO2_1 = [1.0 0.0 0.0 0.0];
  f_PuO2_2 = [1.0 0.0 0.0 0.0];
  f_PuO2_3 = [1.0 0.0 0.0 0.0];

%URANIUM     -  sk�ad uranu 
%1 - Th232  2 - U233 3 - U235  4 - U238
 f_UO2_1 = [0.0 0.0 0.0 1.0];
 f_UO2_2 = [0.0 0.0 0.0 1.0];
 f_UO2_3 = [0.0 0.0 0.0 1.0];
 
  %///////////////////////////////////////////////////////////////////////
 %Masa molowa tlenkow izotopow plutonu
 M_PuO2_i = [Molar(14)+2*Molar(4) Molar(15)+2*Molar(4)  Molar(16)+2*Molar(4)  Molar(17)+2*Molar(4) ]; 
 %Masa molowa tlenkow izotopow uranu
 M_UO2_i =  [Molar(10)+2*Molar(4) Molar(11)+2*Molar(4) Molar(12)+2*Molar(4) Molar(13)+2*Molar(4)];
 
%M_UO2  = Molar(13)+2*Molar(4);              %masa molowa tylko UO2 
%M_PuO2 = Molar(14)+2*Molar(4);              %masa molowa tylko PuO2
M_PuO2_239 = Molar(14)+2*Molar(4);           %masa molowa tylko PuO2
M_PuO2_240 = Molar(15)+2*Molar(4);
M_PuO2_241 = Molar(16)+2*Molar(4);
M_PuO2_242 = Molar(17)+2*Molar(4);%
M_ThO2_232 = Molar(10)+2*Molar(4);
M_UO2_233  = Molar(11)+2*Molar(4);
M_UO2_235  = Molar(12)+2*Molar(4);
M_UO2_238  = Molar(13)+2*Molar(4);


%/////////////////////////////////PLUTON///////////////////////////
%Masy molowe plutonu w konkretnej strefie paliwowej


M_PuO2_mix_1 =  f_PuO2_1(1)*M_PuO2_239 + f_PuO2_1(2)*M_PuO2_240 +   f_PuO2_1(3)*M_PuO2_241   +  f_PuO2_1(4)*M_PuO2_242; 
M_PuO2_mix_2 =  f_PuO2_2(1)*M_PuO2_239 + f_PuO2_2(2)*M_PuO2_240 +   f_PuO2_2(3)*M_PuO2_241   +  f_PuO2_2(4)*M_PuO2_242; 
M_PuO2_mix_3 =  f_PuO2_3(1)*M_PuO2_239 + f_PuO2_3(2)*M_PuO2_240 +   f_PuO2_3(3)*M_PuO2_241   +  f_PuO2_3(4)*M_PuO2_242; 

M_PuO2_mix = [M_PuO2_mix_1 M_PuO2_mix_2 M_PuO2_mix_3];

%////////////////////////////////////URAN///////////////////////////
M_UO2_mix_1 = f_UO2_1(1)*M_ThO2_232 +   f_UO2_1(2)*M_UO2_233 +  f_UO2_1(3)*M_UO2_235 +  f_UO2_1(4)*M_UO2_238;
M_UO2_mix_2 = f_UO2_2(1)*M_ThO2_232 +   f_UO2_2(2)*M_UO2_233 +  f_UO2_2(3)*M_UO2_235 +  f_UO2_2(4)*M_UO2_238;
M_UO2_mix_3 = f_UO2_3(1)*M_ThO2_232 +   f_UO2_3(2)*M_UO2_233 +  f_UO2_3(3)*M_UO2_235 +  f_UO2_3(4)*M_UO2_238;

M_UO2_mix = [M_UO2_mix_1 M_UO2_mix_2 M_UO2_mix_3];
%/////////////////////////////////PALIWO Ostatecznie////////////////////
%Masa Molowa Paliwa wyjsciowego w danej strefie 1/M = suma(w_i/M_i)^-1
M_MOX_1 = ((w_pluton(1)/M_PuO2_mix(1)) + (w_uran(1)/M_UO2_mix(1)))^(-1);
M_MOX_2 = ((w_pluton(2)/M_PuO2_mix(2)) + (w_uran(2)/M_UO2_mix(2)))^(-1);
M_MOX_3 = ((w_pluton(3)/M_PuO2_mix(3)) + (w_uran(3)/M_UO2_mix(3)))^(-1);


M_MOX = [M_MOX_1 M_MOX_2 M_MOX_3];
%///////////////////////////////Udzia�y obj�to�ciowe tlenk�w plutonu w
%danej strefie dla danego paliwa MOX/fertile  f_i = w_i * M_mix / M_i    
f_pluton = w_pluton.*M_MOX./M_PuO2_mix;  %Udzia� objetosciowy izotopow tlenkow plutonu w paliwie MOX z podzialem na strefy
f_uran   = w_uran.*M_MOX./M_UO2_mix;

%UDZIA�Y MASOWE IZOTOP�W w tlenkach
%w_Pu_i(1) = Molar(14)/(M_PuO2_239);
%w_Pu_i(2) = Molar(15)/(M_PuO2_240);
%w_Pu_i(3) = Molar(16)/(M_PuO2_241);
%w_Pu_i(4) = Molar(17)/(M_PuO2_242);
%w_U_i(1) = Molar(10)/(M_ThO2_232);
%w_U_i(2) = Molar(11)/(M_UO2_233);
%w_U_i(3) = Molar(12)/(M_UO2_235);
%w_U_i(4) = Molar(13)/(M_UO2_238);
w_Pu_i(1:4)=0.0;
w_U_i(1:4)=0.0;
%Tu trzeba uwazac  f_i = w_i * M_mix / M_i !!! POPRAWIC
%f_PuO2_1 = [239  240 241 242];
for(i=1:4)
 w_PuO2_i_1(i) = f_PuO2_1(i).*M_PuO2_i(i)./M_PuO2_mix_1;     %udzia� tlenku plutonu i-tego izotopu w strefie 1 tzn w mieszaninie tlenkow plutonu w 1 strefie tlenkow moga wyhcodzic wieksze od 1!!!! etcc
 w_PuO2_i_2(i) = f_PuO2_2(i).*M_PuO2_i(i)./M_PuO2_mix_2;
 w_PuO2_i_3(i) = f_PuO2_3(i).*M_PuO2_i(i)./M_PuO2_mix_3;

 w_Pu_i(i) = Molar(13+i)./(M_PuO2_i(i));   %udzial i-tego izotopou plutonu w miszaninie tlenkow plutonu
 
 w_UO2_i_1(i) = f_UO2_1(i).*M_UO2_i(i)./M_UO2_mix_1;
 w_UO2_i_2(i) = f_UO2_1(i).*M_UO2_i(i)./M_UO2_mix_2;
 w_UO2_i_3(i) = f_UO2_1(i).*M_UO2_i(i)./M_UO2_mix_3;
 
 w_U_i(i) =  Molar(9+i)./(M_UO2_i(i));
end;

%Generalnie udzia� masowy danego izotopou w danej strefie to iloczyn 3
%czynnikow np: w(Pu239 w tlenku Pu239O2) * w(tlenek Pu239 O2 w ca�je
%miszaninie PuO2 ) * w( PuO2 w calym MOX) !!!!!!!!!!!!!!

%Udzialy masowe uranu i plutonu w Moxie
for (i=1:4)
w_Pu_i_MOX_1(i) = w_Pu_i(i).*w_PuO2_i_1(i).*w_pluton(1);
w_Pu_i_MOX_2(i) = w_Pu_i(i).*w_PuO2_i_2(i).*w_pluton(2);
w_Pu_i_MOX_3(i) = w_Pu_i(i).*w_PuO2_i_3(i).*w_pluton(3);

w_U_i_MOX_1(i) =  w_U_i(i).*w_UO2_i_1(i).*w_uran(1);
w_U_i_MOX_2(i) =  w_U_i(i).*w_UO2_i_2(i).*w_uran(2);
w_U_i_MOX_3(i) =  w_U_i(i).*w_UO2_i_3(i).*w_uran(3);
end;

%Udzialy masowe tlenu w miszaninie tlenkow
w_O_MOX_1 = 2*Molar(4)/M_MOX_1;
w_O_MOX_2 = 2*Molar(4)/M_MOX_2;
w_O_MOX_3 = 2*Molar(4)/M_MOX_3;

%Udzialy masowe tlenu w strefach
for i=1:3
 w_O_UO2(i)  = 2*Molar(4)./M_UO2_mix(i);
 w_O_PuO2(i) = 2*Molar(4)./M_PuO2_mix(i);
end

%//////////////////Pluton
Rho_TD_PuO2 =11.46;                         %[g/cm3] g�sto�� teoretyczna tlenku plutonu
 %///////////////Uran
Rho_TD_UO2  =10.97;                         %[g/cm3] gesto�� teoretyczna tlenku uranu

%M_fertile = M_UO2;
%Rho_fertile = Rho_TD_UO2*SD;
%w_fertile = 1.0;
%//////////////////////////MOX
Rho_TD_MOX = Rho_TD_UO2.*w_uran + Rho_TD_PuO2.*w_pluton;               %[g/cm3] g�sto�� teoretyczna MOXu.
TD = 0.95;                                                        %Procent teoretycznej gestosci paliwa dla paliwa zastosowanego     
SD = 0.88;                                                         %Smear Density - g�sto�� "wype�niania paliwa" - w obliczeniach neutronowych przyjmuje sie ze paliwo wypelnia cala koszulke 
Rho_MOX = Rho_TD_MOX*SD;                                          %[g/cm3] gestosc paliwa jadrowego tlenkowego UO2 lub PuO2 do obliczen j�drowych
%//////////////////////////////ABSORBER////////////////////////////
M_B4C = 4*Molar(2)+Molar(3);                                      %Obliczenia masy molowej W�gliku Boru B4C poch�anioacza w reaktorach predkich tylko BOR 10
Rho_B4C=2.52;                                                     %[g/cm3] g�sto�� B4C
w_B = 4*Molar(2)/(4*Molar(2)+Molar(3));                           %udzial masowy boru 10 w B4c
w_C = 1*Molar(3)/(4*Molar(2)+Molar(3));                           %udzial masowy w�gla w B4C

%/////////////////////////////CHLODZIWO////////////////////////////
w_Na = 1.0;                                                        %Przyjmujemy, �e w sodzie jest tylko s�d Na 23 czyli jego udzial masowy = 1.0
M_Na=Molar(5);                                                     %Masa molowa Sodu
Rho_Na=Rho(5);                                                     %G�sto�� sodu

%Stal SS316 stosowana w reaktorach pr�dkich: Fe, <0.03% C, 16-18.5% Cr, 10-14% Ni, 2-3% Mo, <2% Mn, <1% Si, <0.045% P,
%<0.03% S dla stali te wsp�czynniki s� masowo; pomini�to pomniejsze pierwiastki pod uwage brana jest: Fe, Cr, C, Mo, Ni �r�d�o:http://www.azom.com/article.aspx?ArticleID=863

%/////////////////////////////MATERIA� KONSTRUKCYJNY///////////////
w_Cr=0.17; w_Ni=0.12; w_C_steel=0.0003; w_Mo=0.02;                %Udzia�y masowe chromu, niklu, stali i molibdenu
w_Fe=1.0 - w_Cr - w_Ni - w_C_steel - w_Mo;                                %reszta sk�adu to �elazo
M_steel = ( w_Fe/Molar(7)+w_Cr/Molar(6)+w_Ni/Molar(8)+w_C_steel/Molar(3)+w_Mo/Molar(9) )^(-1);         %Masa molowa stali
Rho_steel=7.92;                                                   %[g/cm3] g�sto�� dla stali SS316                        

%////////////////////////////PRODUKTY ROZSZCZEPIENIA
%Dodane produkty rozszczepienia 
M_FP=Molar(18);
Rho_FP=Rho(18);
w_FP=1.0;




%Udzialy objetosciowe  F_m(i,j)  i-strefa rdzenia, j-materia�: Materia�y 1..18 
F_i(size(h,2),18)=0.0;   %Pocz�tkowo zerujemy wszystko - macierz udzia��w obj�to�ciowych - ta macierz jest dla pierwiastk�w F_m(STREFA 1...3, PIERWIASTEK 1...18)
%Dla tych materia��w mamy baze przekroj�w mikroskopowych

%Tworzymy dodatkowa macierz tylko dla paliwa, pochlaniacza, materialu konstrukyjnego i chlodziwa i %materialu rodnego
%Te materia�y to mieszaniny pierwiastk�w z tablicy F_i.
F(size(h,2),5)=0.0;    %F(Strefa 1..3, MATERIA� 1..5) Gdzie pierwszy argument materia�u to: 1- Paliwo  2-S�d/chlodziwo  3-Stal/Material konstrukcyjny   4-B4C/pochlaniacz   5 - Materia� rodny U238
%1 - Paliwo  - zamian w pluton!
%2 - S�d
%3 - Stal
%4 - Pochlaniacz
%5 - Materia� paliworodny - zamiana w Uran!!!
%Tworzymy macierze w i N gdzie w to udzia�y masowe oraz N ilo�� j�der inne
%dla kazdej strefy. w(i,j) N(i,j) i-strefa 1..3, k-materia� 1..18 
%Inicjalizacje macierzy
w_i(3,18)=0.0;          %Macierz udzia��w masowych dla wszystkich pierwiastk�w
N_i(3,18)=0.0;          %Macierz ilo�ci atom�w konkretnych pierwiastk�w
V_m(3,5)=0.0;           %Macierz obj�to�ci stref reaktora - 3 strefy po 5 g��wnych materia��w na stref�.
masa_m(3,5)=0.0;        %Macierz mas materia�u w danej strefie (paliwo, poch�aniacz etc)
Rho_i(3,18)=0.0;        %Macierz g�sto�ci danych materia��w w danej strefie rdzenia


%#STREFA 1 �RODEK RDZENIA 
%udzialy objetosciowe rdzenia F
F_fuel_1 = 0.30;
F_fuel_2 = 0.30;
F_fuel_3 = 0.60;

F(1,1)=F_fuel_1*f_pluton(1);     %pluton
F(1,2)=0.40;     %chlodziwo
F(1,4)=0.008; %  125;     %pochlaniacz
F(1,5)=F_fuel_1*f_uran(1);     %uran
F(1,3) = 1.0 - F(1,1) - F(1,2) - F(1,4) - F(1,5);  % stal to reszta

%#STERFA 2 ZEWNETRZNA CZESC RDZENIA o wyzszym wzbogaceniu
F(2,1) = F_fuel_2*f_pluton(2);       %pluton
F(2,2) = 0.40;                       %chlodziwo
F(2,5) = F_fuel_2*f_uran(2);         %uran
F(2,4) = 0.004;%   17;                       %pochlaniacz
F(2,3)=1.0 - F(2,1)-F(2,2)-F(2,5)-F(2,4); %stal to reszta

%#STREFA 3 PLASZCZ
F(3,1)=F_fuel_3*f_pluton(3);                    %pluton
F(3,5)=F_fuel_3*f_uran(3);                      %uran
F(3,2)=0.20;                   %chlodziwo 
F(3,4)=0.00;                   %pochlaniacz
F(3,3)=1 - F(3,4) - F(3,5) - F(3,2) - F(3,1);  %stal

%//////////////////////////////////KOMUNIKATY BLEDY
if(sum(F(2,:)) ~= 1)
    disp('udzialy nie sumuja sie do jednosci!!')
 %   error('udzialy nie sumuja sie do jednosci!!');
end
V_m(2,:)=F(2,:).*V_stref(2);

if(sum(F(1,:)) ~= 1)
    disp('udzialy nie sumuja sie do jednosci!!')
   % error('udzialy nie sumuja sie do jednosci!!');
end
V_m(1,:)=F(1,:).*V_stref(1); %obj�to�ci danego materia�u w stefie w strefie = udzia� obj�to�ciowy danego materia�u * obj�to�� strefy

if(sum(F(3,:)) ~= 1)
    disp('udzialy nie sumuja sie do jednosci!!')
    error('udzialy nie sumuja sie do jednosci!!');
end
V_m(3,:)=F(3,:).*V_stref(3);

%Trzeba wyznaczyc liczby j�der w 1cm3 
%N_s = N_A * gestosc/masa atomowa
%Znane s� g�sto�ci ka�dego materia�u w rdzeniu - za�o�one
%ge�to�� ka�dego pierwiastka to gesto�� materia�u * udzia� masowy danego
%pierwiastka czyli:   rho_m = w_m*rho_s
%Obliczamy ilo�ci c�asteczek ka�dego materia�u w ka�dej z trzech stref

% w_i(i,:) = [0.0 w_B w_C+w_C_steel w_O_PuO2 + w_O_UO2 w_Na w_Cr w_Fe w_Ni w_Mo 0.0 0.0 0.0 w_U w_Pu 0.0 0.0 0.0 0.0]
%Niektore tablice mog�y wcze�niej by� zadeklarowane ale w trakcie tworzenia kodu zosta�y nieumy�lnie powt�rzone
N_m(3,5)=0.0;                                              %inicjalizacja
M_m(3,5)=0,0;
Rho_m(3,5)=0.0;
%M_m = [M_MOX M_Na M_steel M_B4C M_fertile];                %Masy molowe materia��w
for i=1:3
% M_m(i,:) = [M_MOX(i) M_Na M_steel M_B4C M_MOX(i)];               
 M_m(i,:) = [M_PuO2_mix(i) M_Na M_steel M_B4C M_UO2_mix(i)];                %Masy% molowe materia��w to - moze by watpiliwe bo po modyfikacji zakladamy ze gestosc 5 mterialou w kazdej z 3 stref jest taka sama jak dla 1 strefy
 
% M_m(i,:) = [M_PuO2_mix(i) M_Na M_steel M_B4C M_UO2_mix(i)];   
 %Rho_m(i,:)  =[Rho_MOX(i)  Rho(5) Rho_steel Rho_B4C Rho_MOX(i)];  %zak�adamy ze ge�to�ci znacz�co si� nie zmieniaj�
 Rho_m(i,:)  =[Rho_TD_PuO2*SD  Rho(5) Rho_steel Rho_B4C Rho_TD_UO2*SD];
end

%Rho_m =[Rho_MOX Rho_Na Rho_steel Rho_B4C Rho_fertile];     %G�sto�ci materia��w



N_m = Rho_m.*N_A./M_m;                                     %ilo�� cz�steczek danego materia�u !!! nie j�der!!!
w_ii(3,5,18)= 0.0;  %Macierz pierwiastk�w 3 strefy, 5 materia��w, 18 pierwistk�w %bardzo og�lne podej�cie/ mozna prosciej mniej ogolnie

%w_ii wiekszosc to zera - de facto wykonujemy mase mnozen zero co moze
%zakrawac o bezsens ale mamy relatywnie og�lna metod�.
%W kazdej strefie udzial masowy przyjmujemy taki sam, 
%w_ii(1:3,1,:)= [0.0 0.0 0.0 w_O_MOX 0.0 0.0 0.0  0.0 0.0 0.0 0.0 w_U238_MOX w_Pu239_MOX 0.0 0.0 0.0 0.0];   %udzialy masowe w paliwie

%1. PLUTON UDZIALY MASOWE
%w_ii(1:3,1,4) = w_O_MOX;            %Zawartosc tlenu w paliwie
w_ii(:,1,4) = w_O_PuO2;        %od 1 do 3   

%Izotopy plutonu 14,15,16,17
%for i=1:4
% w_ii(:,1,13+i) =  [w_PuO2_i_1(i) w_PuO2_i_2(i) w_PuO2_i_3(i)];
%end 
%Udzial masowy danego tlenku izotopu plutonu w materiale pluton - 1
w_ii(1,1,14) = w_PuO2_i_1(1)*w_Pu_i(1);
w_ii(2,1,14) = w_PuO2_i_2(1)*w_Pu_i(1);
w_ii(3,1,14) = w_PuO2_i_3(1)*w_Pu_i(1);

w_ii(1,1,15) = w_PuO2_i_1(2)*w_Pu_i(2);
w_ii(2,1,15) = w_PuO2_i_2(2)*w_Pu_i(2);
w_ii(3,1,15) = w_PuO2_i_3(2)*w_Pu_i(2);

w_ii(1,1,16) = w_PuO2_i_1(3)*w_Pu_i(3);
w_ii(2,1,16) = w_PuO2_i_2(3)*w_Pu_i(3);
w_ii(3,1,16) = w_PuO2_i_3(3)*w_Pu_i(3);

w_ii(1,1,17) = w_PuO2_i_1(4)*w_Pu_i(4);
w_ii(2,1,17) = w_PuO2_i_2(4)*w_Pu_i(4);
w_ii(3,1,17) = w_PuO2_i_3(4)*w_Pu_i(4);



%5. MATERIA� PALIWORDONY UDZIALY MASOWE
w_ii(:,5,4) = w_O_UO2;


w_ii(1,5,10) = w_UO2_i_1(1)*w_U_i(1);
w_ii(2,5,10) = w_UO2_i_2(1)*w_U_i(1);
w_ii(3,5,10) = w_UO2_i_3(1)*w_U_i(1);

w_ii(1,5,11) = w_UO2_i_1(2)*w_U_i(2);
w_ii(2,5,11) = w_UO2_i_2(2)*w_U_i(2);
w_ii(3,5,11) = w_UO2_i_3(2)*w_U_i(2);

w_ii(1,5,12) = w_UO2_i_1(3)*w_U_i(3);
w_ii(2,5,12) = w_UO2_i_2(3)*w_U_i(3);
w_ii(3,5,12) = w_UO2_i_3(3)*w_U_i(3);

w_ii(1,5,13) = w_UO2_i_1(4)*w_U_i(4);
w_ii(2,5,13) = w_UO2_i_2(4)*w_U_i(4);
w_ii(3,5,13) = w_UO2_i_3(4)*w_U_i(4);

%2. CHLODZIWO UDZIALY MASOWE
w_ii(1:3,2,5) = w_Na;

%3. MATERIA�Y KONSTRUKCYJNE UDZIALY MASOWE
w_ii(1:3,3,3) = w_C_steel;
w_ii(1:3,3,6) = w_Cr;
w_ii(1:3,3,7) = w_Fe;
w_ii(1:3,3,8) = w_Ni;
w_ii(1:3,3,9) = w_Mo;




%4. POCH�ANIACZ UDZIA�Y MASOWE
w_ii(1:3,4,2) = w_B;  %Bor 10
w_ii(1:3,4,3) = w_C; %Wegiel

% W tym podejsciu trzeba utworzyc dodatkowo dwie macierze dla M_mm i Rho_ii o  takim samym rozmiarze jak w_ii
Rho_mm=[];
Rho_mm(3,5,18) = 0.0;
M_ii(3,5,18)   = 0.0;
N_ii(3,5,18)   = 0.0;

for(k=1:3)
 for(j=1:18)
  for(i=1:5)
   Rho_mm(k,i,j)=Rho_m(i);  %Mozna to prosciej wypelnic
  end
 end
end

for(k=1:3)
 for(j=1:18)
  for(i=1:5)
    M_mm(k,i,j)=M_m(i);  %Mozna to prosciej wypelnic
  end
 end
end

for(i=1:18)
 M_ii(1:3,1:5,i) = Molar(i);
end

%Dodatkowe przeliczenia gestosci efektywnej  - mozna to podwazyc nie jestem w 100% tego pewny
Rho_m3(3,5)=0.0;
%for(i=1:5)
% Rho_m3(1:3,i)=Rho_m(i);  %Macierz 3 wierszowa w kazdym wierszu Rho_m
%end
Rho_m3=Rho_m;  %Poprawione!!

%gestosc efektywna = gestoc materialu * udzial objetosciowy materialu w
%ca�o�ci obj�to�ci strefy pozniej wykorzystujemy iloczyn gestosci
%efektywnejw danej strefie i udzialu masowego do obliczenia ilosci czastek.
%Sam materia� ma jak�s ge�to�� np MOX 11 g/cm3 ale jak zalozymy ze to
%paliwo jest w ca�ej obj�to�ci rdzenia (homogenizacja) to gestosc nam
%zmaleje o stosunek objetosci rzeczywistej MOX do objetosci calego rdzenia
%czyli de facto F.

Rho_eff = Rho_m3.*F;
%Rho_eff = Rho_m3;
Rho_eff_i(3,5,18)=0.0;


for(i=1:18)
 Rho_eff_i(1:3,1:5,i) = Rho_eff;   %18 tablic 3x5 zawierajacych gestosci
% materialow np tlen lub uran moga byc w dwoch roznych metrialach dlatego tak to dziala
end

%Tutaj mam watpliwosci!!!


N_ii = N_A.*w_ii.*Rho_eff_i./M_ii;  %Macierz ilo�ci j�der w zalezno�ci od strefy, materia�u i pierwiastka

%Teraz sumujemy po materia�ach i dostajemy ilo�� j�der danego pierwiastka w strefie
%wiersze to strefy, kolumny to materia�y i 18 takich tablic daje nam ca�o��
%N_ii. Dla naszych obliczen sumujemy wpierw po kolumnach
%Ilo�ci cz�stek: N_i=Rho_i *N_A / M_i ,  gdzie: Rho_i = Rho_m * w_i 
N_i=[];
GG=[];
GG=sum(N_ii,2);      %dostajemy osiemnascie macierzy (wektorow 3x1 kolumnowych)  tak jakby to byla macierz (3,1,18) z sumami kolumn i trzeba sklecic te macierze w jedna macierz 3 na 18

%Macierz 3x18 z ilosciami j�der (1..18) danego pierwiastka w zaleznosci od strefy (1...3)
for(i=1:18)
 N_i = [N_i GG(1:3,1:1,i)];  %Macierze 3x18 3 steefy po 18 pierwiastkow - ilo�� cz�stek
end
N_i=N_i';   %18x3
%Znajmosc ilosci jader pozwala na wyliczenie makroskopowych przekrojow
%czynnych i w konsekwencji wsp�czynnik�w dyfuzjii neutron�w dla przybli�enia czterogrupowego.  
N_i_tr(18,4,3)=0.0;  %Macierz do obliczen przekroj�w makroskopowych

for(i=1:4)
 N_i_tr(:,i,:)=N_i(:,:);     %ta sama ilosc jader dla kazdej grupy - taki trik
end
N_i_f=N_i_tr; %Ta sa te same macierze zarowno dla tranposrtu jak i rozszczepien - To w zasadzie zbedne

%Ilosci neutronow powstajacych na jedno rozszczepienie w zaleznosci od pierwiastka i od grupy
ni(18,4) = 0.0;
ni = Sigma(1:18,1:4,5);   


%Przekroje transportowe
Sigma_tr_micro(18,4) = 0.0;
Sigma_f_micro(18,4)  = 0.0;
Sigma_sc_micro(18,4) = 0.0;
Sigma_a_micro(18,4) =0.0;


Sigma_tr_micro = Sigma(1:18,1:4,1);  %Mikroskopowy przekroj czynny transportowy dla 18 pierwiastkow i 4 grup
Sigma_f_micro  = Sigma(1:18,1:4,3);
Sigma_sc_micro = Sigma_s(1:18,1:4);
Sigma_a_micro = Sigma(1:18,1:4,2) + Sigma(1:18,1:4,3) + Sigma(1:18,1:4,4);   %Przekroj na absorbcje to suma przekrojow na capture/ fission i scattering

Sigma_f_micro2 = Sigma_f_micro;       %zachowanie tej tablicy dla pozniejszych obliczen

Sigma_f_micro  = Sigma_f_micro.*ni;    %Iloczyn makroskopowego przekroju czynnego i ilosci neutronow rozszczepieniowych to jest wykorzystywane glownie pozniej
Sigma_f_micro3 = Sigma_f_micro;       %Zachowanie tej tablicy w takiej postaci


%Tworzymy wieksze macierze wielowymiarowe dla obliczen
for(i=1:3)
 Sigma_tr_micro(1:18,1:4,i) = Sigma_tr_micro(1:18,1:4);   %3 razy to samo!!! (18x4x3)
 Sigma_f_micro(1:18,1:4,i)  = Sigma_f_micro(1:18,1:4);
 Sigma_sc_micro(1:18,1:4,i) = Sigma_sc_micro(1:18,1:4);
 Sigma_a_micro(1:18,1:4,i)  = Sigma_a_micro(1:18,1:4);
 
 Sigma_f_micro2(1:18,1:4,i) = Sigma_f_micro(1:18,1:4);
end

%Simga_tr - makroskopowy przekr�j czynny
%Macierze do dalszych obliczen
Sigma_tr(18,4,3) = 0.0;        %Macierz makroskopowych przekrojow na transport
Sigma_f(18,4,3)  = 0.0;        %Rozszczepienie
Sigma_sc(18,4,3) = 0.0;        %Rozpraszanie
Sigma_a(18,4,3)  = 0.0;        %Absorbcja
Sigma_f2(18,4,3) = 0.0;        %Sigma fission bez ni 

%Sigma_tr = Sigma_tr_micro.*N_i_tr.*10e-24;   %Macierz tr�jwymiarowa 18x4x3 z wartosciami makroskopowych przekrojow czynnych z podzialem na strefy i na grupy neutronow oraz pieriwastki
Sigma_tr = Sigma_tr_micro.*N_i_tr;   %nie trzeba 10^-24 - bo stosujemy barn-cm
%Sigma_f  = Sigma_f_micro.*N_i_f.*10e-24;   %Makroskopowe przekroje czynne na rozszczepienie z uwzglednieniem ze 1barn=10^-24 cm2
Sigma_f  = Sigma_f_micro.*N_i_tr;     %w sumie N_i_f to to samo co N_i_tr wiec niepotrzebnie 
%Sigma_sc = Sigma_sc_micro.*N_i_tr.*10e-24; %Makroskopowe przekroje czynne na rozpraszanie
Sigma_sc = Sigma_sc_micro.*N_i_tr;
%Sigma_a  = Sigma_a_micro.*N_i_tr.*10e-24;
Sigma_a  = Sigma_a_micro.*N_i_tr; 

Sigma_f2 = Sigma_f_micro2.*N_i_tr;


%Teraz sumujemy po pierwiastkach
Sigma_tr1 = sum(Sigma_tr,1);   %suma daje 3 wektory  4x1 trzeba zamienic na jedna tabilce 3x4
Sigma_f1  = sum(Sigma_f,1);
Sigma_sc1 = sum(Sigma_sc,1);
Sigma_a1  = sum(Sigma_a,1);
Sigma_f22  = sum(Sigma_f2,1);   %Suma


Sigma_tr_macro =[];
Sigma_f_macro  =[];
Sigma_sc_macro =[];
Sigma_a_macro  =[];
Sigma_f_macro2 =[];

%Spos�b zamiany kilka wektorow na tablice
for(i=1:3)
   Sigma_tr_macro = [Sigma_tr_macro; Sigma_tr1(:,:,i)];     %MAcierz 3x4 z makroskopowymi przekrojami transportowymi dla 3 stref i 4 grup neutron�w
   Sigma_f_macro  = [Sigma_f_macro;  Sigma_f1(:,:,i) ];
   Sigma_sc_macro = [Sigma_sc_macro; Sigma_sc1(:,:,i)];
   Sigma_a_macro  = [Sigma_a_macro;  Sigma_a1(:,:,i)];
   Sigma_f_macro2 = [Sigma_f_macro2; Sigma_f22(:,:,i)];   %Makroskopowe przekroje czynne bz wspolczynnika ni
end


D_g(3,4)=0.0;
D_g=1./(3.*Sigma_tr_macro);  %Wsp�czynniki dyfuzji neutron�w [cm]

%Macierz srednich dr�g swobodnych transportu
lambda_tr = [];
lambda_tr = 3.*D_g;
lambda_tr_avg=[mean(lambda_tr(:,1)) ,  mean(lambda_tr(:,2)), mean(lambda_tr(:,3)), mean(lambda_tr(:,4))]; %usredniamy dla prostoty


%DALSZA CZESC GEOMETRIA i WLASCIWY ALGORYTM

%Walec/ Dane wynikajace z elementarnych rozwaza� teorii dyfuzjii
%jednogrupowej. Przybli�enie: Transverse Leakage Aproximation -
%Prostopad�em przybli�enie ucieczki neutron�w - przej�cie z problemu
%dwuwymiarowego do problemu jednowymiarowego
% | 1 | 2 | 3 |...| k-1 | k | k+1 |... | N-1 | N |
%r=0                    |   |                   r=R
%---------------------->r(k-1/2)
%-------------------------->r(k+1/2)
%---------------------->|   |<-------------- dr(k)
%---->| r(2-1/2) 
%-------->| r(2+1/2)
%----------------------------------------------->|r(N+1/2)

%W kodzie
% | 1 | 2 | 3 |...| k-1 | k | k+1 |... | N-1 | N |
%r=0              |     |   |                   r=R
%r(1)=0         r(k-1) r(k)                    r(N+1)=R_0

%!!!!!!!!! W kodzie r(k) == r(k-1/2)  w �r�dle !!!!!!!!

%podzial=1...k-1,k,k+1....N
%Ilo�� | - lini podzia�u N+1 przypisane sa im pola i objetosci - teoria w
%raporcie 

%Podzia� obszaru obliczeniowego na N przedzia��w o jednakowych d�ugo�ciach
%Kod og�lnie napisany dla podzia��w o dowolonej d�ugo�ci
N = 100;                               %Ilo�� podzia��w domeny obliczeniowej
k = linspace(1,N,N)';                  %Indeks k numerujacy przedzia�y
g = [1 2 3 4]';                        %Wektor numeracji grup energii
r = linspace(0,R_0,N+1)';                 %[cm] ten wektor odpowiada kolejnym po�o�eniom przedzia��w siatki
rr = r(1:N);                               %Zmodyfikowany wektor po�o�en od 5-495 cm 99 element�w 
rr_col =  [rr; rr; rr; rr];
rr = [rr, rr, rr, rr];
dr = R_0/(N);                             %[cm] d�ugo�� elementarnego podzia�u dla siatki r�wnomiernej - w naszym modelu constans

%Warto�ci P�l oraz obj�to�ci
%ilo�� warto�ci jest N+1 
%A(1:N+1)=0.0;                        %pole odpowiadajace po�o�eniom
A = 2.*pi.*r.*H;                     %[cm2] wektor p�l A w kodzie to A(k-1,k) w �r�dle
%AA=A';
V(1:N)=0.0;                          %Objeto�ci odpowiadajace danym przedzia�om
V(k) = pi.*H.*((r(k+1).^2) - (r(k).^2));  %[cm3] obj�to�ci
V=V';
v2(k) = ((r(k+1).^2) - (r(k).^2));        %[cm2] Roznice kwadratow promieni dla danego przedzia�u - stosowane w obliczeniach
v2=v2';
v2_col=[v2; v2; v2; v2];   %4 razy po 100 v2
v22 =[v2 v2 v2 v2];     
FF = 0.0;

%Wsp�czynnik Dyfuzji - obliczana na bazie mikroskopowych przekroj�w czynnych oraz ilo�ci atom�w danego materia�u
%Mamy trzy strefy pojawia si� problem strefy przej�ciowej pomiedyz strefami
%W kazdej strefie wsp�czynniki dyfuzji sie nie zmieniaja

%Wsp�czynniki w w�z�ach- w�z��w jest  N (test N=100)
%R_stref=[150.0 200.0 150.0]; %D_g(3,4);
%Mikroskopowe tez sa potrzebne
Sigma_tr_gk =[];
Sigma_f_gk  =[];
Sigma_sc_gk  =[];
Sigma_a_gk = [];
Sigma_f2_gk = [];
D_gk=[];
%Sigma pomiedzy pktami k i k-1
%Przypissanie przekrojow czynnych strefom a w zasadzie kazdemu elementowi siatki podzialowej
for(j=1:N)
   if(r(j)<=R_s(1))
     Sigma_tr_gk  = [Sigma_tr_gk; Sigma_tr_macro(1,:)];
     Sigma_f_gk   = [Sigma_f_gk;  Sigma_f_macro(1,:)];
     Sigma_sc_gk  = [Sigma_sc_gk; Sigma_sc_macro(1,:)];
     Sigma_a_gk   = [Sigma_a_gk; Sigma_a_macro(1,:)];
     Sigma_f2_gk  = [Sigma_f2_gk; Sigma_f_macro2(1,:)];
     D_gk=[D_gk; D_g(1,:)];
     
   elseif( (r(j)<=(R_s(2)))&& (r(j)>R_s(1)))
      Sigma_tr_gk  = [Sigma_tr_gk; Sigma_tr_macro(2,:)];
      Sigma_f_gk   = [Sigma_f_gk;  Sigma_f_macro(2,:)];
      Sigma_sc_gk  = [Sigma_sc_gk; Sigma_sc_macro(2,:)];
      Sigma_a_gk   = [Sigma_a_gk; Sigma_a_macro(2,:)];
      Sigma_f2_gk  = [Sigma_f2_gk; Sigma_f_macro2(2,:)];
      D_gk=[D_gk; D_g(2,:)];
      
   elseif( (r(j)>(R_s(2))) && (r(j)<=R_0+2*dr))
      Sigma_tr_gk  = [Sigma_tr_gk; Sigma_tr_macro(3,:)];  %Macierze 100x4 
      Sigma_f_gk   = [Sigma_f_gk;  Sigma_f_macro(3,:)];
      Sigma_sc_gk  = [Sigma_sc_gk; Sigma_sc_macro(3,:)];
      Sigma_a_gk   = [Sigma_a_gk; Sigma_a_macro(3,:)];
      Sigma_f2_gk  = [Sigma_f2_gk; Sigma_f_macro2(3,:)];
      D_gk=[D_gk; D_g(3,:)];
      
   else
   end    
end    
%D_gk w sumie nie potrzebne bo sie inaczej przelicza!!
%Powstaja macierze Nx4 (tutaj np 100x4) zawierajaca czterogupowe przekroje
%dla kazdego wezla i czterogurpowe wspolczyynniki dyfuzji
%Przyst�pujemy do oblicze� Wsp�czynnik�w dyfuzji i przekrojow pomniedzy wezlami


D_gkk1(N-1,G)=0.0;   %efektywnye wspolczynniki dyfuzji pomiedzy wezlami siatki podzialowej jest ich N-1 (tutaj 99) G=4
%resszta jest wyznaczana najpewniej pozniej

%efektywne wspolczynniki dyfuzji pomiedzy w�z�ami obliczeniowymi
%for(=1:N-1)
for(k=2:N) %moze k-1
   D_gkk1(k,:) = (D_gk(k-1,:).*D_gk(k,:).*(dr+dr))./(D_gk(k-1,:).*dr + D_gk(k,:).*dr);   %czyli element k odnosi sie do przestreznieni pomiedzy k a k+1 np k=1 to przestrzen k a k+1
end    
%TEN wyraz D_gkk1 jest wielkosci 1xN-1 !!!! czyli jest dla wezlow w srodku rdzenia


%Do dalszych obliczen konieczne jest wyznaczenie cz�onu �rodla neutron�w
%rozszczepieniowych S_gk. Obliczamy go tak jak w ksiazce korzystajac z
%przekrojow  na rozszczepienie oraz wspolczynnikow ni ilosci
%neutronow powstajacych na jeden akt rozszczepienia, a wszystko to dla 4
%grup neutronow. Ponadto konieczne jest wyznaczenie makroskopowych
%przekrjow czynnych na przejscia z grupy g' -> g.

%k_eff=1.0;   %Wsp�czynnik mno�enia neutron�w

chi=chi';
for(i=1:N)
   chi_gk(i,:) = chi(:);
end

%Deklaracja Strumienia neutron�w Macierz N (zmienna k) wierszy - n warto�ci strumienia
%G (4) - zmienna g kolumny - odpowiadaja grupa energetycznym   phi(k,g) 
phi = zeros(N*G,1); 
C   = zeros(N*G,1);
AA   = [];
AA   = zeros(N*G);

%Macierze po 400 element�w plus 4 strumienie w srodku rdzenia i 4
%strumienie na kra�cach.
%Warunki brzegowe:  d phi/dr=0 dla r=0      oraz phi=0 gdy dla d�ugo�ci ekstrapolacji r=R+0.71lambda_tr
%wprowadzane sa punkty k=0 oraz k=N+1 w �rodku na na ko�cu reaktora.

%Pierwszy guess strumienia neutron�w - zwykle rozk�ad rozszczepieniowy /
%wartosci wzgledne; k_eff tez pierwszy guess
%Oczywiscie dostajemy rozklady strumienia neutronow ale wartosci zaleza juz od mocy reaktora.
%Dlatego trzeba dalej zalozyc moc reaktora i okreslic strumienie na bazie mocy
NNN=0.8e15;
k_eff   = 1.0;    %First Guess k_eff nie zero!
k_eff_0 = k_eff;

phi1_0(1:N,1) = NNN; %chi(1)*NNN;
phi2_0(1:N,1) = 0.0;%chi(2)*NNN;
phi3_0(1:N,1) = 0.0;%NNN*chi(3);
phi4_0(1:N,1) = 0.0;%NNN*chi(4);

phi_0 = [phi1_0; phi2_0; phi3_0; phi4_0];
phi_gk_0 = [];
phi_gk_0 = [phi1_0, phi2_0,phi3_0,phi4_0]; 
chi_gk_0 = [chi_gk(:,1); chi_gk(:,2); chi_gk(:,3); chi_gk(:,4)]; %Grupa za grupa 400x1



%Inicjalizujemy - tzn liczymy pierwszy raz wyraz rozszczepieniowy F.
wyraz =  phi_gk_0.*v22.*Sigma_f_gk;
FF = sum(wyraz,2);
FF = sum(FF);   %jedna liczba
F_old = FF;     %Przypisanie pierwszej wartosci obliczonej czlonu rozszczepieniowego dla iteracji    

eps = 1e-5;               %Poziom zbie�no�ci   roznice zwykle niewielkie dla 1e-4 - 1e-7
lambda = 0.5;             %Pierwszy guess lambdy - zmiana nie wplywa a wynik tylko nie 1

%///////////////////////////////////////////////////////
iteracja = 0;         %Licznik iteracji 
disp('iteracja: ')    

%//PROCES ITERACYJNY 

while( (abs(1.0-lambda) > eps) ) 

disp(iteracja)
Suma_0=[];   %#ok<NASGU>
Suma_0 = Sigma_f_gk.*phi_gk_0;    %Iloczyn strumieni i czlonow ni*Sigma macierz 100x4  N na G
Suma_0 = sum(Suma_0,2); %Dodane

Suma_gk_0 = [Suma_0; Suma_0; Suma_0; Suma_0];
%Suma_gk_0 = [Suma_0(:,1); Suma_0(:,2); Suma_0(:,3); Suma_0(:,4)]; %Grupa za grupa wektor 400x1

S_gk_01 =[];        %Suma taka ze macierz 100x4 -> 100x1 czyli suma wyrazow w wierszu - suma po grupach !!!!
S_gk_01 = (1/k_eff).*chi_gk_0.*Suma_gk_0; 
%Wyznaczenie sumy S_gk_02 Sigma_sc_gk to macierz 100x4 100 element�w
%podzialowych i 4 mozliwe reakcje przejscia g'->g z grupy do grupy dla zjawiska rozpraszania

S_gk_02 =[];
sumka=0.0;



%Pewnie da sie prosciej ale ja nie widze sposobu

for(g=1:G)
    for(k=1:N)         
         if(g==1)    
           S_gk_02 =[S_gk_02; 0];      %Brak wyrazu rozpraszania to mi sie nie podoba
         elseif(g==2) 
                 sumka =  phi_gk_0(k,1)*Sigma_sc_gk(k,1);    %wyraz rozpraszania 1->2 phi1*Sig(1->2) etc  !!!nie ma rozpraszania 1->4 tylko 1->2 i 1->3
                 S_gk_02 =[S_gk_02; sumka];        
         elseif(g==3)   
                 sumka = phi_gk_0(k,1)*Sigma_sc_gk(k,2) + phi_gk_0(k,2)*Sigma_sc_gk(k,3);
                 S_gk_02 =[S_gk_02; sumka];
         elseif(g==4)
                 sumka = phi_gk_0(k,3)*(Sigma_sc_gk(k,4));  
                 S_gk_02 =[S_gk_02; sumka];
         end
    end
end


%Ostatecznie cz�on �rod�owy dla w�z��w obliczeniowych
S_gk=[];
S_gk = S_gk_02 + S_gk_01;
%Wyraz C - prawa strona uk�adu r�wnan

C = S_gk.*v2_col;
%Wspolczynniki do rownania: alfa i beta
alfa_gk=[];
beta_gk=[];


%Z warunkow brzegowych
alfa_1g = 0.0; %Tak jest caly czas juz w macierzy alga_gk
alfa_gNplus1 = (D_gk(N,:).*(2.*(R_0+0.5.*dr)))./(0.5.*dr + 0.71.*lambda_tr_avg);    %wspolczynniki N+1 dla 4 grup droga transportu usredniona
%Strumienie z warunkow brzegowych
phiNg=0.0; %Strumien na dlugosci ekstrapolacji


alfa_gk = D_gkk1.*(2.*rr)./dr;
%Buckling w kierunku z B_z jest przyjety za identyczny dla kazdej grupy neutron�w
alfa_gk = [alfa_gk; [alfa_gNplus1(1) alfa_gNplus1(2) alfa_gNplus1(3) alfa_gNplus1(4) ] ];   %Dodatnie 101 wiersza z wartosciami alfa N+1

%Wyznaczenie wspolczynnikow beta
for(k=1:N)
   beta_gk(k,1) = alfa_gk(k,1) + alfa_gk(k+1,1) - D_gk(k,1)*B_z*B_z*v2(k) + Sigma_a_gk(k,1)*v2(k);        %Pierwsza grupa neutronow 
   beta_gk(k,2) = alfa_gk(k,2) + alfa_gk(k+1,2) - D_gk(k,2)*B_z*B_z*v2(k) + Sigma_a_gk(k,2)*v2(k); 
   beta_gk(k,3) = alfa_gk(k,3) + alfa_gk(k+1,3) - D_gk(k,3)*B_z*B_z*v2(k) + Sigma_a_gk(k,3)*v2(k); 
   beta_gk(k,4) = alfa_gk(k,4) + alfa_gk(k+1,4) - D_gk(k,4)*B_z*B_z*v2(k) + Sigma_a_gk(k,4)*v2(k);     
end    
%Koonstruujemy teraz macierz diagonalna rzadka A do rozwiazania ukladu rownan
%Macierz A sklada sie z 4 podmacierzy kazda na 1 grupe neutronow;

A1 = diag(beta_gk(:,1));
A1 = A1 + diag( -alfa_gk(2:N,1),1);
A1 = A1 + diag( -alfa_gk(2:N,1),-1);


A2 = diag(beta_gk(:,2));
A2 = A2 + diag( -alfa_gk(2:N,2),1);
A2 = A2 + diag( -alfa_gk(2:N,2),-1);

A3 = diag(beta_gk(:,3));
A3 = A3 + diag( -alfa_gk(2:N,3),1);
A3 = A3 + diag( -alfa_gk(2:N,3),-1);

A4 = diag(beta_gk(:,4));
A4 = A4 + diag( -alfa_gk(2:N,4),1);
A4 = A4 + diag( -alfa_gk(2:N,4),-1);

A0=zeros(N);

AA=[A1 A0 A0 A0;A0 A2 A0 A0; A0 A0 A3 A0; A0 A0 A0 A4];     %Macierz uk�adu

%Rozwiazujemy uk�ad r�wna�:

phi = AA\C;  %Strumie neutronow - metoda wlasciwa Matlabowi

%Stosujemy schemat iteracyjny dlatego tez bedzie trzeba policzyc dodatkowy czlon

phiX=[phi(1:N) phi(N+1:2*N) phi(2*N+1:3*N) phi(3*N+1:4*N)];

wyraz =  phiX.*v22.*Sigma_f_gk;
FF = sum(wyraz,2);
FF = sum(FF);
lambda = FF/F_old;
%FF = F_old + 1.4*(FF-F_old);   %SOR


phi_gk_0 = phiX; %Zmiana wartosci strumieni do dalszych obliczen
F_old = FF;
lambda_old = lambda;
k_eff_old = k_eff;   
k_eff  =  k_eff_old*lambda; 

iteracja=iteracja+1;

%k_iteracja = (k_eff - k_eff_old)/k_eff;
end   %KONIEC ITERACJI!!!! 

Power =[];
Power = Sigma_f2_gk.*phi_gk_0;
Power = sum(Power,2);     %Macierz mocy na jeden element element w watach
Power = Power./(2.93e+10);    %[W/cm3]   %Moc w lemencie siatki
%Uwzgledniamy cosinusoidalny rozklad mocy. Srednia 2/pi (srednia z sinusa) to pozwoli nam okreslic srednia moc osiowa. 
Power2 = Power.*(2/pi);   %Moc usredniona
%Power2 = Power.*(0.8);   %Moc usredniona
Power3 = Power2.*V;         %Moc w objetosci elemnetu siatki
Power4 = sum(Power3)*(1e-6);       %Moc ca�kowita [MW]   dla tego strumienia neutron�w


%Ca�kowity strumien
strumien =  sum(phiX,2);   %strumien ca�kowity w danym miejscu
toc
%Sklad rdzenia

format short
disp('Core Volume Fractions:')
disp('|  Plutonium |  Coolant |  Steel  | Absorber  | Uranium |')
disp('Inner Core')
disp(F(1,:))
disp('Outer Core')
disp(F(2,:))
disp('Radial blanket')
disp(F(3,:))
format short e

figure
plot(rr(:,1),phiX,'-','LineWidth',2);        %wykres strumieni grupowych
legend('G1 (>820 keV)','G2 (820-110 keV)','G3 (110-15 keV)','G4 (15-0 keV)','Location','SouthOutside','Orientation','Horizontal')  %
ylabel('Neutron flux [n/cm2s]')
xlabel('Radial distance [cm]')
hold on
plot([R_stref(1),R_stref(1)],[min(min(phiX)),0.7*max(max(phiX))],'k--');
hold on
plot([R_stref(1)+R_stref(2),R_stref(1)+R_stref(2)],[min(min(phiX)),0.7*max(max(phiX))],'k--');
hold on
plot([R_0,R_0],[min(min(phiX)),0.7*max(max(phiX))],'k--');
hold on
%text(20,0.5e14,0,'Inner Core','fontsize',8)
%hold on
%text(160,2e14,0,'Radial Blanket','fontsize',10)
%hold on
%text(120,2e14,0,'Outer Core','fontsize',10)
c0 = ['Total power       : ', num2str(Power4,'%10.2f \n'),' MWt'];
c1 = ['Max total flux    : ', num2str(max(strumien),'%10.2e \n'),' n/cm2s'];
c2 = ['Avg total flux    : ', num2str(mean(strumien),'%10.2e \n'),' n/cm2s'];
c3 = ['k eff             : ', num2str((k_eff),'%10.6f\n\n')];
c4 = ['Initial k eff     : ', num2str((k_eff_0),'%10.6f\n\n')];
c5 = ['Initial flux      : ', num2str(NNN,'%10.2e \n'),' n/cm2s'];

d = char(c0,c1,c2,c3,c4,c5);
s = char(d);
annotation('textbox',[0.6 0.8 0.1 0.1],'string',s,'FontName','Monospaced','EdgeColor','w','fontsize',8,'FitBoxToText','on','FitHeightToText','on','FaceAlpha',0,'LineStyle','none');
d00= ['           |Inner Core|Outer Core|Radial Blanket|'];
d0 = ['Plutonium : ',num2str(F(1,1),'%10.6f'),'  ',num2str(F(2,1),'%10.6f'),'    ',num2str(F(3,1),'%10.6f \n')];
d1 = ['Uranium   : ',num2str(F(1,5),'%10.6f'),'  ',num2str(F(2,5),'%10.6f'),'    ',num2str(F(3,5),'%10.6f \n')];
d2 = ['Sodium    : ',num2str(F(1,2),'%10.6f'),'  ',num2str(F(2,2),'%10.6f'),'    ',num2str(F(3,2),'%10.6f \n')];
d3 = ['Steel     : ',num2str(F(1,3),'%10.6f'),'  ',num2str(F(2,3),'%10.6f'),'    ',num2str(F(3,3),'%10.6f \n')];
d4 = ['Absorber  : ',num2str(F(1,4),'%10.6f'),'  ',num2str(F(2,4),'%10.6f'),'    ',num2str(F(3,4),'%10.6f \n')];


filename = ['Wykres_',num2str(NNN,'%10.2e'),'_',num2str((k_eff_0),'%10.3f'),'_',(num2str((w_pluton(1)),'%10.3f')),'_',(num2str((w_pluton(2)),'%10.3f')),'.jpg'];
filename2 = ['Wykres_',num2str(NNN,'%10.2e'),'_',num2str((k_eff_0),'%10.3f'),'_',(num2str((w_pluton(1)),'%10.3f')),'_',(num2str((w_pluton(2)),'%10.3f')),'.txt'];
filename1 = char(filename);
print ('-djpeg100','-r400',filename1);



suma1 = sum(w_Pu_i_MOX_1(1:4)) + sum(w_U_i_MOX_1(1:4));
suma2 = sum(w_Pu_i_MOX_2(1:4)) + sum(w_U_i_MOX_2(1:4));
suma3 = sum(w_Pu_i_MOX_3(1:4)) + sum(w_U_i_MOX_3(1:4));

wzbogacenie_1= [w_Pu_i_MOX_1./suma1 w_U_i_MOX_1./suma1];
wzbogacenie_2= [w_Pu_i_MOX_2./suma2 w_U_i_MOX_2./suma2];
wzbogacenie_3= [w_Pu_i_MOX_3./suma3 w_U_i_MOX_3./suma3];


e0 = ['Wzbogacenie w izotopy plutonu i uranu paliwa: '];
e00= ['Strefa 1 '];
e1 = ['Pu239  :',num2str(wzbogacenie_1(1),'%10.6f \n')];
e2 = ['Pu240  :',num2str(wzbogacenie_1(2),'%10.6f \n')];
e3 = ['Pu241  :',num2str(wzbogacenie_1(3),'%10.6f \n')];
e4 = ['Pu242  :',num2str(wzbogacenie_1(4),'%10.6f \n')];
e5 = ['Th232  :',num2str(wzbogacenie_1(5),'%10.6f \n')];
e6 = ['U233   :',num2str(wzbogacenie_1(6),'%10.6f \n')];
e7 = ['U235   :',num2str(wzbogacenie_1(7),'%10.6f \n')];
e8 = ['U238   :',num2str(wzbogacenie_1(8),'%10.6f \n')];

e01 = ['Strefa 2 '];
e11 = ['Pu239  :',num2str(wzbogacenie_2(1),'%10.6f \n')];
e21 = ['Pu240  :',num2str(wzbogacenie_2(2),'%10.6f \n')];
e31 = ['Pu241  :',num2str(wzbogacenie_2(3),'%10.6f \n')];
e41 = ['Pu242  :',num2str(wzbogacenie_2(4),'%10.6f \n')];
e51 = ['Th232  :',num2str(wzbogacenie_2(5),'%10.6f \n')];
e61 = ['U233   :',num2str(wzbogacenie_2(6),'%10.6f \n')];
e71 = ['U235   :',num2str(wzbogacenie_2(7),'%10.6f \n')];
e81 = ['U238   :',num2str(wzbogacenie_2(8),'%10.6f \n')];

e02 = ['Strefa 3 '];
e12 = ['Pu239  :',num2str(wzbogacenie_3(1),'%10.6f \n')];
e22 = ['Pu240  :',num2str(wzbogacenie_3(2),'%10.6f \n')];
e32 = ['Pu241  :',num2str(wzbogacenie_3(3),'%10.6f \n')];
e42 = ['Pu242  :',num2str(wzbogacenie_3(4),'%10.6f \n')];
e52 = ['Th232  :',num2str(wzbogacenie_3(5),'%10.6f \n')];
e62 = ['U233   :',num2str(wzbogacenie_3(6),'%10.6f \n')];
e72 = ['U235   :',num2str(wzbogacenie_3(7),'%10.6f \n')];
e82 = ['U238   :',num2str(wzbogacenie_3(8),'%10.6f \n')];



fid = fopen(char(filename2), 'wt');
fprintf(fid,'Fractional Volume \n');
fprintf(fid,'%s\n',d00);
fprintf(fid,'%s\n',d0);
fprintf(fid,'%s\n',d1);
fprintf(fid,'%s\n',d2);
fprintf(fid,'%s\n',d3);
fprintf(fid,'%s\n',d4);
fprintf(fid,'\n');
fprintf(fid,'%s\n',c0);
fprintf(fid,'%s\n',c1);
fprintf(fid,'%s\n',c2);
fprintf(fid,'%s\n',c3);
fprintf(fid,'%s\n',c4);
fprintf(fid,'%s\n',c5);
fprintf(fid,'\n');
fprintf(fid,'%s\n',e0);
fprintf(fid,'%s\n',e00);
fprintf(fid,'%s\n',e1);
fprintf(fid,'%s\n',e2);
fprintf(fid,'%s\n',e3);
fprintf(fid,'%s\n',e4);
fprintf(fid,'%s\n',e5);
fprintf(fid,'%s\n',e6);
fprintf(fid,'%s\n',e7);
fprintf(fid,'%s\n',e8);

fprintf(fid,'%s\n',e01);
fprintf(fid,'%s\n',e11);
fprintf(fid,'%s\n',e21);
fprintf(fid,'%s\n',e31);
fprintf(fid,'%s\n',e41);
fprintf(fid,'%s\n',e51);
fprintf(fid,'%s\n',e61);
fprintf(fid,'%s\n',e71);
fprintf(fid,'%s\n',e81);

fprintf(fid,'%s\n',e02);
fprintf(fid,'%s\n',e12);
fprintf(fid,'%s\n',e22);
fprintf(fid,'%s\n',e32);
fprintf(fid,'%s\n',e42);
fprintf(fid,'%s\n',e52);
fprintf(fid,'%s\n',e62);
fprintf(fid,'%s\n',e72);
fprintf(fid,'%s\n',e82);

fprintf(fid,'Udzial masowy tlenkow plutonu i uranu w miszance paliwowej: \n');
fprintf(fid,'%10.6f',w_pluton(1:3));
fprintf(fid,'\n');
fprintf(fid,'%10.6f',w_uran(1:3));


fclose(fid);


%Masa plutonu w rdzeniu srodkowym
%disp('kilogramy Pu239 na strefe')
masa_Pu1 = V_stref(1)*0.001*Molar(14)*N_i(14,1)*(10^24)/(N_A*10^24);
masa_Pu2 = V_stref(2)*0.001*Molar(14)*N_i(14,2)*(10^24)/(N_A*10^24);
masa_Pu3 = V_stref(3)*0.001*Molar(14)*N_i(14,3)*(10^24)/(N_A*10^24);
masa_Pu1;
masa_Pu2;
masa_Pu3;


toc



