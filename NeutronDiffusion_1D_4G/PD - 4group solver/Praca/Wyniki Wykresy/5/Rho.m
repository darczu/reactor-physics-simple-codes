%Funkcja zwraca wartosc gestosci materia�u 1..18
function [Rho]=Rho(i);
%Jednostka [g/cm3]
    %Zrodlo wikipedia 
        %Gestosci niestety zaleza od bardzo wielu czynnikow
%argument i to numer pierwiastka taki jak w funkcji Sigma
%{  
    Argmuenty funkcji:
    i - numer pierwiastka  1...18
        1 - Boron 
        2 - Boron-10
        3 - Carbon 
        4 - Oxygen
        5 - Sodium
        6 - Chormium
        7 - Iron
        8 - Nickel
        9 - Molybdenum
        10 - Thorium 232
        11 - Uranium 233 
        12 - Uranium 235
        13 - Uranium 238 
        14 - Plutonium 239
        15 - Plutonium 240
        16 - Plutonium 241
        17 - Plutonium 242 
        18 - Fisson Products (pairs) 
%}

if (i>18)
    error('Nie ma takiego materialu')
end    
    Rho(1:18)=0.0;


    Rho(1)= 2.46;
    Rho(2)= 2.46;
    Rho(3)= 2.267; %grafit   %amorficzny wegiel 1.8-2.1
    Rho(4)= 1.141;
    Rho(5)= 0.927;  % S�d w stanie ciek�ym w punkcie topnienia
    Rho(6)= 7.19;
    Rho(7)= 7.874;
    Rho(8)= 8.908;
    Rho(9)= 10.28;
    Rho(10)=11.7;
    Rho(11)=19.0;
    Rho(12)=19.0;
    Rho(13)=19.0;
    Rho(14)=19.9;
    Rho(15)=19.9;
    Rho(16)=19.9;
    Rho(17)=19.9;
    Rho(18)=(2.64+6.52+11.0+3*1.93)/6;               %Srednia z 6 (pow 6%) bardzo obfitych izotopow Sr, Zr, Tc, Cs, Cs,Cs 
    
    Rho=Rho(i);
    
        