%Plik zawiera funkcje na macierze rozpraszania 4-grupowe. Elastyczne i
%nieelastyczne. Sigma_g'->g Przekr�j czynny na przej�cie neutronu z grupy energetycznej
%g' do grupy g-tej.
%4-Group Scattering Matrices (Elastic % Inelastic) 
%�r�d�o: A.E.Waltar Fast Breeder Reactors
%{
 i - numer pierwiastka  1...18
        1 - Boron
        2 - Boron-10 TAKIE SAMO jak Boron!
        3 - Carbon 
        4 - Oxygen
        5 - Sodium
        6 - Chormium
        7 - Iron
        8 - Nickel
        9 - Molybdenum
        10 - Thorium 232
        11 - Uranium 233 
        12 - Uranium 235
        13 - Uranium 238 
        14 - Plutonium 239
        15 - Plutonium 240
        16 - Plutonium 241
        17 - Plutonium 242 
        18 - Fisson Products (pairs) 
  j - przekroj czynny na przejscie z grupy X do grupy  Y   X->Y
        1 -    1->2
        2 -    1->3
        3 -    2->3
        4 -    3->4
%}
function [a]=Sigma_s(i,j)
Sigma_s=[];
Sigma_s=zeros(18,4);

if (i>18)
    error('Nie ma takiego materialu')
elseif(j > 4)
    error('Nie ma takiego przejscia')
end

Sigma_s(1,:) = [0.45 0.0 0.33 0.15];
Sigma_s(2,:) = [0.45 0.0 0.33 0.15];
Sigma_s(3,:) = [0.39 0.0 0.38 0.27];
Sigma_s(4,:) = [0.40 0.0 0.26 0.16];
Sigma_s(5,:) = [0.51 0.0 0.17 0.13];
Sigma_s(6,:) = [0.32 0.02 0.13 0.05];
Sigma_s(7,:) = [0.37 0.03 0.08 0.03];
Sigma_s(8,:) = [0.29 0.02 0.11 0.06];
Sigma_s(9,:) = [0.71 0.04 0.11 0.06];
Sigma_s(10,:) = [1.15 0.05 0.21 0.05];
Sigma_s(11,:) = [0.87 0.06 0.12 0.04];
Sigma_s(12,:) = [0.77 0.02 0.20 0.04];
Sigma_s(13,:) = [1.32 0.07 0.22 0.05];
Sigma_s(14,:) = [0.79 0.04 0.13 0.07];
Sigma_s(15,:) = [0.72 0.02 0.22 0.05];
Sigma_s(16,:) = [0.78 0.04 0.31 0.05];
Sigma_s(17,:) = [0.59 0.06 0.18 0.05];
Sigma_s(18,:) = [1.75 0.08 0.2 0.09];


a=Sigma_s(i,j);



