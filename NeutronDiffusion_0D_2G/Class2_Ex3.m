 clear; clc; close all;  
% Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
% Class 2/Example 3 - Reactor Physics Tutorials  Rev 12-12-2016
% 2-group eigenvalue problem for infinite multiplying medium
k_inf_code = 1.0963093;
% Absorbtion = Total - Scattering (Transfer) 
abs1 = 8.90788529E-03;
abs2 = 5.85988164E-02 ;

A = [abs1, 0.0; ...
     0.0,  abs2];

outscat1to2 =  1.802136E-02;
outscat2to1 =  0.0;

Sout = [outscat1to2,  0.0   ; ...
            0.0,  outscat2to1 ];

inscat2to1 =   0.0;
inscat1to2 =   1.802136E-02;

Sin = [0.0, inscat2to1  ;  ...
      inscat1to2     ,  0.0  ];

chi1  = 1.0;
chi2  = 0.0;
chi = [chi1; chi2];

nufiss1 = 5.01007447E-03;
nufiss2 = 7.97062814E-02;
nufiss = [nufiss1, nufiss2];

F = chi*nufiss;
M = A + Sout - Sin;
B = inv(M) * F;

[phi, k] = eig(B);
 

% M phi = 1/k  F phi







