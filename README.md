# Reactor Physics Simple Codes

Some codes were re-developed and transferred to ANS-666 Reactor Physics course.
Most of this codes were developed many years ago. Hence, do not treat them seriosuly.

REACTOR-PHYSICS-COURSE-SIMPLE-CODES-PACKAGE   (RPCSCP)
A suite of simple computer codes to study the basics of nuclear reactor physics.
All codes/scripts were written in MATLAB



Last Revision 22-07-2017, Piotr Darnowski, piotr.darnowski@pw.edu.pl
REACTOR-PHYSICS-COURSE-SIMPLE-CODES-PACKAGE   (RPCSCP)
It is a suite of simple computer codes to study the basics of Nuclear Reactor Physics.
All codes/scripts were written in MATLAB environment.

The package indluces: diffusion 1D/0D codes, single and multigroup, point burnup calculations, point neutron kinetics and dynamics and point Xenon & Samarium transients.
The package was published initially 22-07-2017, it is still under development.
Contents:

NeutronDiffusion_0D_2G

NeutronDiffusion_1D_1G

NeutronDiffusion_1D_4G

NeutronKineticsDynamics_0D

NeutronKinetics_Xenon_and_Samarium_0D

FuelBurnup_0D



