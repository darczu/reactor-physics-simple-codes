%Program FASTBURN - skrypt do obslugi skryptow - rozpad4 i transmutacja2 
%Piotr Darnowski
%24.10.2012
clear
clc
tic
format long e
nr = 1;  %przypadek 1
nr = num2str(nr);

% =============OBLICZENIA DLA LEZ===============
%fileBOCEOC = fopen(['BOC_1_LEZout.txt'],'wt');
fileNall =[nr,'_NALL_LEZout.xlsx'];
fileBOC = [nr,'_N_BOC_LEZ_BOCout.xlsx'];
fileEOC = [nr,'_N_EOC_LEZ_BOCout.xlsx'];
% ===== wypalenie 140 dni 1 BOL 0% MA
czas=140; %dni
krok=2500; %s
fileCBOL1 = [nr,'_CaptureBOL_LEZ.xlsx'];
fileFBOL1 = [nr,'_FissionBOL_LEZ.xlsx'];
fileN0BOL1= [nr,'_N_BOL_LEZ.xlsx'];
fileNBOL1 = [nr,'_N_BOL_LEZ_140outA.xlsx'];
fileNBOL11= [nr,'_N_BOL_LEZ_140outB.xlsx'];
fileBBOL1 = [nr,'_B_BOL_LEZ_140outC.txt'];
[final2] = transmutacja2(czas,krok,fileCBOL1,fileFBOL1,fileN0BOL1,fileNBOL1,fileNBOL11,fileBBOL1);
ZAID  = final2(:,1); %ZAID
N_BOL = final2(:,4); %poczatkowe
N_140 = final2(:,2); %po wypaleniu

%fclose('all');
clear final2
% ===== wypalenie 280 dni 1 BOL 0% MA
czas=280; %dni
krok=2500; %s
fileCBOL1 = [nr,'_CaptureBOL_LEZ.xlsx'];
fileFBOL1 = [nr,'_FissionBOL_LEZ.xlsx'];
fileN0BOL1 =[nr,'_N_BOL_LEZ.xlsx'];
fileNBOL1 = [nr,'_N_BOL_LEZ_280outA.xlsx'];
fileNBOL11= [nr,'_N_BOL_LEZ_280outB.xlsx'];
fileBBOL1 = [nr,'_B_BOL_LEZ_280outC.txt'];
[final2] = transmutacja2(czas,krok,fileCBOL1,fileFBOL1,fileN0BOL1,fileNBOL1,fileNBOL11,fileBBOL1);
N_280 = final2(:,2); %po wypaleniu
%fclose('all');


%clear
clear final2
% ===== wypalenie 420 dni 1 BOL 0% MA
czas=420; %dni
krok=2500; %s
fileCBOL1 = [nr,'_CaptureBOL_LEZ.xlsx'];
fileFBOL1 = [nr,'_FissionBOL_LEZ.xlsx'];
fileN0BOL1 =[nr,'_N_BOL_LEZ.xlsx'];
fileNBOL1 = [nr,'_N_BOL_LEZ_420outA.xlsx'];
fileNBOL11= [nr,'_N_BOL_LEZ_420outB.xlsx'];
fileBBOL1 = [nr,'_B_BOL_LEZ_420outC.txt'];
[final2] = transmutacja2(czas,krok,fileCBOL1,fileFBOL1,fileN0BOL1,fileNBOL1,fileNBOL11,fileBBOL1);
N_420 = final2(:,2); %po wypaleniu
%fclose('all');
%do stworzenia outputu
N_BOC = 0.25.*(N_BOL + N_140 + N_280 + N_420); 
N_BOCout=[final2(:,1), N_BOC, final2(:,3)];
xlswrite(fileBOC,N_BOCout);
clear final2
%wypalanie BOC ->EOC 140 dni
N_EOC =[];
% =============== END OF CYCLE WYPALANIE ===== NEIAKTYWNE
%czas=140; %dni
%krok=2500; %s
%fileCBOL1 = [nr,'_CaptureBOL_LEZ.xlsx'];
%fileFBOL1 = [nr,'_FissionBOL_LEZ.xlsx'];
%fileN0BOL1 =[fileBOC]; %<==================================
%fileNBOL1 = [nr,'_N_EOC_LEZoutA.xlsx'];
%fileNBOL11= [nr,'_N_EOC_LEZoutB.xlsx'];
%fileBBOL1 = [nr,'_B_EOC_LEZoutC.txt'];  %trzeba przemnozyc wypalenie przez 4
%[final2] = transmutacja2(czas,krok,fileCBOL1,fileFBOL1,fileN0BOL1,fileNBOL1,fileNBOL11,fileBBOL1);
%N_EOC = final2(:,2); 


% ======== ZAPIS DO PLIKU
N_all=[N_BOL, N_140, N_280, N_420, N_BOC];
N_text ={'N_BOL ','N_140 ' , 'N_280 ', 'N_420 ', 'N_BOC ', 'N_EOC'};
xlswrite(fileNall, N_all);
xlswrite(fileNall, N_text,'Arkusz2');


%xlswrite(fileEOC,N_EOC);
fclose('all')


% =============OBLICZENIA DLA MEZ===============
% =============OBLICZENIA DLA HEZ===============
% =============OBLICZENIA DLA IBZ===============
% =============OBLICZENIA DLA AB===============