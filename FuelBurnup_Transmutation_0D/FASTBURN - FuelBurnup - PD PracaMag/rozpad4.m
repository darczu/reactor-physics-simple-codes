%Druga wersja programu rozpad - rozpad2 ktory liczy krok po korku
%Wersja programu - masowa (ilczaca na masach) 27.03.2012
%wtedy y0 jest masa
%function [f2, t2, dawka]= rozpad3(A,t_end,dt1,dt2,y0,name,Radio,SA)
function [f2, t2] = rozpad4(A,B,C,t_end,dt1,dt2,y0,name,ZAID,Num)
format long e

dt=dt1;  %krok czasowy
t2=0:dt1:t_end;
y=[];
f2=[]; 
rndclr=[];  %???

y=y0;  %wektor koncentracji
%inicjujemy
k=1;
 A = A.*(1e-24);  %Decay
 B = B.*(1e-24);  %Fission
 C = C.*(1e-24);  %Capture
 D = A + B + C;
 

for i=1:size(t2,2)
 f2=[f2 y];
% sprawdzic - usunac te mnozniki 


 y = expm(D*dt)*y; %sprawdzic to
  k
  k=k+1;
end
%Koniec glownej petli


name=[name; 'all' ]; %Wektor nazw

figure
for i=1:size(y0)
 rndclr=[rndclr ; rand, rand, rand];   %Losowanie koloru
% loglog(t2,f2(i,:),'color',rndclr(i,:),'LineWidth',2) 
 semilogy(t2,f2(i,:),'color',rndclr(i,:),'LineWidth',2) 
 %plot(t2,f2(i,:),'color',rndclr(i,:),'LineWidth',2) 
 hold on
 semilogy(t2,sum(f2(1:end,:),1),'-r','LineWidth',3)
end

%Pluton
%semilogy(t2,wynik1(9,:),'-o',t2,wynik1(5,:),'--')

 
hold on
 legend(name,'Location','NorthEastOutside')
grid
xlim([100 t_end]);
ylim([1e-10 2*max(y0)]);
xlabel('Czas, [s]')
ylabel('Koncentracja [at/barn-cm]')

figure

%hold on

end
