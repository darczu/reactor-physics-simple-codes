%Druga wersja programu rozpad - rozpad2 ktory liczy krok po korku
%Wersja programu - masowa (ilczaca na masach) 27.03.2012
%wtedy y0 jest masa
%function [f2, t2, dawka]= rozpad3(A,t_end,dt1,dt2,y0,name,Radio,SA)
function [f2, t2] = rozpad3(A,B,C,t_end,dt1,dt2,y0,name,ZAID,Num)
format long e

dt=dt1;  %krok czasowy
t2=0:dt1:t_end;
y=[];
f2=[]; 
rndclr=[];  %???

y=y0;  %wektor koncentracji
%inicjujemy
k=1;
for i=1:size(t2,2)
 f2=[f2 y];
 D = A + B + C;
 y = expm(D*dt).*y; %sprawdzic to
  k
  k=k+1;
end
%Koniec glownej petli

name=[name ; 'TRU']; %Wektor nazw

figure
for i=1:size(y0)
 rndclr=[rndclr ; rand, rand, rand];   %Losowanie koloru
 loglog(t2,f2(i,:),'color',rndclr(i,:),'LineWidth',2) 
 hold on
end
loglog(t2,sum(f2(4:end,:),1),'-r','LineWidth',3)
 
hold on
 legend(name,'Location','NorthEastOutside')
grid
xlim([1e1 t_end]);
ylim([1e-4 2*max(y0)]);
title('Actinides mass per initial one gram of whole mixture')
xlabel('Time, [y]')
ylabel('Mass per gram of mixture, [grams of isotope/gram of SF]')

%hold on

 
 
 
end
