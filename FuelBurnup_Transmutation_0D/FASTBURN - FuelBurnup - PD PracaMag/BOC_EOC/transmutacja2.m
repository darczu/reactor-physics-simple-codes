%Program Transmutacja FASTBURNER
%Start 4.10.2012 Praca Magisterska Piotr Darnowski
%Zmodyfikowany program Transmutacja-rozpady napisany na zaliczenie przedmiotu "Paliwa J�drowe"
function [final2]=transmutacja2(czas,krok,fileC,fileF,fileN0,fileN, fileNN, fileB)
%czas - czas wypalania [dni]

%clear
%clc
format long e
fileBurn = fopen(fileB,'wt');
[L_rozp, nazwy1]=xlsread('Decay.xlsx'); %szczytanie wartosci stalych rozpadu i nazw
%[L_capture, nazwy2]=xlsread('CaptureBOL_1LEZ.xlsx'); %wczytanie wartosci iloczynow strumieni i przekrojow dla absorbcji
[L_capture, nazwy2]=xlsread(fileC); 
%[L_fission, nazwy3]=xlsread('FissionBOL_1LEZ.xlsx'); %szczytanie ilosczynow przekrojow na fission i strumieni
[L_fission, nazwy3]=xlsread(fileF); 
izotop1 = nazwy1(2:end,1);    
izotop2 = nazwy2(2:end,1);
izotop3 = nazwy3(2:end,1);  %szczytanie nazwy izotopu
[izotop1 izotop2 izotop3] %sprawdzenie czy sie zgadza
izotop=izotop1;

t_end=czas;  %czas koncowy obliczen w dniach
t_end=t_end*24*60*60;  %seconds
%dt1=2500;  %poczatkowy krok czasowy [s] %250s
dt1 = krok;
dt2=250; %koncowy krok czasowy NIEPOTRZEBNE

%N0 = xlsread('N_BOL_1LEZ.xlsx') %wczytanie udzialow pierwiastkow i nr ZAID
N0 = xlsread(fileN0) %wczytanie udzialow pierwiastkow i nr ZAID
ZAID = N0(:,1);  %wektor ZAIDow
N_i = N0(:,2);  %wektor koncentracji [at/b-cm]
Num = N0(:,3);  %numer izotopu do inputu MCNP


[wynik1, czas1] = rozpad5(L_rozp,L_fission,L_capture,t_end,dt1,dt2,N_i,izotop,ZAID,Num);
%wynik1 = rozpad3(L_rozp,t_end,dt1,dt2,N0,izotop,R0,SA0)
%semilogy(czas1./(60*60*24),wynik1(9,:),'-',czas1./(60*60*24),wynik1(5,:),'--')
%pluton239  i U238

%wynik ostateczny
poczatek=N_i;
koniec=wynik1(:,size(wynik1,2));

rndclr=[]; 
name=[];
figure
for i=1:size(N_i,1)
    %if((koniec(i)>0))
     name = [name, izotop(i)]   
     rndclr=[rndclr ; rand, rand, rand];  
     semilogy(czas1./(60*60*24),wynik1(i,:),'color',rndclr(i,:),'LineWidth',2)
     grid off
     hold on
    %end
end
hold on
%suma aktynowcow = all - FP
 semilogy(czas1./(60*60*24),sum(wynik1(1:end-1,:),1),'--b')
%suma TRU
hold on
 semilogy(czas1./(60*60*24),sum(wynik1(8:end-1,:),1),'--m')
%suma MA
hold on
 semilogy(czas1./(60*60*24),sum(wynik1(15:end-1,:),1),'--r')
hold on
 
xlim([0 t_end/(60*60*24)]);
ylim([1e-12 0.1]);
xlabel('Czas [dni]');
ylabel('Koncentracja [at/barn-cm]');
name=[name, 'Aktynowce', 'TRU','MA'];
legend(name,'Location','NorthEastOutside');
title([fileN]);
%disp('ZAID | NUM | N0 | N(koniec)');
final=[ZAID ZAID koniec Num];
final2 = [ZAID, koniec, Num, poczatek];

%zapis do pliku
xlswrite(fileN,final);
xlswrite(fileNN,final2)

%Obliczenie wypalenia
Lfsum = L_fission(1:end-1,1:end-1);
Lfsum2 = abs(sum(Lfsum,1))'; %macierz strumien*N*czas*XS
koniec2=koniec(1:end-1);
poczatek2=poczatek(1:end-1);
Burnup1 = 100.*sum((Lfsum2.*koniec2.*t_end.*(1e-24)),1); %licznik
Burnup2 = sum(poczatek2,1);  %mianownik
Burnup=Burnup1./Burnup2;  %% at - wypalenie
BurnupGWD = Burnup*10; %GWd/tHM

fprintf(fileBurn,['# ',fileN,'\n']);
fprintf(fileBurn,[num2str(Burnup),' [percent at] \n']);
fprintf(fileBurn,[num2str(BurnupGWD),' [GWd/tHM] \n']);
fprintf(fileBurn,['DLA EOC trzeba to oblicztc inaczej \n']);
fprintf(fileBurn,[num2str(Burnup1),' - licznik [%at] \n']);
fprintf(fileBurn,[num2str(Burnup2),' - mianownik [%at] jeszcze razy 4 \n']);
fprintf(fileBurn,['mianownik z obliczen dla swiezego paliwa w stanie BOL lub BOC \n']);

%obliczenia CR - Conversion Ratio po wszystkich HM na poaczatku wypalania i
%na koncu
Lcsum = L_capture(1:end-1,1:end-1);
Lcsum2 = abs(diag(Lcsum));
%Lcsum2 = abs(sum(Lcsum,1))'; %macierz strumien*N*czas*XS
CR1 = sum(Lcsum2.*poczatek2,1);
CR2 = sum((Lcsum2+Lfsum2).*poczatek2,1);
CR3 = sum(Lcsum2.*koniec2,1);
CR4 = sum((Lcsum2+Lfsum2).*koniec2,1);
CR = CR1./CR2;
CRR = CR3./CR4;
fprintf(fileBurn,['CR poczatkowe: ',num2str(CR),'\n']);
fprintf(fileBurn,['CR koncowe: ',num2str(CRR),'\n']);


