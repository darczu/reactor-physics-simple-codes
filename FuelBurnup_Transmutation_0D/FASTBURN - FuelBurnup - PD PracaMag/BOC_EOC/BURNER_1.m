%Skrypt Burner 26.10.2012 Piotr Darnowski obsluguje FASTBURN2
%BURNER_1 jest zmodyfikowana wersja BURNERa przeznaczona do obliczania
%wypalania z BOC do EOC
%data: 7.11.2012
%odpala programy
clear
clc
format long e
numer=1;
area='LEZ';
krok=280; %sekund 280
%XSflux = 'LEZ';  %flaga do wyboru Macierzy Capture i Fission LEZ lub HEZ
tic
FASTBURN2_1(1,'LEZ',krok,'LEZ')
FASTBURN2_1(1,'MEZ',krok,'LEZ')
FASTBURN2_1(1,'IBZ',krok,'LEZ')
FASTBURN2_1(1,'HEZ',krok,'HEZ')
FASTBURN2_1(1,'AB',krok,'HEZ')

FASTBURN2_1(2,'LEZ',krok,'LEZ')
FASTBURN2_1(2,'MEZ',krok,'LEZ')
FASTBURN2_1(2,'IBZ',krok,'LEZ')
FASTBURN2_1(2,'HEZ',krok,'HEZ')
FASTBURN2_1(2,'AB',krok,'HEZ')

FASTBURN2_1(3,'LEZ',krok,'LEZ')
FASTBURN2_1(3,'MEZ',krok,'LEZ')
FASTBURN2_1(3,'IBZ',krok,'LEZ')
FASTBURN2_1(3,'HEZ',krok,'HEZ')
FASTBURN2_1(3,'AB',krok,'HEZ')

toc