%Program Transmutacja FASTBURNER
%Start 4.10.2012 Praca Magisterska Piotr Darnowski
%Zmodyfikowany program Transmutacja-rozpady napisany na zaliczenie przedmiotu "Paliwa J�drowe"
clear
clc
tic
format long e

[L_rozp, nazwy1]=xlsread('Decay.xlsx'); %szczytanie wartosci stalych rozpadu i nazw
[L_capture, nazwy2]=xlsread('Capture.xlsx'); %wczytanie wartosci iloczynow strumieni i przekrojow dla absorbcji
[L_fission, nazwy3]=xlsread('Fission.xlsx'); %szczytanie ilosczynow przekrojow na fission i strumieni
izotop1 = nazwy1(2:end,1);    
izotop2 = nazwy2(2:end,1);
izotop3 = nazwy3(2:end,1);  %szczytanie nazwy izotopu
[izotop1 izotop2 izotop3] %sprawdzenie czy sie zgadza
izotop=izotop1;

t_end=100;  %czas koncowy obliczen w dniach
t_end=t_end*24*60*60;  %seconds
dt1=100;  %poczatkowy krok czasowy
dt2=100; %koncowy krok czasowy

N0 = xlsread('N_1.xlsx') %wczytanie udzialow pierwiastkow i nr ZAID
ZAID = N0(:,1);  %wektor ZAIDow
N_i = N0(:,2);  %wektor koncentracji [at/b-cm]
Num = N0(:,3);  %numer izotopu do inputu MCNP


[wynik1, czas1] = rozpad4(L_rozp,L_fission,L_capture,t_end,dt1,dt2,N_i,izotop,ZAID,Num)
%wynik1 = rozpad3(L_rozp,t_end,dt1,dt2,N0,izotop,R0,SA0)

%pluton239  i U238
semilogy(t2,wynik1(9,:),'-o',t2,wynik1(5,:),'--')
toc


%wynik ostateczny
poczatek=N_i;
koniec=wynik1(:,size(wynik1,2));
[Num poczatek koniec]
%trzeba wprowadzic dyskryminacje co mniejsze od 1e-15 to odpada
%zapis do pliku

