%Program FASTBURN2 - skrypt do obslugi skryptow - rozpad4 i transmutacja2 
% w wersji FASTBURN2 jest podfunkcja programu FASTBURNER
%Piotr Darnowski
%25.10.2012
%wypalania z BOC do EOC
%data: 7.11.2012 zmodyfikowany
%odpala programy  
function []=FASTBURN2_1(nr,st,krok,XSflux)
%clear
%clc
tic
format long e
%nr = 1;  %przypadek 1  BURNER
nr = num2str(nr);

fileNall =[nr,'_NALL_',st,'out.xlsx'];
%fileBOC = [nr,'_N_BOC_',st,'_BOCout.xlsx'];
fileBOC = [nr,'_N_BOC_',st,'.xlsx'];
fileEOC = [nr,'_N_EOC_',st,'_BOCout.xlsx'];
% ===== wypalenie 140 dni 
czas=140; %dni
%{
fileCBOL1 = [nr,'_CaptureBOL_',XSflux,'.xlsx'];
fileFBOL1 = [nr,'_FissionBOL_',XSflux,'.xlsx'];
fileN0BOL1= [nr,'_N_BOL_',st,'.xlsx'];
fileNBOL1 = [nr,'_N_BOL_',st,'_140outA.xlsx'];
fileNBOL11= [nr,'_N_BOL_',st,'_140outB.xlsx'];
fileBBOL1 = [nr,'_B_BOL_',st,'_140outC.txt'];
[final2] = transmutacja2(czas,krok,fileCBOL1,fileFBOL1,fileN0BOL1,fileNBOL1,fileNBOL11,fileBBOL1);

N_BOL = final2(:,4); %poczatkowe
N_140 = final2(:,2); %po wypaleniu

%fclose('all');
clear final2
% ===== wypalenie 280 dni 
czas=280; %dni
%krok=2500; %s
fileCBOL1 = [nr,'_CaptureBOL_',XSflux,'.xlsx'];
fileFBOL1 = [nr,'_FissionBOL_',XSflux,'.xlsx'];
fileN0BOL1 =[nr,'_N_BOL_',st,'.xlsx'];
fileNBOL1 = [nr,'_N_BOL_',st,'_280outA.xlsx'];
fileNBOL11= [nr,'_N_BOL_',st,'_280outB.xlsx'];
fileBBOL1 = [nr,'_B_BOL_',st,'_280outC.txt'];
[final2] = transmutacja2(czas,krok,fileCBOL1,fileFBOL1,fileN0BOL1,fileNBOL1,fileNBOL11,fileBBOL1);
N_280 = final2(:,2); %po wypaleniu
%fclose('all');
%clear
clear final2
% ===== wypalenie 420 dni 
czas=420; %dni
%krok=2500; %s
fileCBOL1 = [nr,'_CaptureBOL_',XSflux,'.xlsx'];
fileFBOL1 = [nr,'_FissionBOL_',XSflux,'.xlsx'];
fileN0BOL1 =[nr,'_N_BOL_',st,'.xlsx'];
fileNBOL1 = [nr,'_N_BOL_',st,'_420outA.xlsx'];
fileNBOL11= [nr,'_N_BOL_',st,'_420outB.xlsx'];
fileBBOL1 = [nr,'_B_BOL_',st,'_420outC.txt'];
[final2] = transmutacja2(czas,krok,fileCBOL1,fileFBOL1,fileN0BOL1,fileNBOL1,fileNBOL11,fileBBOL1);
N_420 = final2(:,2); %po wypaleniu
%fclose('all');
%do stworzenia outputu
N_BOC = 0.25.*(N_BOL + N_140 + N_280 + N_420); 
N_BOCout=[final2(:,1), N_BOC, final2(:,3)];
xlswrite(fileBOC,N_BOCout);
clear final2

%}
%wypalanie BOC ->EOC 140 dni
N_EOC =[];
% =============== END OF CYCLE WYPALANIE =====  7.11.2012
czas=140; %dni

fileCBOC1 = ['CaptureBOC_',[nr,XSflux],'.xlsx']
fileFBOC1 = ['FissionBOC_',[nr,XSflux],'.xlsx'];
fileN0BOC1 =[fileBOC]; %<==================================
fileNBOC1 = [nr,'_N_EOC_',st,'outA.xlsx'];
fileNBOC11= [nr,'_N_EOC_',st,'outB.xlsx'];
fileBBOC1 = [nr,'_B_EOC_',st,'outC.txt'];  %trzeba przemnozyc wypalenie przez 4
[final2] = transmutacja2(czas,krok,fileCBOC1,fileFBOC1,fileN0BOC1,fileNBOC1,fileNBOC11,fileBBOC1);
ZAID  = final2(:,1); %ZAID
N_EOC = final2(:,2); 
N_BOC = final2(:,4);

% ======== ZAPIS DO PLIKU
N_all=[ZAID, N_BOC, N_EOC];
N_text ={'ZAID ',' N_BOC ','N_EOC'};
xlswrite(fileNall, N_all);
xlswrite(fileNall, N_text,'Arkusz2');
xlswrite(fileEOC,N_EOC);
fclose('all')

toc
