clear; clc; close all;  
% Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
% Class 5 Examples - Reactor Physics Tutorials  Rev 17-01-2016
%
%%% EXERCISE 2 - Spatial Diffusion with fission source and cirticality search %%%
% Solve one speed neutron diffusion equation in 1D slab geometry. Assume symmetry - zero flux condition on the right side and left hand side .
% Solve problem for fission source term
% Medium is multiplying
% Core data are based on PWR core discussed during Class 2.
% Slab half-thincknes:    H = 180 cm
% Diffusion coeffcient:   D = 9.2106 cm
% Macroscopic abs. XS: Sigma_a  0.1532 1/cm
% Macroscopic fission XS times nu:  Sigma_nuf =   0.1567 1/cm
% 
H = 200;           %slab thickness
a = H/2;	       %half-thickness
D = 9.2106;        %diffusion coeffcient
Sigma_a =  0.1532; %Abs. macro XS
Sigma_nuf =   0.1567; % 1/cm
N = 100;	           %nodalization number of divisions
nodes = N+1; 	   %number of nodes
h = H/N;		   %constant node interval - step
S = 3e14;     %constant source
L = sqrt(D/Sigma_a);  %Diffusion length
c = 1;             %c=0 - plane, c=1 cylindrical, c=2 - spherical
%Convergence criteria
epsilon1 = 1e-7;
epsilon2 = 1e-7;


% SOLUTION


% Guess initial fission source S(0) and k(0)
phi = ones(N+1,1);
S = (1e14)*Sigma_nuf.*phi;  %guess
k = 1.0;   %guess
Source = S/k;			%initial source term
eps1 = 10.0; eps2 = 10.0;
iter = 0;
%OUTER ITERATIONS
while(and((eps1>epsilon1),(eps2>epsilon2)))
  iter = iter + 1
  phi = NeutronSolver(N,D,h,Sigma_a,Source);  % INNER ITERATIONS - RESULTS phi(n+1)
  
  S_old = S; 
  S = Sigma_nuf.*phi;                         %S(n+1)=F*phi(n+1)
  
  k_old = k;
  k =   (sum(S))/(sum(Source));            %k(n+1) = Integral(S(n+1))/Integral(S(n))*k(n)
  
  Source = S/k;
  
  eps1 = abs((k - k_old)./k);
  eps2 = max(abs((S-S_old)./S));
end



%Space
x = linspace(-a,a,nodes)';
%x = linspace(0,a,nodes);

%Analitical solution - to be found in Lewis
%phiA = (1- (cosh(x./L)./cosh(a./L))).*(Source/Sigma_a); %plane
%phiA = (Source./Sigma_a).*cos(pi.*x./a);

phi = phi./max(phi);
phiA = (1).*cos(pi.*x./H);


%phiA = phiA';
%Plot
figure(1)
hold on;
plot(x,phi,'or','LineWidth',2)
plot(x,phiA,'-b','LineWidth',2)


%Theoretical k-eff
Bg = (pi/(H));
k_theo = (Sigma_nuf/Sigma_a)/(1+L*L*Bg*Bg)

figure(2)
rel_error = abs((phi-phiA)./phiA);
plot(x(2:end-1),rel_error(2:end-1),'-om','LineWidth',2)



