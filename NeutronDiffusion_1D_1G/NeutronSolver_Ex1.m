% Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
% Class 5 Examples - Reactor Physics Tutorials  Rev 17-01-2016
% 1D diffusion problem Solver for 1D slab geometry with two zero flux BC

function [phi] = NeutronSolver_Ex1(N,D,h,Sigma_a,Source)


%Matrix & Vector initialziation for 1:N-1  nodes
A = zeros(N-1,N-1);
S = zeros(N-1,1);
phi = zeros(N-1,1);

%Source vector
S(:,1) = Source;  %Ex1
%S(:,1) = Source(2:(end-1)); %Ex2 , Ex3

%Problem matrix generation
a1 = (-D/(h^2));
a2 = (2*D/(h^2))+Sigma_a;
a3 = (-D/(h^2)); 

for i=1:N-1
  %Boundary conditions
  if (i==1)
       %J = 0
%		A(i,1)    = a1*(1-(c/(2*i-1))) + a2 ;
%        A(i,1) = a1 + a2;
%		A(i,2)    = a3;  
	   %phi = 0	
		 A(i,1) = a2;
         A(i,2) = a3;
		% A(i,2) = a3*(1+(c/(2*i-1)))
		
  elseif (i==(N-1))
  
      %phi = 0
		%A(i,N-2)  = a1*(1-(c/(2*i-1)));
        A(i,N-2)  = a1;
		A(i,N-1)  = a2;
  %Tridiagional matrix		
  else
		%A(i,i-1)   = a1*(1-(c/(2*i-1)));
        A(i,i-1)   = a1;
		A(i,i)     = a2;
        A(i,i+1)   = a3;
		%A(i,i+1)   = a3*(1+(c/(2*i-1)));
  end	
end



%Problem solution
phi = A\S;  %or phi=inv(A)*S

%phi0 = phi(1);     %J = 0
phi0 = 0.0;         %phi = 0
phiN = 0.0;

phi = [phi0; phi; phiN];

