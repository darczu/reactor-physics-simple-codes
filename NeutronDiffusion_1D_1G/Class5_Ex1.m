clear; clc; close all;  
% Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
% Class 5 Examples - Reactor Physics Tutorials  Rev 17-01-2016
%
%%% EXERCISE 1 - Spatial Diffusion with constant source %%%
% Solve one speed neutron diffusion equation in 1D slab geometry. Assume symmetry - no flux boundary condition on the left side of the slab and zero flux condition on the right side.
% Solve problem for constant neutron source equal to: 3.0e+14 n/s/cc
% Medium is non-multiplying
% Core data are based on PWR core discussed during Class 2.
% Slab half-thincknes:    H = 180 cm
% Diffusion coeffcient:   D = 9.2106 cm
% Macroscopic abs. XS: Sigma_a  0.1532 1/cm
% 
H = 200;           %slab thickness
a = H/2;	       %half-thickness
D = 9.2106;        %diffusion coeffcient
Sigma_a =  0.1532; %Abs. macro XS
N = 100;	           %nodalization number of divisions
nodes = N+1; 	   %number of nodes
h = H/N;		   %constant node interval - step
Source = 3e14;     %constant source
L = sqrt(D/Sigma_a);  %Diffusion length
c = 1;             %c=0 - plane, c=1 cylindrical, c=2 - spherical

% SOLUTION
phi = NeutronSolver_Ex1(N,D,h,Sigma_a,Source)


%Space
x = linspace(-a,a,nodes);
%x = linspace(0,a,nodes);

%Analitical solution - to be found in Lewis
phiA = (1- (cosh(x./L)./cosh(a./L))).*(Source/Sigma_a); %plane
%phiA = (Source./Sigma_a).*cos(pi.*x./a);


phiA = phiA';
%Plot
figure(1)
hold on;
plot(x,phi,'or','LineWidth',2)
plot(x,phiA,'-b','LineWidth',2)

figure(2)
rel_error = abs((phi-phiA)./phiA);
plot(x,rel_error,'-om','LineWidth',2)



% Macroscopic fission XS times nu:  Sigma_nuf =   0.1567 1/cm