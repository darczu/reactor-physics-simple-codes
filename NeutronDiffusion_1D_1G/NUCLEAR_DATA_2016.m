% NUCLEAR DATA TABLES FOR REACTOR PHYSICS (ANS-666) TUTORIALS 2016
% WARSAW UNIVERSITY OF TEHCNOLOGY, INSTITUTE OF HEAT ENGINEERING, PIOTR DARWNOSKI
% Data based on (Duderstadt, 1976) & (Lewis, 2008) for typical thermal spectrum PWR reactor
% f - fission
% a - absorption
% c - capture
% t - total
% s - scattering
% tr - transport
% L - Lewis data
% D - Duderstadt data
% XS - Cross Section
% In MATLAB you can add it to your script:  run NUCLEAR_DATA_2016.m
% History: 12-12-2016 Class 2/ Example 2

%barn = 1e-24;       %1 barn [cm2]
barn=1;  %OPTION - NO BARNS
N_A = 6.022e+23;    %Avogadro const. 

% === NUCLEAR DATA FOR EXAMPLE 2 CLASS 2 ===

%Microscopic XS Data
% Hydrogen H-1
sHtr = 0.650*barn;           %transport, D
sHa  = 0.294*barn;           %absorption, D


% Oxygen O-16
sOtr = 0.260*barn;           
sOa  = (1.78e-4)*barn;       
            

%Zirconium Zr-91
sZRtr = 0.787*barn;          
sZRa  = 0.190*barn;      

%Iron Fe-56
sFEtr =  0.554*barn;
sFEa  =  2.33*barn;

%U235
sU235tr = 1.62*barn; 
sU235a  = 484.0*barn;
sU235f  = 312.0*barn;  %U-235  (n,f) fission
nuU235  = 2.43;   %nu - av. number of n per fission


%U238
sU238tr = 1.06*barn;
sU238a  = 2.11*barn;
sU238f  = 0.638*barn;
nuU238  = 2.84;


% Xenon Xe-135
sXE135tr = 1.21*barn;
sXE135a  = (2.36e6)*barn;


% Boron B-10
sB10tr = 0.877*barn;
sB10a  = (3.41e+3)*barn;  %There is an error in Duderstandt e-3


